//
//  BlurtWebViewController.swift
//  Runner
//
//  Created by Sagar on 10/07/22.
//

import UIKit
import WebKit

extension Bundle {
    var releaseVersionNumber: String? {
        return infoDictionary?["CFBundleShortVersionString"] as? String
    }
    var buildVersionNumber: String? {
        return infoDictionary?["CFBundleVersion"] as? String
    }
}

class BlurtWebViewController: UIViewController {
	let blurt = "blurt"
	let config = WKWebViewConfiguration()
	let rect = CGRect(x: 0, y: 0, width: 10, height: 10)
	var webView: WKWebView?
	var didFinish = false
	var postingKeyValidationHandler: ((String) -> Void)? = nil
	var upvoteHandler: ((String) -> Void)? = nil
	var decryptTokenHandler: ((String) -> Void)? = nil
	var postVideoHandler: ((String) -> Void)? = nil
	var commentHandler: ((String) -> Void)? = nil
	var updateFollowInfo: ((String) -> Void)? = nil
	var updateFollow: ((String) -> Void)? = nil
	var claimRewardsHandler: ((String) -> Void)? = nil

	override func viewDidLoad() {
		super.viewDidLoad()
		config.userContentController.add(self, name: blurt)
		webView = WKWebView(frame: rect, configuration: config)
		webView?.navigationDelegate = self
		guard
			let path = Bundle.main.path(forResource: "index", ofType: "html", inDirectory: "public")
		else { return }
		let url = URL(fileURLWithPath: path)
		let dir = url.deletingLastPathComponent()
		webView?.loadFileURL(url, allowingReadAccessTo: dir)
	}

	func validatePostingKey(
		username: String,
		postingKey: String,
		domain: String,
		handler: @escaping (String) -> Void
	) {
		postingKeyValidationHandler = handler
		OperationQueue.main.addOperation {
			self.webView?.evaluateJavaScript("validateKey('\(username)', '\(postingKey)', '\(domain)');")
		}
	}

	func upvote(
		username: String,
		postingKey: String,
		domain: String,
		author: String,
		permlink: String,
		weight: Int,
		handler: @escaping (String) -> Void
	) {
		upvoteHandler = handler
		OperationQueue.main.addOperation {
			self.webView?.evaluateJavaScript("upvote('\(username)', '\(postingKey)', '\(domain)', '\(author)', '\(permlink)', \(weight));")
		}
	}

	func updateFollowInfo(
		accountName: String,
		actionAccount: String,
		actionType: String,
		handler: @escaping (String) -> Void
	) {
		updateFollowInfo = handler
		OperationQueue.main.addOperation {
			self.webView?.evaluateJavaScript("updateFollowInfo('\(accountName)', '\(actionAccount)', '\(actionType)');")
		}
	}

	func updateFollowData(
		accountName: String,
		postingKey: String,
		actionAccount: String,
		actionType: String,
		handler: @escaping (String) -> Void
	) {
		updateFollow = handler
		OperationQueue.main.addOperation {
			self.webView?.evaluateJavaScript("updateFollow('\(accountName)', '\(postingKey)', '\(actionAccount)', '\(actionType)');")
		}
	}

	func comment(
		username: String,
		postingKey: String,
		domain: String,
		author: String,
		permlink: String,
		body: String,
		tags: String,
		summary: String,
		title: String,
		pAuthor: String,
		pPermlink: String,
		thumbImage: String,
		handler: @escaping (String) -> Void
	) {
		commentHandler = handler
		var appVersion = "blurt_iOS/\(Bundle.main.releaseVersionNumber ?? "1.0.0")_\(Bundle.main.buildVersionNumber ?? "1")";
		OperationQueue.main.addOperation {
			self.webView?.evaluateJavaScript("comment('\(username)', '\(postingKey)', '\(domain)', '\(permlink)', '\(title)', '\(body)', '\(summary)', '\(tags)', '\(appVersion)', '\(pAuthor)', '\(pPermlink)', '\(thumbImage)');")
		}
	}

	func claimRewards(
		accountName: String,
		postingKey: String,
		domain: String,
		rewardBlurt: String,
		rewardVestingBalance: String,
		handler: @escaping (String) -> Void
	) {
		claimRewardsHandler = handler
		OperationQueue.main.addOperation {
			self.webView?.evaluateJavaScript("claimRewards('\(accountName)', '\(postingKey)', '\(rewardBlurt)', '\(rewardVestingBalance)',  '\(domain)');")
		}
	}
}

extension BlurtWebViewController: WKNavigationDelegate {
	func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
		didFinish = true
	}
}

extension BlurtWebViewController: WKScriptMessageHandler {
	func userContentController(
		_ userContentController: WKUserContentController,
		didReceive message: WKScriptMessage
	) {
		guard message.name == blurt else { return }
		guard let dict = message.body as? [String: AnyObject] else { return }
		guard let type = dict["type"] as? String else { return }
		switch type {
			case "validateKey":
				guard
					let isValid = dict["valid"] as? Bool,
					let accountName = dict["accountName"] as? String,
					let error = dict["error"] as? String,
					let response = ValidBlurtKeyResponse.jsonStringFrom(dict: dict)
				else { return }
				debugPrint("Is it valid? \(isValid ? "TRUE" : "FALSE")")
				debugPrint("account name is \(accountName)")
				debugPrint("Error is \(error)")
				postingKeyValidationHandler?(response)
			case "upvote":
				guard
					let isValid = dict["valid"] as? Bool,
					let accountName = dict["accountName"] as? String,
					let error = dict["error"] as? String,
					let response = ValidBlurtKeyResponse.jsonStringFrom(dict: dict)
				else { return }
				debugPrint("Is it valid? \(isValid ? "TRUE" : "FALSE")")
				debugPrint("account name is \(accountName)")
				debugPrint("Error is \(error)")
				upvoteHandler?(response)
			case "comment":
				guard
					let isValid = dict["valid"] as? Bool,
					let accountName = dict["accountName"] as? String,
					let error = dict["error"] as? String,
					let response = ValidBlurtKeyResponse.jsonStringFrom(dict: dict)
				else { return }
				debugPrint("Is it valid? \(isValid ? "TRUE" : "FALSE")")
				debugPrint("account name is \(accountName)")
				debugPrint("Error is \(error)")
				commentHandler?(response)
			case "updateFollowInfo":
				guard
					let isValid = dict["valid"] as? Bool,
					let accountName = dict["accountName"] as? String,
					let error = dict["error"] as? String,
					let response = ValidBlurtKeyResponse.jsonStringFrom(dict: dict)
				else { return }
				debugPrint("Is it valid? \(isValid ? "TRUE" : "FALSE")")
				debugPrint("account name is \(accountName)")
				debugPrint("Error is \(error)")
				updateFollowInfo?(response)
			case "updateFollow":
				guard
					let isValid = dict["valid"] as? Bool,
					let accountName = dict["accountName"] as? String,
					let error = dict["error"] as? String,
					let response = ValidBlurtKeyResponse.jsonStringFrom(dict: dict)
				else { return }
				debugPrint("Is it valid? \(isValid ? "TRUE" : "FALSE")")
				debugPrint("account name is \(accountName)")
				debugPrint("Error is \(error)")
				updateFollow?(response)
			case "claimRewards":
				guard
					let isValid = dict["valid"] as? Bool,
					let accountName = dict["accountName"] as? String,
					let error = dict["error"] as? String,
					let response = ValidBlurtKeyResponse.jsonStringFrom(dict: dict)
				else { return }
				debugPrint("Is it valid? \(isValid ? "TRUE" : "FALSE")")
				debugPrint("account name is \(accountName)")
				debugPrint("Error is \(error)")
				claimRewardsHandler?(response)
			default: debugPrint("Do nothing here.")
		}
	}
}
