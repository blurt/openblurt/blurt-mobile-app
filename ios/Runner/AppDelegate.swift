import UIKit
import Flutter

@UIApplicationMain
@objc class AppDelegate: FlutterAppDelegate {
	var blurt: BlurtWebViewController?
	let authBridge = AuthBridge()
	override func application(
		_ application: UIApplication,
		didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?
	) -> Bool {
		blurt = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "BlurtWebViewController") as? BlurtWebViewController

		blurt?.viewDidLoad()

		let controller : FlutterViewController = window?.rootViewController as! FlutterViewController
		authBridge.initiate(controller: controller, window: window, blurt: blurt)
		GeneratedPluginRegistrant.register(with: self)
		return super.application(application, didFinishLaunchingWithOptions: launchOptions)
	}
}
