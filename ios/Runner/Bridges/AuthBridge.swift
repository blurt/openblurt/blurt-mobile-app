//
//  AuthBridge.swift
//  Runner
//
//  Created by Sagar on 10/07/22.
//

import UIKit
import Flutter

class AuthBridge {
	var window: UIWindow?
	var blurt: BlurtWebViewController?

	func initiate(controller: FlutterViewController, window: UIWindow?, blurt: BlurtWebViewController?) {
		self.window = window
		self.blurt = blurt
		let authChannel = FlutterMethodChannel(
			name: "blog.blurt.blurt/auth",
			binaryMessenger: controller.binaryMessenger
		)
		authChannel.setMethodCallHandler({
			[weak self] (call: FlutterMethodCall, result: @escaping FlutterResult) -> Void in
			// Note: this method is invoked on the UI thread.
			switch (call.method) {
				case "validate":
					guard
						let arguments = call.arguments as? NSDictionary,
						let username = arguments ["username"] as? String,
						let password = arguments["postingKey"] as? String,
						let domain = arguments["domain"] as? String
					else {
						result(FlutterMethodNotImplemented)
						return
					}
					self?.authenticate(username: username, postingKey: password, domain: domain, result: result)
				case "upvote":
					guard
						let arguments = call.arguments as? NSDictionary,
						let username = arguments ["username"] as? String,
						let password = arguments["postingKey"] as? String,
						let domain = arguments["domain"] as? String,
						let author = arguments["author"] as? String,
						let permlink = arguments["permlink"] as? String,
						let weight = arguments["weight"] as? Int
					else {
						result(FlutterMethodNotImplemented)
						return
					}
					self?.upvote(username: username, postingKey: password, domain: domain, author: author, permlink: permlink, weight: weight, result: result)
				case "updateFollowInfo":
					guard
						let arguments = call.arguments as? NSDictionary,
						let accountName = arguments ["accountName"] as? String,
						let actionAccount = arguments["actionAccount"] as? String,
						let actionType = arguments["actionType"] as? String
					else {
						result(FlutterMethodNotImplemented)
						return
					}
					self?.updateFollowInfo(accountName: accountName, actionAccount: actionAccount, actionType: actionType, result: result)
				case "updateFollow":
					guard
						let arguments = call.arguments as? NSDictionary,
						let postingKey = arguments["postingKey"] as? String,
						let accountName = arguments ["accountName"] as? String,
						let actionAccount = arguments["actionAccount"] as? String,
						let actionType = arguments["actionType"] as? String
					else {
						result(FlutterMethodNotImplemented)
						return
					}
					self?.updateFollow(accountName: accountName, postingKey: postingKey, actionAccount: actionAccount, actionType: actionType, result: result)
				case "comment":
					guard
						let arguments = call.arguments as? NSDictionary,
						let username = arguments ["username"] as? String,
						let password = arguments["postingKey"] as? String,
						let domain = arguments["domain"] as? String,
						let author = arguments["author"] as? String,
						let permlink = arguments["permlink"] as? String,
						let body = arguments["body"] as? String,
						let tags = arguments["tags"] as? String,
						let summary = arguments["summary"] as? String,
						let title = arguments["title"] as? String,
						let pAuthor = arguments["pAuthor"] as? String,
						let pPermlink = arguments["pPermlink"] as? String,
						let thumbImage = arguments["thumbImage"] as? String
					else {
						result(FlutterMethodNotImplemented)
						return
					}
					self?.comment(username: username, password: password, domain: domain, author: author, permlink: permlink, body: body, tags: tags, summary: summary, title: title, pAuthor: pAuthor, pPermlink: pPermlink, thumbImage: thumbImage, result: result)
				case "claimRewards":
					guard
						let arguments = call.arguments as? NSDictionary,
						let postingKey = arguments["postingKey"] as? String,
						let accountName = arguments ["accountName"] as? String,
						let rewardBlurt = arguments["rewardBlurt"] as? String,
						let rewardVestingBalance = arguments["rewardVestingBalance"] as? String,
						let domain = arguments["domain"] as? String
					else {
						result(FlutterMethodNotImplemented)
						return
					}
					self?.claimRewards(accountName: accountName, postingKey: postingKey,
														 domain: domain, rewardBlurt: rewardBlurt,
														 rewardVestingBalance: rewardVestingBalance,
														 result: result)
				default: debugPrint("do nothing")
			}
		})
	}

	private func claimRewards(
		accountName: String, postingKey: String, domain: String, rewardBlurt: String,
		rewardVestingBalance: String, result: @escaping FlutterResult) {
			guard let blurt = blurt else {
				result(FlutterError(code: "ERROR",
														message: "Error setting up Blurt",
														details: nil))
				return
			}
			blurt.claimRewards(accountName: accountName, postingKey: postingKey, domain: domain, rewardBlurt: rewardBlurt, rewardVestingBalance: rewardVestingBalance) { response in
				result(response)
			}
	}

	private func authenticate(username: String, postingKey: String, domain: String, result: @escaping FlutterResult) {
		guard let blurt = blurt else {
			result(FlutterError(code: "ERROR",
													message: "Error setting up Blurt",
													details: nil))
			return
		}
		blurt.validatePostingKey(username: username, postingKey: postingKey, domain: domain) { response in
			result(response)
		}
	}

	private func updateFollow(
		accountName: String,
		postingKey: String,
		actionAccount: String,
		actionType: String,
		result: @escaping FlutterResult
	) {
		guard let blurt = blurt else {
			result(FlutterError(code: "ERROR",
													message: "Error setting up Blurt",
													details: nil))
			return
		}
		blurt.updateFollowData(
			accountName: accountName,
			postingKey: postingKey,
			actionAccount: actionAccount,
			actionType: actionType
		) { response in
			result(response)
		}
	}

	private func updateFollowInfo(
		accountName: String,
		actionAccount: String,
		actionType: String,
		result: @escaping FlutterResult
	) {
		guard let blurt = blurt else {
			result(FlutterError(code: "ERROR",
													message: "Error setting up Blurt",
													details: nil))
			return
		}
		blurt.updateFollowInfo(
			accountName: accountName,
			actionAccount: actionAccount,
			actionType: actionType
		) { response in
			result(response)
		}
	}

	private func upvote(
		username: String,
		postingKey: String,
		domain: String,
		author: String,
		permlink: String,
		weight: Int,
		result: @escaping FlutterResult
	) {
		guard let blurt = blurt else {
			result(FlutterError(code: "ERROR",
													message: "Error setting up Blurt",
													details: nil))
			return
		}
		blurt.upvote(
			username: username,
			postingKey: postingKey,
			domain: domain,
			author: author,
			permlink: permlink,
			weight: weight
		) { response in
			result(response)
		}
	}

	private func comment(
		username: String,
		password: String,
		domain: String,
		author: String,
		permlink: String,
		body: String,
		tags: String,
		summary: String,
		title: String,
		pAuthor: String,
		pPermlink: String,
		thumbImage: String,
		result: @escaping FlutterResult
	) {
		guard let blurt = blurt else {
			result(FlutterError(code: "ERROR",
													message: "Error setting up Blurt",
													details: nil))
			return
		}
		blurt.comment(
			username: username,
			postingKey: password,
			domain: domain,
			author: author,
			permlink: permlink,
			body: body,
			tags: tags,
			summary: summary,
			title: title,
			pAuthor: pAuthor,
			pPermlink: pPermlink,
			thumbImage: thumbImage
		) { response in
			result(response)
		}
	}
}
