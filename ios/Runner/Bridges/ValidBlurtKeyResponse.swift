//
//  ValidBlurtKeyResponse.swift
//  Runner
//
//  Created by Sagar on 10/07/22.
//

import Foundation

struct ValidBlurtKeyResponse: Codable {
	let valid: Bool
	let accountName: String?
	let error: String

	static func jsonStringFrom(dict: [String: AnyObject]) -> String? {
		guard
			let isValid = dict["valid"] as? Bool,
			let error = dict["error"] as? String
		else { return nil }
		let response = ValidBlurtKeyResponse(
			valid: isValid,
			accountName: dict["accountName"] as? String,
			error: error
		)
		guard let data = try? JSONEncoder().encode(response) else { return nil }
		guard let dataString = String(data: data, encoding: .utf8) else { return nil }
		return dataString
	}
}
