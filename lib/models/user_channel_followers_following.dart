import 'dart:convert';

import 'package:blurt/models/safe_convert.dart';

class UserChannelFollow {
  final List<UserChannelFollowItem> result;

  UserChannelFollow({
    required this.result,
  });

  factory UserChannelFollow.fromJson(Map<String, dynamic>? json) =>
      UserChannelFollow(
        result: asList(json, 'result')
            .map((e) => UserChannelFollowItem.fromJson(e))
            .toList(),
      );

  factory UserChannelFollow.fromJsonString(String jsonString) {
    var data = json.decode(jsonString);
    return UserChannelFollow.fromJson(data);
  }

  Map<String, dynamic> toJson() => {
        'result': result.map((e) => e.toJson()),
      };
}

class UserChannelFollowItem {
  final String follower;
  final String following;

  UserChannelFollowItem({
    this.follower = "",
    this.following = "",
  });

  factory UserChannelFollowItem.fromJson(Map<String, dynamic>? json) =>
      UserChannelFollowItem(
        follower: asString(json, 'follower'),
        following: asString(json, 'following'),
      );

  Map<String, dynamic> toJson() => {
        'follower': follower,
        'following': following,
      };
}
