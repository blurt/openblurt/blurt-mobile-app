import 'dart:convert';
import 'dart:core';

import 'package:blurt/models/safe_convert.dart';

class BlurtTagsResponse {
  final BlurtTagsResponseResult result;

  BlurtTagsResponse({
    required this.result,
  });

  factory BlurtTagsResponse.fromJson(Map<String, dynamic>? json) =>
      BlurtTagsResponse(
        result: BlurtTagsResponseResult.fromJson(asMap(json, 'result')),
      );

  factory BlurtTagsResponse.fromJsonString(String jsonString) =>
      BlurtTagsResponse.fromJson(json.decode(jsonString));
}

class BlurtTagsResponseResult {
  final BlurtTagsResponseResultTagIdx tagIdx;

  BlurtTagsResponseResult({
    required this.tagIdx,
  });

  factory BlurtTagsResponseResult.fromJson(Map<String, dynamic>? json) =>
      BlurtTagsResponseResult(
        tagIdx: BlurtTagsResponseResultTagIdx.fromJson(asMap(json, 'tag_idx')),
      );
}

class BlurtTagsResponseResultTagIdx {
  final List<String> trending;

  BlurtTagsResponseResultTagIdx({
    required this.trending,
  });

  factory BlurtTagsResponseResultTagIdx.fromJson(Map<String, dynamic>? json) =>
      BlurtTagsResponseResultTagIdx(
        trending: asList(json, 'trending').map((e) => e.toString()).toList(),
      );

  Map<String, dynamic> toJson() => {
        'trending': trending.map((e) => e).toList(),
      };
}
