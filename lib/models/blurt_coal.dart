import 'dart:convert';

import 'package:blurt/models/safe_convert.dart';

List<BlurtCoalItem> coalItemsFromString(String jsonStr) {
  final jsonList = json.decode(jsonStr) as List;
  return jsonList.map((e) => BlurtCoalItem.fromJson(e)).toList();
}

class BlurtCoalItem {
  final String name;

  BlurtCoalItem({
    required this.name,
  });

  factory BlurtCoalItem.fromJson(Map<String, dynamic>? json) => BlurtCoalItem(
        name: asString(json, 'type'),
      );
}
