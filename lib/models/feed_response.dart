import 'dart:convert';
import 'dart:core';

import 'package:blurt/models/feed_response_item.dart';
import 'package:blurt/models/safe_convert.dart';

class FeedResponse {
  final FeedResponseResult result;

  FeedResponse({
    required this.result,
  });

  factory FeedResponse.fromJson(Map<String, dynamic>? json) => FeedResponse(
        result: FeedResponseResult.fromJson(asMap(json, 'result')),
      );

  factory FeedResponse.fromJsonString(String string) =>
      FeedResponse.fromJson(json.decode(string));
}

class FeedMoreResponse {
  final List<FeedResponseItem> result;

  FeedMoreResponse({
    required this.result,
  });

  factory FeedMoreResponse.fromJson(Map<String, dynamic>? json) {
    // FeedResponseItem
    var items = asList(json, 'result')
        .map((e) {
          var innerMap = e as Map<String, dynamic>?;
          if (innerMap != null) {
            return FeedResponseItem.fromJson(innerMap);
          }
          return null;
        })
        .toList()
        .whereType<FeedResponseItem>()
        .toList();
    return FeedMoreResponse(result: items);
  }

  factory FeedMoreResponse.fromJsonString(String string) =>
      FeedMoreResponse.fromJson(json.decode(string));
}

class CommentsResponse {
  final List<FeedResponseItem> content;

  CommentsResponse({
    required this.content,
  });

  // here key = actual post key
  // everything else is comments
  factory CommentsResponse.excludingKey(
      {Map<String, dynamic>? json, required String key}) {
    var result = asMap(json, 'result');
    var responseContent = asMap(result, 'content');
    var keys = responseContent.keys.toList().where((e) => e != key).toList();
    var list = keys.map((e) {
      return FeedResponseItem.fromJson(asMap(responseContent, e));
    }).toList();
    return CommentsResponse(content: list);
  }

  factory CommentsResponse.excludingKeyFromJson({
    required String string,
    required String forKey,
  }) =>
      CommentsResponse.excludingKey(
        json: json.decode(string),
        key: forKey,
      );
}

class PostDetailsContent {
  final CommentsResponse comments;
  final SingleItemResponse content;
  PostDetailsContent({
    required this.comments,
    required this.content,
  });
}

class SingleItemResponse {
  final FeedResponseItem content;

  SingleItemResponse({
    required this.content,
  });

  factory SingleItemResponse.fromJsonForKey(
      {required Map<String, dynamic>? json, required String forKey}) {
    var result = asMap(json, 'result');
    var responseContent = asMap(result, 'content');
    var content = asMap(
      responseContent,
      responseContent.keys.firstWhere((element) => element == forKey),
    );
    return SingleItemResponse(content: FeedResponseItem.fromJson(content));
  }

  factory SingleItemResponse.fromJsonStringForKey({
    required String string,
    required String forKey,
  }) =>
      SingleItemResponse.fromJsonForKey(
        json: json.decode(string),
        forKey: forKey,
      );
}

class ChainProps {
  final String totalVestingFundBlurt;
  final String totalVestingShares;
  // total_vesting_fund_blurt

  ChainProps({
    required this.totalVestingFundBlurt,
    required this.totalVestingShares,
  });

  double get fund {
    return double.tryParse(totalVestingFundBlurt.replaceAll(" BLURT", "")) ?? 0;
  }

  double get shares {
    return double.tryParse(totalVestingShares.replaceAll(" VESTS", "")) ?? 0;
  }

  factory ChainProps.fromJson(Map<String, dynamic>? json) {
    return ChainProps(
      totalVestingFundBlurt: asString(json, 'total_vesting_fund_blurt'),
      totalVestingShares: asString(json, 'total_vesting_shares'),
    );
  }
}

class FeedResponseResult {
  final List<FeedResponseItem> items;
  final ChainProps props;

  FeedResponseResult({
    required this.items,
    required this.props,
  });

  factory FeedResponseResult.fromJson(Map<String, dynamic>? json) {
    // FeedResponseItem
    var map = asMap(json, 'content');
    var items = map.values
        .map((e) {
          var innerMap = e as Map<String, dynamic>?;
          if (innerMap != null) {
            return FeedResponseItem.fromJson(innerMap);
          }
          return null;
        })
        .toList()
        .whereType<FeedResponseItem>()
        .toList();
    var props = ChainProps.fromJson(asMap(json, 'props'));
    return FeedResponseResult(items: items, props: props);
  }
}
