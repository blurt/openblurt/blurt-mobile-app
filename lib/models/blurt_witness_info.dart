import 'dart:convert';

import 'package:blurt/models/safe_convert.dart';

class BlurtWitnessInfo {
  final BlurtWitnessInfoData? result;

  BlurtWitnessInfo({
    required this.result,
  });

  factory BlurtWitnessInfo.fromJson(Map<String, dynamic>? json) {
    if (json?['result'] == null) {
      return BlurtWitnessInfo(result: null);
    }
    return BlurtWitnessInfo(
      result: BlurtWitnessInfoData.fromJson(asMap(json, 'result')),
    );
  }

  factory BlurtWitnessInfo.fromJsonString(String jsonString) =>
      BlurtWitnessInfo.fromJson(json.decode(jsonString));
}

class BlurtWitnessInfoData {
  final int id;
  final String owner;
  final String created;
  final String url;
  final String votes;
  final String virtualLastUpdate;
  final String virtualPosition;
  final String virtualScheduledTime;
  final int totalMissed;
  final int lastAslot;
  final int lastConfirmedBlockNum;
  final String signingKey;
  final BlurtWitnessInfoDataProps props;
  final String runningVersion;
  final String hardforkVersionVote;
  final String hardforkTimeVote;
  final int availableWitnessAccountSubsidies;

  BlurtWitnessInfoData({
    this.id = 0,
    this.owner = "",
    this.created = "",
    this.url = "",
    this.votes = "",
    this.virtualLastUpdate = "",
    this.virtualPosition = "",
    this.virtualScheduledTime = "",
    this.totalMissed = 0,
    this.lastAslot = 0,
    this.lastConfirmedBlockNum = 0,
    this.signingKey = "",
    required this.props,
    this.runningVersion = "",
    this.hardforkVersionVote = "",
    this.hardforkTimeVote = "",
    this.availableWitnessAccountSubsidies = 0,
  });

  factory BlurtWitnessInfoData.fromJson(Map<String, dynamic>? json) =>
      BlurtWitnessInfoData(
        id: asInt(json, 'id'),
        owner: asString(json, 'owner'),
        created: asString(json, 'created'),
        url: asString(json, 'url'),
        votes: asString(json, 'votes'),
        virtualLastUpdate: asString(json, 'virtual_last_update'),
        virtualPosition: asString(json, 'virtual_position'),
        virtualScheduledTime: asString(json, 'virtual_scheduled_time'),
        totalMissed: asInt(json, 'total_missed'),
        lastAslot: asInt(json, 'last_aslot'),
        lastConfirmedBlockNum: asInt(json, 'last_confirmed_block_num'),
        signingKey: asString(json, 'signing_key'),
        props: BlurtWitnessInfoDataProps.fromJson(asMap(json, 'props')),
        runningVersion: asString(json, 'running_version'),
        hardforkVersionVote: asString(json, 'hardfork_version_vote'),
        hardforkTimeVote: asString(json, 'hardfork_time_vote'),
        availableWitnessAccountSubsidies:
            asInt(json, 'available_witness_account_subsidies'),
      );

  Map<String, dynamic> toJson() => {
        'id': id,
        'owner': owner,
        'created': created,
        'url': url,
        'votes': votes,
        'virtual_last_update': virtualLastUpdate,
        'virtual_position': virtualPosition,
        'virtual_scheduled_time': virtualScheduledTime,
        'total_missed': totalMissed,
        'last_aslot': lastAslot,
        'last_confirmed_block_num': lastConfirmedBlockNum,
        'signing_key': signingKey,
        'props': props.toJson(),
        'running_version': runningVersion,
        'hardfork_version_vote': hardforkVersionVote,
        'hardfork_time_vote': hardforkTimeVote,
        'available_witness_account_subsidies': availableWitnessAccountSubsidies,
      };
}

class BlurtWitnessInfoDataProps {
  final String accountCreationFee;
  final int maximumBlockSize;
  final int accountSubsidyBudget;
  final int accountSubsidyDecay;
  final String operationFlatFee;
  final String bandwidthKbytesFee;
  final String proposalFee;

  BlurtWitnessInfoDataProps({
    this.accountCreationFee = "",
    this.maximumBlockSize = 0,
    this.accountSubsidyBudget = 0,
    this.accountSubsidyDecay = 0,
    this.operationFlatFee = "",
    this.bandwidthKbytesFee = "",
    this.proposalFee = "",
  });

  factory BlurtWitnessInfoDataProps.fromJson(Map<String, dynamic>? json) =>
      BlurtWitnessInfoDataProps(
        accountCreationFee: asString(json, 'account_creation_fee'),
        maximumBlockSize: asInt(json, 'maximum_block_size'),
        accountSubsidyBudget: asInt(json, 'account_subsidy_budget'),
        accountSubsidyDecay: asInt(json, 'account_subsidy_decay'),
        operationFlatFee: asString(json, 'operation_flat_fee'),
        bandwidthKbytesFee: asString(json, 'bandwidth_kbytes_fee'),
        proposalFee: asString(json, 'proposal_fee'),
      );

  Map<String, dynamic> toJson() => {
        'account_creation_fee': accountCreationFee,
        'maximum_block_size': maximumBlockSize,
        'account_subsidy_budget': accountSubsidyBudget,
        'account_subsidy_decay': accountSubsidyDecay,
        'operation_flat_fee': operationFlatFee,
        'bandwidth_kbytes_fee': bandwidthKbytesFee,
        'proposal_fee': proposalFee,
      };
}
