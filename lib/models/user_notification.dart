import 'dart:convert';

import 'package:blurt/models/safe_convert.dart';

List<UserNotificationItem> userNotificationItemsfromJsonString(String jsonStr) {
  final jsonList = json.decode(jsonStr) as List;
  return jsonList.map((e) => UserNotificationItem.fromJson(e)).toList();
}

class UserNotificationItem {
  final String type;
  final String voter;
  final String permlink;
  final int weight;
  final int timestamp;
  final int block;
  final String follower;
  final bool isRootPost;
  final String author;
  final String parentPermlink;
  final String account;
  final String from;
  final String amount;
  final String memo;
  final bool? approve;

  UserNotificationItem({
    this.type = "",
    this.voter = "",
    this.permlink = "",
    this.weight = 0,
    this.timestamp = 0,
    this.block = 0,
    this.follower = "",
    this.isRootPost = false,
    this.author = "",
    this.parentPermlink = "",
    this.account = "",
    this.from = "",
    this.amount = "",
    this.memo = "",
    this.approve = false,
  });

  factory UserNotificationItem.fromJson(Map<String, dynamic>? json) =>
      UserNotificationItem(
        type: asString(json, 'type'),
        voter: asString(json, 'voter'),
        permlink: asString(json, 'permlink'),
        weight: asInt(json, 'weight'),
        timestamp: asInt(json, 'timestamp'),
        block: asInt(json, 'block'),
        follower: asString(json, 'follower'),
        isRootPost: asBool(json, 'is_root_post'),
        author: asString(json, 'author'),
        parentPermlink: asString(json, 'parent_permlink'),
        account: asString(json, 'account'),
        from: asString(json, 'from'),
        amount: asString(json, 'amount'),
        memo: asString(json, 'memo'),
        approve: asBool(json, 'approve'),
      );

  String get name {
    if (voter.isNotEmpty) return voter;
    if (follower.isNotEmpty) return follower;
    if (author.isNotEmpty) return author;
    if (account.isNotEmpty) return account;
    if (from.isNotEmpty) return from;
    return '';
  }
}
