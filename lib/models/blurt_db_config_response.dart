import 'dart:convert';

import 'package:blurt/models/safe_convert.dart';

class BlurtDBConfigResponse {
  final String jsonrpc;
  final BlurtDBConfigResponseResult result;
  final int id;

  BlurtDBConfigResponse({
    this.jsonrpc = "",
    required this.result,
    this.id = 0,
  });

  factory BlurtDBConfigResponse.fromJson(Map<String, dynamic>? json) =>
      BlurtDBConfigResponse(
        jsonrpc: asString(json, 'jsonrpc'),
        result: BlurtDBConfigResponseResult.fromJson(asMap(json, 'result')),
        id: asInt(json, 'id'),
      );

  factory BlurtDBConfigResponse.fromJsonString(String jsonString) =>
      BlurtDBConfigResponse.fromJson(json.decode(jsonString));
}

class BlurtDBConfigResponseResult {
  final int blurtVotingManaRegenerationSeconds;

  BlurtDBConfigResponseResult({
    this.blurtVotingManaRegenerationSeconds = 0,
  });

  factory BlurtDBConfigResponseResult.fromJson(Map<String, dynamic>? json) =>
      BlurtDBConfigResponseResult(
        blurtVotingManaRegenerationSeconds:
            asInt(json, 'BLURT_VOTING_MANA_REGENERATION_SECONDS'),
      );
}
