import 'dart:convert';

import 'package:blurt/models/safe_convert.dart';

class FeedResponseItem {
  final int id;
  final String author;
  final String permlink;
  final String category;
  final String parentAuthor;
  final String parentPermlink;
  final String title;
  final String body;
  final String jsonMetadata;
  final String lastUpdate;
  final String created;
  final String active;
  final String lastPayout;
  final int depth;
  final int children;
  final String netRshares;
  final String absRshares;
  final String voteRshares;
  final String childrenAbsRshares;
  final String cashoutTime;
  final String maxCashoutTime;
  final int totalVoteWeight;
  final int rewardWeight;
  final String totalPayoutValue;
  final String curatorPayoutValue;
  final int authorRewards;
  final int netVotes;
  final String rootAuthor;
  final String rootPermlink;
  final String maxAcceptedPayout;
  final int percentBlurt;
  final bool allowReplies;
  final bool allowVotes;
  final bool allowCurationRewards;
  final String url;
  final String rootTitle;
  final String pendingPayoutValue;
  final String totalPendingPayoutValue;
  final List<FeedResponseItemActiveVotesItem> activeVotes;
  final String promoted;
  final int bodyLength;
  final String? firstReBloggedBy;
  final String? firstRebloggedOn;
  var visited = false;

  FeedResponseItemJsonMetaData? get meta {
    if (jsonMetadata.isEmpty) {
      return null;
    }
    return FeedResponseItemJsonMetaData.fromString(jsonMetadata);
  }

  FeedResponseItem({
    this.id = 0,
    this.author = "",
    this.permlink = "",
    this.category = "",
    this.parentAuthor = "",
    this.parentPermlink = "",
    this.title = "",
    this.body = "",
    this.jsonMetadata = "",
    this.lastUpdate = "",
    this.created = "",
    this.active = "",
    this.lastPayout = "",
    this.depth = 0,
    this.children = 0,
    this.netRshares = "",
    this.absRshares = "",
    this.voteRshares = "",
    this.childrenAbsRshares = "",
    this.cashoutTime = "",
    this.maxCashoutTime = "",
    this.totalVoteWeight = 0,
    this.rewardWeight = 0,
    this.totalPayoutValue = "",
    this.curatorPayoutValue = "",
    this.authorRewards = 0,
    this.netVotes = 0,
    this.rootAuthor = "",
    this.rootPermlink = "",
    this.maxAcceptedPayout = "",
    this.percentBlurt = 0,
    this.allowReplies = false,
    this.allowVotes = false,
    this.allowCurationRewards = false,
    this.url = "",
    this.rootTitle = "",
    this.pendingPayoutValue = "",
    this.totalPendingPayoutValue = "",
    required this.activeVotes,
    this.promoted = "",
    this.bodyLength = 0,
    this.firstReBloggedBy = null,
    this.firstRebloggedOn = null,
  });

  DateTime get createdAt {
    return DateTime.tryParse('$created+0000') ?? DateTime.now();
  }

  factory FeedResponseItem.fromJson(Map<String, dynamic>? json) =>
      FeedResponseItem(
        id: asInt(json, 'id'),
        author: asString(json, 'author'),
        permlink: asString(json, 'permlink'),
        category: asString(json, 'category'),
        parentAuthor: asString(json, 'parent_author'),
        parentPermlink: asString(json, 'parent_permlink'),
        title: asString(json, 'title'),
        body: asString(json, 'body'),
        jsonMetadata: asString(json, 'json_metadata'),
        lastUpdate: asString(json, 'last_update'),
        created: asString(json, 'created'),
        active: asString(json, 'active'),
        lastPayout: asString(json, 'last_payout'),
        depth: asInt(json, 'depth'),
        children: asInt(json, 'children'),
        netRshares: asString(json, 'net_rshares'),
        absRshares: asString(json, 'abs_rshares'),
        voteRshares: asString(json, 'vote_rshares'),
        childrenAbsRshares: asString(json, 'children_abs_rshares'),
        cashoutTime: asString(json, 'cashout_time'),
        maxCashoutTime: asString(json, 'max_cashout_time'),
        totalVoteWeight: asInt(json, 'total_vote_weight'),
        rewardWeight: asInt(json, 'reward_weight'),
        totalPayoutValue: asString(json, 'total_payout_value'),
        curatorPayoutValue: asString(json, 'curator_payout_value'),
        authorRewards: asInt(json, 'author_rewards'),
        netVotes: asInt(json, 'net_votes'),
        rootAuthor: asString(json, 'root_author'),
        rootPermlink: asString(json, 'root_permlink'),
        maxAcceptedPayout: asString(json, 'max_accepted_payout'),
        percentBlurt: asInt(json, 'percent_blurt'),
        allowReplies: asBool(json, 'allow_replies'),
        allowVotes: asBool(json, 'allow_votes'),
        allowCurationRewards: asBool(json, 'allow_curation_rewards'),
        url: asString(json, 'url'),
        rootTitle: asString(json, 'root_title'),
        pendingPayoutValue: asString(json, 'pending_payout_value'),
        totalPendingPayoutValue: asString(json, 'total_pending_payout_value'),
        activeVotes: asList(json, 'active_votes')
            .map((e) => FeedResponseItemActiveVotesItem.fromJson(e))
            .toList(),
        promoted: asString(json, 'promoted'),
        bodyLength: asInt(json, 'body_length'),
        firstReBloggedBy: asString(json, 'first_reblogged_by'),
        firstRebloggedOn: asString(json, 'first_reblogged_on'),
      );

  double get pendingPayout {
    return double.tryParse(pendingPayoutValue.split(" ")[0]) ?? 0.0;
  }

  double get totalPayout {
    return double.tryParse(totalPayoutValue.split(" ")[0]) ?? 0.0;
  }

  double get curatorPayout {
    return double.tryParse(curatorPayoutValue.split(" ")[0]) ?? 0.0;
  }

  double get payout {
    return pendingPayout > (totalPayout + curatorPayout)
        ? pendingPayout
        : (totalPayout + curatorPayout);
  }

  double get netResourceShares {
    return double.tryParse(netRshares) ?? 0.0;
  }

  Map<String, dynamic> toJson() => {
        'id': id,
        'author': author,
        'permlink': permlink,
        'category': category,
        'parent_author': parentAuthor,
        'parent_permlink': parentPermlink,
        'title': title,
        'body': body,
        'json_metadata': jsonMetadata,
        'last_update': lastUpdate,
        'created': created,
        'active': active,
        'last_payout': lastPayout,
        'depth': depth,
        'children': children,
        'net_rshares': netRshares,
        'abs_rshares': absRshares,
        'vote_rshares': voteRshares,
        'children_abs_rshares': childrenAbsRshares,
        'cashout_time': cashoutTime,
        'max_cashout_time': maxCashoutTime,
        'total_vote_weight': totalVoteWeight,
        'reward_weight': rewardWeight,
        'total_payout_value': totalPayoutValue,
        'curator_payout_value': curatorPayoutValue,
        'author_rewards': authorRewards,
        'net_votes': netVotes,
        'root_author': rootAuthor,
        'root_permlink': rootPermlink,
        'max_accepted_payout': maxAcceptedPayout,
        'percent_blurt': percentBlurt,
        'allow_replies': allowReplies,
        'allow_votes': allowVotes,
        'allow_curation_rewards': allowCurationRewards,
        'url': url,
        'root_title': rootTitle,
        'pending_payout_value': pendingPayoutValue,
        'total_pending_payout_value': totalPendingPayoutValue,
        'active_votes': activeVotes.map((e) => e.toJson()),
        'promoted': promoted,
        'body_length': bodyLength,
      };
}

class FeedResponseItemJsonMetaData {
  final List<String> tags;
  final List<String>? images;
  final String? description;
  final String? app;

  FeedResponseItemJsonMetaData({
    required this.tags,
    required this.description,
    required this.images,
    required this.app,
  });

  factory FeedResponseItemJsonMetaData.fromJson(Map<String, dynamic>? json) =>
      FeedResponseItemJsonMetaData(
        tags: asList(json, 'tags').whereType<String>().toList(),
        images: asList(json, 'image').whereType<String>().toList(),
        description: asString(json, 'description'),
        app: asString(json, 'app'),
      );

  factory FeedResponseItemJsonMetaData.fromString(String source) =>
      FeedResponseItemJsonMetaData.fromJson(json.decode(source));
}

class FeedResponseItemActiveVotesItem {
  final String voter;
  final double weight;
  final String rshares;
  final int percent;
  final String time;

  double get resourceShares {
    return double.tryParse(rshares) ?? 0.0;
  }

  DateTime? get voteTime {
    return DateTime.tryParse("$time+00:00");
  }

  FeedResponseItemActiveVotesItem({
    this.voter = "",
    this.weight = 0,
    this.rshares = "",
    this.percent = 0,
    this.time = "",
  });

  factory FeedResponseItemActiveVotesItem.fromJson(
          Map<String, dynamic>? json) =>
      FeedResponseItemActiveVotesItem(
        voter: asString(json, 'voter'),
        weight: asDouble(json, 'weight'),
        rshares: asString(json, 'rshares'),
        percent: asInt(json, 'percent'),
        time: asString(json, 'time'),
      );

  Map<String, dynamic> toJson() => {
        'voter': voter,
        'weight': weight,
        'rshares': rshares,
        'percent': percent,
        'time': time,
      };
}
