import 'dart:convert';

import 'package:blurt/models/safe_convert.dart';

class UserChannelInfoResponse {
  final UserChannelInfoData result;
  final String balance;
  final int postCount;
  final double votingPower;
  final UserChannelProps props;
  final String vestingShares;
  final String delegatedVestingShares;

  UserChannelInfoResponse({
    required this.result,
    required this.balance,
    required this.postCount,
    required this.votingPower,
    required this.props,
    required this.vestingShares,
    required this.delegatedVestingShares,
  });

  double get vested {
    return double.tryParse(vestingShares.replaceAll(" VESTS", "")) ?? 0;
  }

  double get delegated {
    return double.tryParse(delegatedVestingShares.replaceAll(" VESTS", "")) ??
        0;
  }

  String get blurtPower {
    return "${((vested - delegated) * (props.fund / props.shares)).toStringAsFixed(3)} BP";
  }

  String getVotingPower() {
    return "${votingPower.toStringAsFixed(2)} %";
  }

  factory UserChannelInfoResponse.fromJsonString(
    String jsonString,
    String name,
  ) {
    var data = json.decode(jsonString);
    var result = asMap(data, 'result');
    var user = asMap(asMap(result, 'accounts'), name);
    var userJsonString = asString(user, 'json_metadata');
    if (userJsonString.isEmpty) {
      return UserChannelInfoResponse(
        balance: asString(user, 'balance'),
        postCount: asInt(user, 'post_count'),
        votingPower: asDouble(user, 'voting_power'),
        vestingShares: asString(user, 'vesting_shares'),
        delegatedVestingShares: asString(user, 'delegated_vesting_shares'),
        props: UserChannelProps.fromJson(asMap(result, 'props')),
        result: UserChannelInfoData(
          name: name,
          about: 'No info provided',
          coverImage: '',
          location: '',
          profileImage: '',
        ),
      );
    } else {
      return UserChannelInfoResponse(
        props: UserChannelProps.fromJson(asMap(result, 'props')),
        balance: asString(user, 'balance'),
        postCount: asInt(user, 'post_count'),
        vestingShares: asString(user, 'vesting_shares'),
        delegatedVestingShares: asString(user, 'delegated_vesting_shares'),
        votingPower: asDouble(user, 'voting_power') / 100.0,
        result: UserChannelInfoData.fromJsonString(userJsonString),
      );
    }
  }
}

class UserChannelProps {
  final String totalVestingFundBlurt;
  final String totalVestingShares;

  UserChannelProps({
    required this.totalVestingFundBlurt,
    required this.totalVestingShares,
  });

  factory UserChannelProps.fromJson(Map<String, dynamic>? json) =>
      UserChannelProps(
        totalVestingFundBlurt: asString(json, 'total_vesting_fund_blurt'),
        totalVestingShares: asString(json, 'total_vesting_shares'),
      );

  double get fund {
    return double.tryParse(totalVestingFundBlurt.replaceAll(" BLURT", "")) ?? 0;
  }

  double get shares {
    return double.tryParse(totalVestingShares.replaceAll(" VESTS", "")) ?? 0;
  }
}

class UserChannelInfoData {
  final String profileImage;
  final String coverImage;
  final String name;
  final String about;
  final String location;

  UserChannelInfoData({
    required this.profileImage,
    required this.coverImage,
    required this.name,
    required this.about,
    required this.location,
  });

  factory UserChannelInfoData.fromJson(Map<String, dynamic>? json) =>
      UserChannelInfoData(
        profileImage: asString(json, 'profile_image'),
        coverImage: asString(json, 'cover_image'),
        name: asString(json, 'name'),
        about: asString(json, 'about'),
        location: asString(json, 'location'),
      );

  factory UserChannelInfoData.fromJsonString(String jsonString) =>
      UserChannelInfoData.fromJson(asMap(json.decode(jsonString), 'profile'));
}
