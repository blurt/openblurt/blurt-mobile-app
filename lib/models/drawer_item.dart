import 'dart:convert';

import 'package:blurt/bloc/server.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:provider/provider.dart';

class DrawerItem {
  final String name;
  final IconData icon;

  http.Request? getRequest(BuildContext context) {
    var data = Provider.of<BlurtAppData>(context, listen: false);
    var domain = data.domain;
    var userData = data.userData;
    var newName = name.toLowerCase();
    if (newName == 'new') {
      newName = 'created';
    }
    if (newName == 'tags') {
      newName = 'hot';
    }
    if (newName.startsWith('hot')) {
      newName = 'hot';
    }
    if (newName != 'hot' &&
        newName != 'trending' &&
        newName != 'created' &&
        newName != 'my feed') {
      return null;
    }
    if (newName == 'my feed' && userData != null) {
      newName = "@${userData.username}/feed";
    }
    if (newName == 'hot' && name.toLowerCase() != 'hot') {
      newName = name.toLowerCase();
    }
    var request = http.Request('POST', Uri.parse(domain));
    request.body = json.encode({
      "id": 0,
      "jsonrpc": "2.0",
      "method": "call",
      "params": [
        "condenser_api",
        "get_state",
        [
          newName,
        ]
      ]
    });
    return request;
  }

  http.Request? getRequestForNextPage(
      BuildContext context, String author, String permlink) {
    var data = Provider.of<BlurtAppData>(context, listen: false);
    var domain = data.domain;
    var newName = name.toLowerCase();
    var userData = data.userData;
    if (newName == 'hot') {
      newName = 'get_discussions_by_hot';
    } else if (newName == 'trending') {
      newName = 'get_discussions_by_trending';
    } else if (newName == 'new') {
      newName = 'get_discussions_by_created';
    } else if (newName == 'my feed') {
      newName = 'get_discussions_by_feed';
    } else if (newName == 'tags') {
      newName = 'get_discussions_by_hot';
    } else if (newName.startsWith('hot')) {
      newName = 'get_discussions_by_hot';
    } else {
      return null;
    }
    var request = http.Request('POST', Uri.parse(domain));
    request.body = json.encode({
      "id": 0,
      "jsonrpc": "2.0",
      "method": "call",
      "params": [
        "condenser_api",
        newName,
        [
          {
            "tag": newName == 'get_discussions_by_feed' && userData != null
                ? userData.username
                : name.toLowerCase().startsWith('hot')
                    ? name.toLowerCase().replaceAll('hot/', '')
                    : "",
            "limit": 20,
            "start_author": author,
            "start_permlink": permlink
          }
        ]
      ]
    });
    return request;
  }

  DrawerItem({
    required this.name,
    required this.icon,
  });

  static List<DrawerItem> items = [
    DrawerItem(name: "Announcements", icon: Icons.newspaper),
  ];

  static DrawerItem myFeed =
      DrawerItem(name: "My Feed", icon: Icons.person_pin_sharp);
}
