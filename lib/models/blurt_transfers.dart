import 'dart:convert';

import 'package:blurt/models/blurt_transfer_history.dart';
import 'package:blurt/models/safe_convert.dart';

class BlurtTransfers {
  final List<BlurtTransferHistory> transferHistory;

  BlurtTransfers({
    required this.transferHistory,
  });

  factory BlurtTransfers.fromList(List<dynamic> json, String type) {
    var items = json
        .map((e) {
          var innerItems = e as List<dynamic>;
          var map = innerItems[1] as Map<String, dynamic>;
          return BlurtTransferHistory.fromJson(map);
        })
        .where((element) {
          return element.opType == type;
        })
        .toList()
        .reversed
        .toList();
    return BlurtTransfers(
      transferHistory: items,
    );
  }
}

class BlurtTransferResponse {
  BlurtTransfers claims;
  BlurtTransfers curationRewards;
  BlurtTransfers authorRewards;
  BlurtTransfers transfer;
  BlurtTransfers transferPower;
  String? rewardBlurt;
  String? rewardVestingBalance;
  String? rewardVestingBlurt;

  BlurtTransferResponse({
    required this.claims,
    required this.curationRewards,
    required this.authorRewards,
    required this.transfer,
    required this.transferPower,
    required this.rewardBlurt,
    required this.rewardVestingBalance,
    required this.rewardVestingBlurt,
  });

  factory BlurtTransferResponse.fromList(
      Map<String, dynamic>? json, String owner) {
    var resultMap = asMap(json, 'result');
    var accountsMap = asMap(resultMap, 'accounts');
    var ownerMap = asMap(accountsMap, owner);
    var historyList = asList(ownerMap, 'transfer_history');
    var rewardBlurt = asString(ownerMap, 'reward_blurt_balance');
    var rewardVestingBalance = asString(ownerMap, 'reward_vesting_balance');
    var rewardVestingBlurt = asString(ownerMap, 'reward_vesting_blurt');
    return BlurtTransferResponse(
      claims: BlurtTransfers.fromList(historyList, "claim_reward_balance"),
      authorRewards: BlurtTransfers.fromList(historyList, "author_reward"),
      curationRewards: BlurtTransfers.fromList(historyList, "curation_reward"),
      transfer: BlurtTransfers.fromList(historyList, "transfer"),
      transferPower:
          BlurtTransfers.fromList(historyList, "transfer_to_vesting"),
      rewardBlurt: rewardBlurt,
      rewardVestingBalance: rewardVestingBalance,
      rewardVestingBlurt: rewardVestingBlurt,
    );
  }

  factory BlurtTransferResponse.fromString(String string, String owner) {
    var jsonDecoded = json.decode(string);
    return BlurtTransferResponse.fromList(jsonDecoded, owner);
  }
}
