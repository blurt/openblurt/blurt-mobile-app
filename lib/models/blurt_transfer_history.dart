import 'package:blurt/models/blurt_transfer_history_op.dart';
import 'package:blurt/models/safe_convert.dart';
import 'package:timeago/timeago.dart' as timeago;

class BlurtTransferHistory {
  final int virtualOp;
  final String timestamp;
  final String opType;
  final BlurtTransferHistoryOp operationData;

  DateTime? get dateTime {
    return DateTime.tryParse(timestamp);
  }

  String get dateTimeString {
    return dateTime != null ? timeago.format(dateTime!) : "";
  }

  BlurtTransferHistory({
    this.virtualOp = 0,
    this.timestamp = "",
    this.opType = "",
    required this.operationData,
  });

  factory BlurtTransferHistory.fromJson(Map<String, dynamic>? json) {
    var dataAsList = asList(json, "op");
    var opType = dataAsList.first as String;
    var data = BlurtTransferHistoryOp.fromJson(dataAsList[1]);
    return BlurtTransferHistory(
      virtualOp: asInt(json, 'virtual_op'),
      timestamp: asString(json, 'timestamp'),
      opType: opType,
      operationData: data,
    );
  }
}
