class TemplateItem {
  String title;
  String body;
  String tags;
  String thumb;

  TemplateItem({
    required this.title,
    required this.tags,
    required this.thumb,
    required this.body,
  });

  Map<String, String> toJSONEncodable() {
    return {
      'title': title,
      'body': body,
      'tags': tags,
      'thumb': thumb,
    };
  }

  factory TemplateItem.from(Map map) {
    return TemplateItem(
      title: map['title'] ?? '',
      tags: map['tags'] ?? '',
      thumb: map['thumb'] ?? '',
      body: map['body'] ?? '',
    );
  }
}

class TemplateItemList {
  List<TemplateItem> items = [];

  TemplateItemList({
    required this.items,
  });

  List<Map<String, String>> toJSONEncodable() {
    return items.map((item) {
      return item.toJSONEncodable();
    }).toList();
  }

  factory TemplateItemList.from(List list) {
    return TemplateItemList(
      items: list.map((e) {
        return TemplateItem.from(e as Map);
      }).toList(),
    );
  }
}
