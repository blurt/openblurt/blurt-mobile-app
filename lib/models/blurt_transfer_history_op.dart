import 'package:blurt/models/safe_convert.dart';

class BlurtTransferHistoryOp {
  final String author;
  final String permlink;
  final String blurtPayout;
  final String vestingPayout;
  final String rewardBlurt;
  final String rewardVests;
  final String commentAuthor;
  final String commentPermlink;
  final String from;
  final String to;
  final String amount;
  final String memo;

  BlurtTransferHistoryOp({
    this.author = "",
    this.permlink = "",
    this.blurtPayout = "",
    this.vestingPayout = "",
    this.rewardBlurt = "",
    this.rewardVests = "",
    this.commentAuthor = "",
    this.commentPermlink = "",
    this.from = "",
    this.to = "",
    this.amount = "",
    this.memo = "",
  });

  factory BlurtTransferHistoryOp.fromJson(Map<String, dynamic>? json) =>
      BlurtTransferHistoryOp(
        author: asString(json, 'author'),
        permlink: asString(json, 'permlink'),
        blurtPayout: asString(json, 'blurt_payout'),
        vestingPayout: asString(json, 'vesting_payout'),
        rewardBlurt: asString(json, 'reward_blurt'),
        rewardVests: asString(json, 'reward_vests'),
        commentAuthor: asString(json, 'comment_author'),
        commentPermlink: asString(json, 'comment_permlink'),
        from: asString(json, 'from'),
        to: asString(json, 'to'),
        amount: asString(json, 'amount'),
        memo: asString(json, 'memo'),
      );
}
