import 'dart:convert';

import 'package:blurt/models/safe_convert.dart';

class BlurtChainProperties {
  final String jsonrpc;
  final BlurtChainPropertiesResult result;
  final int id;

  BlurtChainProperties({
    this.jsonrpc = "",
    required this.result,
    this.id = 0,
  });

  double get bandwidthFee {
    return double.tryParse(
            result.bandwidthKbytesFee.split(" ").first.toString()) ??
        0.750;
  }

  double get flatFee {
    return double.tryParse(
            result.operationFlatFee.split(" ").first.toString()) ??
        0.012;
  }

  factory BlurtChainProperties.fromJson(Map<String, dynamic>? json) =>
      BlurtChainProperties(
        jsonrpc: asString(json, 'jsonrpc'),
        result: BlurtChainPropertiesResult.fromJson(asMap(json, 'result')),
        id: asInt(json, 'id'),
      );

  factory BlurtChainProperties.fromJsonString(String string) =>
      BlurtChainProperties.fromJson(json.decode(string));

  Map<String, dynamic> toJson() => {
        'jsonrpc': jsonrpc,
        'result': result.toJson(),
        'id': id,
      };
}

class BlurtChainPropertiesResult {
  final String accountCreationFee;
  final int maximumBlockSize;
  final int accountSubsidyBudget;
  final int accountSubsidyDecay;
  final String operationFlatFee;
  final String bandwidthKbytesFee;

  BlurtChainPropertiesResult({
    this.accountCreationFee = "",
    this.maximumBlockSize = 0,
    this.accountSubsidyBudget = 0,
    this.accountSubsidyDecay = 0,
    this.operationFlatFee = "",
    this.bandwidthKbytesFee = "",
  });

  factory BlurtChainPropertiesResult.fromJson(Map<String, dynamic>? json) =>
      BlurtChainPropertiesResult(
        accountCreationFee: asString(json, 'account_creation_fee'),
        maximumBlockSize: asInt(json, 'maximum_block_size'),
        accountSubsidyBudget: asInt(json, 'account_subsidy_budget'),
        accountSubsidyDecay: asInt(json, 'account_subsidy_decay'),
        operationFlatFee: asString(json, 'operation_flat_fee'),
        bandwidthKbytesFee: asString(json, 'bandwidth_kbytes_fee'),
      );

  Map<String, dynamic> toJson() => {
        'account_creation_fee': accountCreationFee,
        'maximum_block_size': maximumBlockSize,
        'account_subsidy_budget': accountSubsidyBudget,
        'account_subsidy_decay': accountSubsidyDecay,
        'operation_flat_fee': operationFlatFee,
        'bandwidth_kbytes_fee': bandwidthKbytesFee,
      };
}
