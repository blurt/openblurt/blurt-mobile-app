class BlurtUserData {
  String username;
  String postingKey;

  BlurtUserData({
    required this.username,
    required this.postingKey,
  });
}
