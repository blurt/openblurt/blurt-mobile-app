import 'dart:convert';

import 'package:blurt/models/safe_convert.dart';

class BlurtUserWitnessVotes {
  final String jsonrpc;
  final BlurtUserWitnessVotesObject result;

  BlurtUserWitnessVotes({
    this.jsonrpc = "",
    required this.result,
  });

  factory BlurtUserWitnessVotes.fromJson(Map<String, dynamic>? json) =>
      BlurtUserWitnessVotes(
        jsonrpc: asString(json, 'jsonrpc'),
        result: BlurtUserWitnessVotesObject.fromJson(asMap(json, 'result')),
      );

  factory BlurtUserWitnessVotes.fromJsonString(String jsonString) =>
      BlurtUserWitnessVotes.fromJson(json.decode(jsonString));

  Map<String, dynamic> toJson() => {
        'jsonrpc': jsonrpc,
        'result': result.toJson(),
      };
}

class BlurtUserWitnessVotesObject {
  final List<BlurtUserWitnessVotesObjectVoteItem> votes;

  BlurtUserWitnessVotesObject({
    required this.votes,
  });

  factory BlurtUserWitnessVotesObject.fromJson(Map<String, dynamic>? json) =>
      BlurtUserWitnessVotesObject(
        votes: asList(json, 'votes')
            .map((e) => BlurtUserWitnessVotesObjectVoteItem.fromJson(e))
            .toList(),
      );

  Map<String, dynamic> toJson() => {
        'votes': votes.map((e) => e.toJson()).toList(),
      };
}

class BlurtUserWitnessVotesObjectVoteItem {
  final String witness;
  final String account;

  BlurtUserWitnessVotesObjectVoteItem({
    this.witness = "",
    this.account = "",
  });

  factory BlurtUserWitnessVotesObjectVoteItem.fromJson(
          Map<String, dynamic>? json) =>
      BlurtUserWitnessVotesObjectVoteItem(
        witness: asString(json, 'witness'),
        account: asString(json, 'account'),
      );

  Map<String, dynamic> toJson() => {
        'witness': witness,
        'account': account,
      };
}
