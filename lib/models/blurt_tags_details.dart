import 'dart:convert';

import 'package:blurt/models/safe_convert.dart';

class BlurtTagDetails {
  final List<BlurtTagDetailsItem> items;

  BlurtTagDetails({
    required this.items,
  });

  factory BlurtTagDetails.fromJson(Map<String, dynamic>? json) {
    var result = asMap(json, 'result');
    var tags = asMap(result, 'tags');
    var items = tags.values
        .map((e) {
          var innerMap = e as Map<String, dynamic>?;
          if (innerMap != null) {
            return BlurtTagDetailsItem.fromJson(innerMap);
          }
          return null;
        })
        .toList()
        .whereType<BlurtTagDetailsItem>()
        .toList();
    return BlurtTagDetails(items: items);
  }

  factory BlurtTagDetails.fromJsonString(String string) =>
      BlurtTagDetails.fromJson(json.decode(string));
}

class BlurtTagDetailsItem {
  final String name;
  final String totalPayouts;
  final int comments;
  final int topPosts;

  BlurtTagDetailsItem({
    this.name = "No Tag",
    required this.totalPayouts,
    required this.comments,
    required this.topPosts,
  });

  double get payout {
    return double.tryParse(totalPayouts.replaceAll(" BLURT", "")) ?? 0;
  }

  factory BlurtTagDetailsItem.fromJson(Map<String, dynamic>? json) =>
      BlurtTagDetailsItem(
        name: asString(json, 'name'),
        totalPayouts: asString(json, 'total_payouts'),
        comments: asInt(json, 'comments'),
        topPosts: asInt(json, 'top_posts'),
      );
}
