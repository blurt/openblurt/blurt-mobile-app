import 'dart:convert';

import 'package:blurt/models/safe_convert.dart';

class BlurtPriceInfo {
  final double priceUsd;
  final double priceBtc;

  BlurtPriceInfo({
    this.priceUsd = 0.0,
    this.priceBtc = 0.0,
  });

  factory BlurtPriceInfo.fromJson(Map<String, dynamic>? json) => BlurtPriceInfo(
        priceUsd: asDouble(json, 'price_usd'),
        priceBtc: asDouble(json, 'price_btc'),
      );

  factory BlurtPriceInfo.fromJsonString(String jsonString) =>
      BlurtPriceInfo.fromJson(json.decode(jsonString));

  Map<String, dynamic> toJson() => {
        'price_usd': priceUsd,
        'price_btc': priceBtc,
      };
}
