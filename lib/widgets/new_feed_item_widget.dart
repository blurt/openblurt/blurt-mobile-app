import 'dart:developer';

import 'package:blurt/models/feed_response_item.dart';
import 'package:blurt/widgets/custom_circle_avatar.dart';
import 'package:flutter/material.dart';
import 'package:reading_time/reading_time.dart';
import 'package:timeago/timeago.dart' as timeago;

class FeedItemWidget extends StatelessWidget {
  const FeedItemWidget(
      {Key? key,
      required this.item,
      required this.onUserProfileTapped,
      required this.onPostTapped,
      required this.showUserAvatar})
      : super(key: key);
  final FeedResponseItem item;
  final Function onUserProfileTapped;
  final Function onPostTapped;
  final bool showUserAvatar;

  Widget _errorIndicator() {
    return Container(
      height: 160,
      decoration: BoxDecoration(
        image: DecorationImage(
          image: Image.asset('assets/icon44.png').image,
          fit: BoxFit.fitHeight,
        ),
      ),
    );
  }

  Widget _imageThumb(String url) {
    String url = "";
    if (item.meta?.images?.isNotEmpty == true) {
      url = 'https://imgp.blurt.world/320x160/${item.meta!.images!.first}';
    } else {
      url =
          'https://imgp.blurt.world/320x160/https://cdn.publish0x.com/prod/fs/images/f843764f9514e1194501f4c4f3a8356e6670cfd884841f851d69e26fa6fa3d6c.png';
    }
    return FadeInImage.assetNetwork(
      height: 160,
      fit: BoxFit.cover,
      placeholder: 'assets/icon44.png',
      image: url,
      placeholderErrorBuilder:
          (BuildContext context, Object error, StackTrace? stackTrace) {
        return _errorIndicator();
      },
      imageErrorBuilder:
          (BuildContext context, Object error, StackTrace? stackTrace) {
        return _errorIndicator();
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    var timeAgo =
        timeago.format(DateTime.tryParse(item.created ?? '') ?? DateTime.now());
    String readTime = readingTime(item.body ?? '').msg;
    readTime = readTime == "less than a minute" ? "1 min read" : readTime;
    return SizedBox(
      child: Card(
        child: Column(
          children: [
            showUserAvatar
                ? InkWell(
                    child: ListTile(
                      leading: CustomCircleAvatar(
                        url:
                            'https://imgp.blurt.world/profileimage/${item.author}/64x64',
                        width: 44,
                        height: 44,
                      ),
                      title: Text(item.author),
                    ),
                    onTap: () {
                      onUserProfileTapped();
                    },
                  )
                : Container(),
            InkWell(
              child: Column(
                children: [
                  ListTile(
                    title: item.meta?.images?.isNotEmpty == true
                        ? _imageThumb(item.meta!.images!.first)
                        : _errorIndicator(),
                  ),
                  ListTile(
                    title: Text(
                      item.title,
                      maxLines: 2,
                    ),
                    subtitle: Text(
                      'BLURT ${(item.payout).toStringAsFixed(3)} | ❤️ ${item.activeVotes.length ?? 0} | 💬 ${item.children ?? 0} | ⏱️ $timeAgo\nin #${item.meta?.tags.first ?? 'Blurt'} · 👁️ $readTime',
                      // textAlign: TextAlign.center,
                    ),
                  ),
                ],
              ),
              onTap: () {
                onPostTapped();
              },
            )
          ],
        ),
      ),
    );
  }
}
