import 'package:flutter/material.dart';

class CustomCircleAvatar extends StatelessWidget {
  const CustomCircleAvatar({
    Key? key,
    required this.height,
    required this.width,
    required this.url,
    this.borderColor = Colors.transparent,
  }) : super(key: key);
  final double height;
  final double width;
  final String url;
  final Color borderColor;

  @override
  Widget build(BuildContext context) {
    return Container(
      height: height,
      width: width,
      decoration: BoxDecoration(
        borderRadius: BorderRadius.all(Radius.circular(height / 2.0)),
        border: Border.all(color: borderColor),
      ),
      child: CircleAvatar(
        backgroundImage: NetworkImage(url),
        backgroundColor: Colors.transparent,
        radius: 100,
      ),
    );
  }
}
