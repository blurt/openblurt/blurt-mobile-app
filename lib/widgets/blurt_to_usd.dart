import 'package:blurt/communication/feeds/blurt_communicator.dart';
import 'package:flutter/material.dart';

class BlurtToUSDWidget extends StatefulWidget {
  const BlurtToUSDWidget({
    Key? key,
    required this.blurt,
    required this.domain,
  }) : super(key: key);
  final double blurt;
  final String domain;

  @override
  State<BlurtToUSDWidget> createState() => _BlurtToUSDWidgetState();
}

class _BlurtToUSDWidgetState extends State<BlurtToUSDWidget> {
  Future<double>? usdPrice;
  @override
  void initState() {
    super.initState();
    usdPrice = BlurtCommunicator().getPriceInUSD(widget.domain);
  }

  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
      future: usdPrice,
      builder: (builder, snapshot) {
        if (snapshot.hasError) {
          return Text("${widget.blurt.toStringAsFixed(3)} BLURT");
        } else if (snapshot.hasData &&
            snapshot.connectionState == ConnectionState.done) {
          var usdValue = snapshot.data as double;
          return Text(
              "${widget.blurt.toStringAsFixed(3)} BLURT · \$ ${(usdValue * widget.blurt).toStringAsFixed(3)}");
        } else {
          return Text("${widget.blurt.toStringAsFixed(3)} BLURT");
        }
      },
    );
  }
}
