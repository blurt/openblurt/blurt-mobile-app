import 'dart:convert';

import 'package:blurt/bloc/server.dart';
import 'package:blurt/communication/feeds/blurt_communicator.dart';
import 'package:blurt/models/blurt_user_stream.dart';
import 'package:blurt/screens/eula/eula_screen.dart';
import 'package:blurt/screens/home_screen.dart';
import 'package:flutter/material.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:http/http.dart' as http;
import 'package:provider/provider.dart';

class MyApp extends StatefulWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  State<MyApp> createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  static const storage = FlutterSecureStorage();

  @override
  Widget build(BuildContext context) {
    var initialData = BlurtAppData(
      domain: 'https://rpc.blurt.world/',
      isDarkMode: true,
      notificationDateTime: DateTime.parse('1969-07-20 20:18:04Z'),
      props: null,
      userData: null,
      didAcceptEULA: false,
      blockAndCoal: [],
    );
    return StreamProvider<BlurtAppData>.value(
      value: server.blurtAppData,
      initialData: initialData,
      child: const BlurtApp(),
    );
  }

  @override
  void initState() {
    super.initState();
    loadData();
  }

  http.Request getBlockedRequest(String domain, String forUser) {
    var request = http.Request('POST', Uri.parse(domain));
    request.body = json.encode({
      "jsonrpc": "2.0",
      "method": "call",
      "params": [
        "condenser_api",
        "get_following",
        [forUser, "", "ignore", 1000]
      ]
    });
    return request;
  }

  void loadData() async {
    String? username = await storage.read(key: 'username');
    String? postingKey = await storage.read(key: 'postingKey');
    String didAcceptEula = await storage.read(key: 'did_accept_eula') ?? 'no';
    String isDarkMode = await storage.read(key: 'is_dark_mode') ?? 'yes';
    String notification =
        await storage.read(key: 'notification') ?? '1969-07-20 20:18:04Z';
    String domain =
        await storage.read(key: 'domain') ?? 'https://rpc.blurt.world/';
    List<String> coalItems = await BlurtCommunicator().getCoalList();
    if (username != null) {
      var blocked = await BlurtCommunicator().getFollow(getBlockedRequest(
        domain,
        username,
      ));
      coalItems.addAll(blocked.map((e) => e.following).toList());
    }
    BlurtUserData? userData;
    if (username != null &&
        postingKey != null &&
        username.isNotEmpty &&
        postingKey.isNotEmpty) {
      userData = BlurtUserData(
        username: username,
        postingKey: postingKey,
      );
      didAcceptEula = 'yes';
    }
    server.updateAppData(
      BlurtAppData(
        isDarkMode: isDarkMode == 'yes',
        domain: domain,
        notificationDateTime: DateTime.parse(notification),
        props: null,
        userData: userData,
        didAcceptEULA: didAcceptEula == 'yes',
        blockAndCoal: coalItems,
      ),
    );
  }
}

class BlurtApp extends StatelessWidget {
  const BlurtApp({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    var data = Provider.of<BlurtAppData>(context);
    var lightTheme = ThemeData.light().copyWith(
      colorScheme: ColorScheme.fromSeed(seedColor: server.themeColor),
      primaryColor: server.themeColor,
      primaryColorDark: server.themeColor,
      primaryColorLight: server.themeColor,
      appBarTheme: AppBarTheme(
        color: server.themeColor,
      ),
      floatingActionButtonTheme: FloatingActionButtonThemeData(
        backgroundColor: server.themeColor,
      ),
    );
    var darkTheme = ThemeData.dark().copyWith(
      colorScheme: ColorScheme.fromSeed(seedColor: server.themeColor),
      primaryColor: server.themeColor,
      primaryColorDark: server.themeColor,
      primaryColorLight: server.themeColor,
      appBarTheme: AppBarTheme(
        color: server.themeColor,
      ),
      floatingActionButtonTheme: FloatingActionButtonThemeData(
        backgroundColor: server.themeColor,
      ),
    );
    return MaterialApp(
      title: 'Blurt',
      debugShowCheckedModeBanner: false,
      theme: data.isDarkMode ? darkTheme : lightTheme,
      home: data.didAcceptEULA
          ? const HomePage()
          : const BlurtAppEulaScreen(fromSettings: false),
    );
  }
}
