import 'dart:convert';
import 'dart:developer';

import 'package:blurt/bloc/server.dart';
import 'package:blurt/models/blurt_coal.dart';
import 'package:blurt/models/blurt_db_config_response.dart';
import 'package:blurt/models/blurt_price_info.dart';
import 'package:blurt/models/blurt_tags_details.dart';
import 'package:blurt/models/blurt_transfers.dart';
import 'package:blurt/models/blurt_user_witness_votes.dart';
import 'package:blurt/models/blurt_witness_info.dart';
import 'package:blurt/models/feed_response.dart';
import 'package:blurt/models/feed_response_item.dart';
import 'package:blurt/models/user_channel_followers_following.dart';
import 'package:blurt/models/user_channel_info.dart';
import 'package:blurt/models/user_notification.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:http/http.dart' as http;

class BlurtCommunicator {
  final String notificationServer = "https://notify.blurt.one";
  Future<List<FeedResponseItem>> getFeedItems(
    http.Request request,
    BlurtAppData appData,
  ) async {
    try {
      log('Loading feed - ${request.body.toString()}');
      http.StreamedResponse response = await request.send();
      if (response.statusCode == 200) {
        var string = await response.stream.bytesToString();
        var feedResponse = FeedResponse.fromJsonString(string);
        server.updateChainProps(feedResponse.result.props, appData);
        return feedResponse.result.items;
      } else {
        log('Status code - ${response.reasonPhrase}');
        throw 'Status code - ${response.reasonPhrase}';
      }
    } catch (e) {
      log('Error - ${e.toString()}');
      rethrow;
    }
  }

  Future<double> getPriceInUSD(String domain) async {
    try {
      var uri = Uri.parse('https://api.blurt.world/price_info');
      var response = await http.get(uri);
      if (response.statusCode == 200) {
        var string = response.body;
        return BlurtPriceInfo.fromJsonString(string).priceUsd;
      } else {
        log('Status code - ${response.reasonPhrase}');
        throw 'Status code - ${response.reasonPhrase}';
      }
    } catch (e) {
      log('Error getting coal - ${e.toString()}');
      rethrow;
    }
  }

  Future<List<BlurtUserWitnessVotesObjectVoteItem>> getWitnessVotes(
    String domain,
    String user,
  ) async {
    try {
      var request = http.Request('POST', Uri.parse(domain));
      request.body = json.encode({
        "jsonrpc": "2.0",
        "method": "database_api.list_witness_votes",
        "params": {
          "start": [user, ""],
          "limit": 1000,
          "order": "by_witness_account"
        }
      });
      http.StreamedResponse response = await request.send();
      if (response.statusCode == 200) {
        var string = await response.stream.bytesToString();
        var res = BlurtUserWitnessVotes.fromJsonString(string);
        return res.result.votes;
      } else {
        log('Status code - ${response.reasonPhrase}');
        throw 'Status code - ${response.reasonPhrase}';
      }
    } catch (e) {
      log('Error - ${e.toString()}');
      rethrow;
    }
  }

  Future<BlurtWitnessInfo> getWitnessInfo(
    String domain,
    String user,
  ) async {
    try {
      var request = http.Request('POST', Uri.parse(domain));
      request.body = json.encode({
        "jsonrpc": "2.0",
        "method": "call",
        "params": [
          "condenser_api",
          "get_witness_by_account",
          [user]
        ]
      });
      http.StreamedResponse response = await request.send();
      if (response.statusCode == 200) {
        var string = await response.stream.bytesToString();
        return BlurtWitnessInfo.fromJsonString(string);
      } else {
        log('Status code - ${response.reasonPhrase}');
        throw 'Status code - ${response.reasonPhrase}';
      }
    } catch (e) {
      log('Error - ${e.toString()}');
      rethrow;
    }
  }

  Future<List<BlurtUserWitnessVotesObjectVoteItem>> getGivenWitnessVotes(
    String domain,
    String user,
  ) async {
    try {
      var request = http.Request('POST', Uri.parse(domain));
      request.body = json.encode({
        "id": 1,
        "jsonrpc": "2.0",
        "method": "database_api.list_witness_votes",
        "params": {
          "start": [user, ""],
          "limit": 30,
          "order": "by_account_witness"
        }
      });
      http.StreamedResponse response = await request.send();
      if (response.statusCode == 200) {
        var string = await response.stream.bytesToString();
        var res = BlurtUserWitnessVotes.fromJsonString(string);
        return res.result.votes;
      } else {
        log('Status code - ${response.reasonPhrase}');
        throw 'Status code - ${response.reasonPhrase}';
      }
    } catch (e) {
      log('Error - ${e.toString()}');
      rethrow;
    }
  }

  Future<List<String>> getCoalList() async {
    try {
      var uri = Uri.parse('https://coal.blurtwallet.com/');
      var response = await http.get(uri);
      if (response.statusCode == 200) {
        var string = response.body;
        var feedResponse = coalItemsFromString(string);
        return feedResponse.map((e) => e.name).toList();
      } else {
        log('Status code - ${response.reasonPhrase}');
        throw 'Status code - ${response.reasonPhrase}';
      }
    } catch (e) {
      log('Error getting coal - ${e.toString()}');
      rethrow;
    }
  }

  Future<List<BlurtTagDetailsItem>> getTags(String domain) async {
    try {
      var request = http.Request('POST', Uri.parse(domain));
      request.body = json.encode({
        "id": 0,
        "jsonrpc": "2.0",
        "method": "call",
        "params": [
          "condenser_api",
          "get_state",
          [
            "tags",
          ]
        ]
      });
      http.StreamedResponse response = await request.send();
      if (response.statusCode == 200) {
        var string = await response.stream.bytesToString();
        var tags = BlurtTagDetails.fromJsonString(string);
        return tags.items;
      } else {
        log('Status code - ${response.reasonPhrase}');
        throw 'Status code - ${response.reasonPhrase}';
      }
    } catch (e) {
      log('Error - ${e.toString()}');
      rethrow;
    }
  }

  Future<int> getManaRegen(String domain) async {
    try {
      var headers = {'Content-Type': 'application/json'};
      var request = http.Request('POST', Uri.parse(domain));
      request.body = json.encode({
        "jsonrpc": "2.0",
        "method": "database_api.get_config",
      });
      request.headers.addAll(headers);
      http.StreamedResponse response = await request.send();
      if (response.statusCode == 200) {
        var string = await response.stream.bytesToString();
        return BlurtDBConfigResponse.fromJsonString(string)
            .result
            .blurtVotingManaRegenerationSeconds;
      } else {
        log('Status code - ${response.reasonPhrase}');
        throw 'Status code - ${response.reasonPhrase}';
      }
    } catch (e) {
      log('Error - ${e.toString()}');
      rethrow;
    }
  }

  Future<List<UserChannelFollowItem>> getFollow(http.Request request) async {
    try {
      http.StreamedResponse response = await request.send();
      if (response.statusCode == 200) {
        var string = await response.stream.bytesToString();
        var feedResponse = UserChannelFollow.fromJsonString(string);
        return feedResponse.result;
      } else {
        log('Status code - ${response.reasonPhrase}');
        throw 'Status code - ${response.reasonPhrase}';
      }
    } catch (e) {
      log('Error - ${e.toString()}');
      rethrow;
    }
  }

  Future<List<UserNotificationItem>> getNotifications(
    String username,
    BlurtAppData appData,
  ) async {
    try {
      var request = http.Request(
          'GET', Uri.parse('$notificationServer/get_notifications/$username'));
      http.StreamedResponse response = await request.send();
      if (response.statusCode == 200) {
        var string = await response.stream.bytesToString();
        var notifications = userNotificationItemsfromJsonString(string);
        if (notifications.isNotEmpty) {
          var datetime = DateTime.fromMillisecondsSinceEpoch(
            notifications[0].timestamp * 1000,
          );
          const storage = FlutterSecureStorage();
          await storage.write(
              key: 'notification', value: datetime.toIso8601String());
          server.changeNotificationDateTimeRead(datetime, appData);
        }
        return notifications;
      } else {
        log('Status code - ${response.reasonPhrase}');
        throw 'Status code - ${response.reasonPhrase}';
      }
    } catch (e) {
      log('Error - ${e.toString()}');
      rethrow;
    }
  }

  Future<UserChannelInfoResponse> getUserChannelInfo(
    http.Request request,
    String name,
  ) async {
    try {
      http.StreamedResponse response = await request.send();
      if (response.statusCode == 200) {
        var string = await response.stream.bytesToString();
        var data = UserChannelInfoResponse.fromJsonString(string, name);
        return data;
      } else {
        log('Status code - ${response.reasonPhrase}');
        throw 'Status code - ${response.reasonPhrase}';
      }
    } catch (e) {
      log('Error - ${e.toString()}');
      rethrow;
    }
  }

  Future<List<FeedResponseItem>> getFeedMoreItems(http.Request request) async {
    try {
      http.StreamedResponse response = await request.send();
      if (response.statusCode == 200) {
        var string = await response.stream.bytesToString();
        var feedResponse = FeedMoreResponse.fromJsonString(string);
        return feedResponse.result;
      } else {
        log('Status code - ${response.reasonPhrase}');
        throw 'Status code - ${response.reasonPhrase}';
      }
    } catch (e) {
      log('Error - ${e.toString()}');
      rethrow;
    }
  }

  Future<BlurtTransferResponse> getTransferHistory(
      String username, String domain) async {
    try {
      var headers = {'Content-Type': 'application/json'};
      var request = http.Request('POST', Uri.parse(domain));
      request.body = json.encode({
        "id": 1,
        "jsonrpc": "2.0",
        "method": "call",
        "params": [
          "condenser_api",
          "get_state",
          ["/@$username/transfers"]
        ]
      });
      request.headers.addAll(headers);
      http.StreamedResponse response = await request.send();
      if (response.statusCode == 200) {
        var string = await response.stream.bytesToString();
        return BlurtTransferResponse.fromString(string, username);
      } else {
        log('Status code - ${response.reasonPhrase}');
        throw 'Status code - ${response.reasonPhrase}';
      }
    } catch (e) {
      log('Error - ${e.toString()}');
      rethrow;
    }
  }
}
