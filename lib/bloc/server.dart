import 'dart:async';

import 'package:blurt/models/blurt_user_stream.dart';
import 'package:blurt/models/feed_response.dart';
import 'package:flutter/material.dart';

class BlurtAppData {
  bool isDarkMode;
  String domain;
  DateTime notificationDateTime;
  ChainProps? props;
  BlurtUserData? userData;
  bool didAcceptEULA;
  List<String> blockAndCoal;

  BlurtAppData({
    required this.isDarkMode,
    required this.domain,
    required this.notificationDateTime,
    required this.props,
    required this.userData,
    required this.didAcceptEULA,
    required this.blockAndCoal,
  });
}

class Server {
  final _controller = StreamController<BlurtAppData>();
  final themeColor = Colors.deepOrange;

  Stream<BlurtAppData> get blurtAppData {
    return _controller.stream;
  }

  void updateEULAState(BlurtAppData appData) {
    updateAppData(
      BlurtAppData(
        isDarkMode: appData.isDarkMode,
        domain: appData.domain,
        notificationDateTime: appData.notificationDateTime,
        props: appData.props,
        userData: appData.userData,
        didAcceptEULA: true,
        blockAndCoal: appData.blockAndCoal,
      ),
    );
  }

  void changeDomain(String domain, BlurtAppData appData) {
    updateAppData(
      BlurtAppData(
        isDarkMode: appData.isDarkMode,
        domain: domain,
        notificationDateTime: appData.notificationDateTime,
        props: appData.props,
        userData: appData.userData,
        didAcceptEULA: appData.didAcceptEULA,
        blockAndCoal: appData.blockAndCoal,
      ),
    );
  }

  void changeNotificationDateTimeRead(DateTime dateTime, BlurtAppData appData) {
    updateAppData(
      BlurtAppData(
        isDarkMode: appData.isDarkMode,
        domain: appData.domain,
        notificationDateTime: dateTime,
        props: appData.props,
        userData: appData.userData,
        didAcceptEULA: appData.didAcceptEULA,
        blockAndCoal: appData.blockAndCoal,
      ),
    );
  }

  void changeTheme(bool value, BlurtAppData appData) {
    updateAppData(
      BlurtAppData(
        isDarkMode: value,
        domain: appData.domain,
        notificationDateTime: appData.notificationDateTime,
        props: appData.props,
        userData: appData.userData,
        didAcceptEULA: appData.didAcceptEULA,
        blockAndCoal: appData.blockAndCoal,
      ),
    );
  }

  void updateUserData(BlurtUserData? data, BlurtAppData appData) {
    updateAppData(
      BlurtAppData(
        isDarkMode: appData.isDarkMode,
        domain: appData.domain,
        notificationDateTime: data == null
            ? DateTime.parse('1969-07-20 20:18:04Z')
            : appData.notificationDateTime,
        props: appData.props,
        userData: data,
        didAcceptEULA: appData.didAcceptEULA,
        blockAndCoal: appData.blockAndCoal,
      ),
    );
  }

  void updateChainProps(ChainProps? props, BlurtAppData appData) {
    updateAppData(
      BlurtAppData(
        isDarkMode: appData.isDarkMode,
        domain: appData.domain,
        notificationDateTime: appData.notificationDateTime,
        props: props,
        userData: appData.userData,
        didAcceptEULA: appData.didAcceptEULA,
        blockAndCoal: appData.blockAndCoal,
      ),
    );
  }

  void updateBlockAndCoal(List<String> blackAndCoal, BlurtAppData appData) {
    updateAppData(
      BlurtAppData(
        isDarkMode: appData.isDarkMode,
        domain: appData.domain,
        notificationDateTime: appData.notificationDateTime,
        props: appData.props,
        userData: appData.userData,
        didAcceptEULA: appData.didAcceptEULA,
        blockAndCoal: blackAndCoal,
      ),
    );
  }

  void updateAppData(BlurtAppData appData) {
    _controller.sink.add(appData);
  }
}

Server server = Server();
