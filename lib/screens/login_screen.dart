import 'package:blurt/bloc/server.dart';
import 'package:blurt/models/blurt_user_stream.dart';
import 'package:blurt/models/login_bridge_response.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:provider/provider.dart';

class LoginScreen extends StatefulWidget {
  const LoginScreen({Key? key}) : super(key: key);

  @override
  State<LoginScreen> createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  var isLoading = false;
  static const platform = MethodChannel('blog.blurt.blurt/auth');
  var username = '';
  var postingKey = '';

  // Create storage
  static const storage = FlutterSecureStorage();

  void onLoginTapped(String domain, BlurtAppData appData) async {
    setState(() {
      isLoading = true;
    });
    try {
      final String result = await platform.invokeMethod('validate', {
        'username': username,
        'postingKey': postingKey,
        'domain': domain,
      });
      var response = LoginBridgeResponse.fromJsonString(result);
      if (response.valid && response.error.isEmpty) {
        debugPrint("Successful login");
        await storage.write(key: 'username', value: username);
        await storage.write(key: 'postingKey', value: postingKey);
        server.updateUserData(
          BlurtUserData(
            username: username,
            postingKey: postingKey,
          ),
          appData,
        );
        setState(() {
          isLoading = false;
          Navigator.of(context).pop();
        });
      } else {
        setState(() {
          isLoading = false;
          showError(response.error);
        });
      }
    } catch (e) {
      setState(() {
        isLoading = false;
      });
      showError('Error occurred - ${e.toString()}');
    }
  }

  void showError(String string) {
    var snackBar = SnackBar(content: Text('Error: $string'));
    ScaffoldMessenger.of(context).showSnackBar(snackBar);
  }

  Widget _loginForm(String domain, bool isDarkMode, BlurtAppData appData) {
    return Container(
      margin: const EdgeInsets.all(10),
      child: Column(
        children: [
          TextField(
            decoration: InputDecoration(
              icon: Icon(
                Icons.person,
                color: isDarkMode ? Colors.white : Colors.black,
              ),
              label: const Text('Blurt Username'),
              hintText: 'Enter Blurt username here',
            ),
            cursorColor: server.themeColor,
            autocorrect: false,
            onChanged: (value) {
              setState(() {
                username = value;
              });
            },
            enabled: isLoading ? false : true,
          ),
          const SizedBox(height: 20),
          TextField(
            decoration: InputDecoration(
              icon: Icon(
                Icons.key,
                color: isDarkMode ? Colors.white : Colors.black,
              ),
              label: const Text('Blurt Posting Key'),
            ),
            obscureText: true,
            onChanged: (value) {
              setState(() {
                postingKey = value;
              });
            },
            enabled: isLoading ? false : true,
          ),
          const SizedBox(height: 20),
          isLoading
              ? const CircularProgressIndicator()
              : ElevatedButton(
                  style: ButtonStyle(
                    backgroundColor:
                        MaterialStateProperty.all(server.themeColor),
                  ),
                  onPressed: () {
                    onLoginTapped(domain, appData);
                  },
                  child: const Text('Log in'),
                ),
        ],
      ),
    );
  }

  void showMessage(String string) {
    var snackBar = SnackBar(content: Text(string));
    ScaffoldMessenger.of(context).showSnackBar(snackBar);
  }

  @override
  Widget build(BuildContext context) {
    var data = Provider.of<BlurtAppData>(context);
    var domain = data.domain;
    var isDarkMode = data.isDarkMode;
    return Scaffold(
      appBar: AppBar(
        title: const Text('Login'),
        actions: [
          IconButton(
            onPressed: () {
              showMessage(
                  'Worried about security? Please do NOT.\nYour posting key securely stored on your device ONLY.\nYou are entering your posting key here, so that you do not have to enter it over and over, for your every action on Blurt.');
            },
            icon: const Icon(Icons.help),
          )
        ],
      ),
      body: _loginForm(domain, isDarkMode, data),
    );
  }
}
