import 'dart:io';

import 'package:blurt/bloc/server.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart' show rootBundle;
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:provider/provider.dart';

class BlurtAppEulaScreen extends StatefulWidget {
  const BlurtAppEulaScreen({
    Key? key,
    required this.fromSettings,
  }) : super(key: key);
  final bool fromSettings;

  @override
  State<BlurtAppEulaScreen> createState() => _BlurtAppEulaScreenState();
}

class _BlurtAppEulaScreenState extends State<BlurtAppEulaScreen> {
  var text = '';

  @override
  void initState() {
    super.initState();
    readFile();
  }

  void readFile() async {
    var data = await rootBundle.loadString(
      'assets/eula/${Platform.isAndroid ? 'androidEULA.md' : 'iosEULA.md'}',
    );
    setState(() {
      text = data;
    });
  }

  @override
  Widget build(BuildContext context) {
    var data = Provider.of<BlurtAppData>(context);
    return Scaffold(
      appBar: AppBar(
        title: const Text('End User License Agreement'),
      ),
      body: SafeArea(
        child: Container(
          margin: const EdgeInsets.all(10),
          child: SingleChildScrollView(
            child: Column(
              children: [
                Text(text),
                widget.fromSettings
                    ? Container()
                    : ElevatedButton(
                        onPressed: () async {
                          const storage = FlutterSecureStorage();
                          storage.write(key: 'did_accept_eula', value: 'yes');
                          server.updateEULAState(data);
                        },
                        child: const Text("I Agree"),
                      )
              ],
            ),
          ),
        ),
      ),
    );
  }
}
