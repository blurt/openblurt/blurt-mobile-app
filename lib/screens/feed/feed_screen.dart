import 'dart:developer';

import 'package:blurt/bloc/server.dart';
import 'package:blurt/communication/feeds/blurt_communicator.dart';
import 'package:blurt/models/blurt_user_stream.dart';
import 'package:blurt/models/drawer_item.dart';
import 'package:blurt/models/feed_response_item.dart';
import 'package:blurt/screens/add_a_post/add_post.dart';
import 'package:blurt/screens/post_details/post_details.dart';
import 'package:blurt/screens/user_channel/user_channel.dart';
import 'package:blurt/widgets/custom_circle_avatar.dart';
import 'package:blurt/widgets/fab_custom.dart';
import 'package:blurt/widgets/fab_overlay.dart';
import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:provider/provider.dart';
import 'package:reading_time/reading_time.dart';
import 'package:timeago/timeago.dart' as timeago;

enum FeedStyle {
  page,
  list,
}

class FeedScreen extends StatefulWidget {
  const FeedScreen({
    Key? key,
    required this.item,
  }) : super(key: key);
  final DrawerItem item;

  @override
  State<FeedScreen> createState() => _FeedScreenState();
}

class _FeedScreenState extends State<FeedScreen>
    with AutomaticKeepAliveClientMixin<FeedScreen> {
  @override
  bool get wantKeepAlive => true;
  Future<void>? loadData;
  List<FeedResponseItem> items = [];
  var fetchingExtra = false;
  String widgetName = '';
  bool isMenuOpen = false;
  FeedStyle feedStyle = FeedStyle.page;
  var noMoreRecords = false;

  Widget _errorState() {
    return Column(
      children: [
        const Spacer(),
        Row(children: const [
          Spacer(),
          Text(
            'Oops! Something went wrong.\nPlease Try again.',
            textAlign: TextAlign.center,
          ),
          Spacer(),
        ]),
        const SizedBox(height: 20),
        ElevatedButton(
          onPressed: () {
            setState(() {
              loadData = null;
            });
          },
          child: const Text('Retry'),
        ),
        const Spacer(),
      ],
    );
  }

  BoxDecoration _cardBoxDecoration() {
    var data = Provider.of<BlurtAppData>(context, listen: false);
    return BoxDecoration(
      color: data.isDarkMode ? Colors.black54 : Colors.white70,
      boxShadow: [
        BoxShadow(
          color: data.isDarkMode ? Colors.white10 : Colors.black12,
          spreadRadius: 3,
          blurRadius: 3,
        )
      ],
    );
  }

  Widget _postAuthorInfo(FeedResponseItem item) {
    String timeInString = " · Posted ${timeago.format(item.createdAt)}";
    var avatar = CustomCircleAvatar(
      height: 45,
      width: 45,
      url: 'https://imgp.blurt.world/profileimage/${item.author}/64x64',
      borderColor: Colors.deepOrange,
    );
    var children = [
      avatar,
      const SizedBox(width: 3),
      Text('${item.author}$timeInString'),
    ];

    Widget reBlurted;
    if (item.firstReBloggedBy != null && item.firstReBloggedBy!.isNotEmpty) {
      reBlurted = Row(
        children: [
          const Icon(Icons.sync),
          const SizedBox(width: 5),
          Text('${item.firstReBloggedBy} ReBlurted'),
        ],
      );
    } else {
      reBlurted = Container();
    }

    return InkWell(
      onTap: () {
        var screen = UserChannel(name: item.author);
        var route = MaterialPageRoute(builder: (c) => screen);
        Navigator.of(context).push(route);
      },
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          reBlurted,
          const SizedBox(height: 3),
          Row(
            children: children,
          )
        ],
      ),
    );
  }

  Widget _postImage(FeedResponseItem item) {
    var width = MediaQuery.of(context).size.width.toInt() - 40;
    String url = "";
    if (item.meta?.images?.isNotEmpty == true) {
      url = 'https://imgp.blurt.world/${width}x220/${item.meta!.images!.first}';
    } else {
      url =
          'https://imgp.blurt.world/${width}x220/https://cdn.publish0x.com/prod/fs/images/f843764f9514e1194501f4c4f3a8356e6670cfd884841f851d69e26fa6fa3d6c.png';
    }
    return ConstrainedBox(
      constraints: const BoxConstraints(maxHeight: 220),
      child: Image.network(url),
    );
  }

  Widget _postImageForList(FeedResponseItem item) {
    String url = "";
    if (item.meta?.images?.isNotEmpty == true) {
      url = 'https://imgp.blurt.world/400x400/${item.meta!.images!.first}';
    } else {
      url =
          'https://imgp.blurt.world/400x400/https://cdn.publish0x.com/prod/fs/images/f843764f9514e1194501f4c4f3a8356e6670cfd884841f851d69e26fa6fa3d6c.png';
    }
    return SizedBox(
      width: MediaQuery.of(context).size.width,
      height: 200,
      child: FittedBox(
        fit: BoxFit.fill,
        child: Image.network(url),
      ),
    );
  }

  Widget _postDetailsContent(FeedResponseItem item) {
    String readTime = readingTime(item.body).msg;
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        const SizedBox(height: 10),
        _postImage(item),
        const SizedBox(height: 10),
        Text(
          item.title.length > 80 ? item.title.substring(0, 80) : item.title,
          style: Theme.of(context).textTheme.headline6,
          textAlign: TextAlign.center,
        ),
        const SizedBox(height: 10),
        Text(
          'BLURT ${item.payout} ·  ❤️ ${item.netVotes} likes · 📝 ${item.children} comments\n\n🏷️ ${item.meta?.tags.join(', ') ?? ''}\n\n🕵️ Read time: $readTime',
          textAlign: TextAlign.center,
        ),
      ],
    );
  }

  Widget _postDetailsContentForList(FeedResponseItem item) {
    return Container(
      decoration: const BoxDecoration(color: Colors.black87),
      width: double.infinity,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            "  ${item.title}",
            maxLines: 1,
            style: const TextStyle(
              color: Colors.deepOrange,
              fontSize: 18,
              fontWeight: FontWeight.w800,
            ),
          ),
          Text(
            '  ${item.pendingPayoutValue} · ❤️ ${item.netVotes} · 🔖 ${item.category} · 👤 ${item.author}',
            maxLines: 2,
            style: const TextStyle(
              color: Colors.deepOrange,
              fontSize: 14,
              fontWeight: FontWeight.bold,
            ),
          )
        ],
      ),
    );
  }

  Widget _fullPost(FeedResponseItem item) {
    return Container(
      width: MediaQuery.of(context).size.width,
      decoration: _cardBoxDecoration(),
      child: Container(
        margin: const EdgeInsets.all(10),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            _postAuthorInfo(item),
            InkWell(
              onTap: () {
                var screen =
                    PostDetails(author: item.author, permlink: item.permlink);
                var route = MaterialPageRoute(builder: (c) => screen);
                Navigator.of(context).push(route);
              },
              child: _postDetailsContent(item),
            ),
          ],
        ),
      ),
    );
  }

  Widget carouselFrom(List<FeedResponseItem> items, BlurtAppData appData) {
    if (items.isEmpty) {
      return Center(
        child: Column(
          children: const [
            Spacer(),
            Text(
              'It is so lonely here.\nWe did not find anything to show here.',
              textAlign: TextAlign.center,
            ),
            SizedBox(height: 10),
            Icon(Icons.heart_broken),
            Spacer(),
          ],
        ),
      );
    }
    return CarouselSlider(
      options: CarouselOptions(
        height: MediaQuery.of(context).size.height,
        enableInfiniteScroll: false,
        viewportFraction: 0.95,
        scrollDirection: Axis.vertical,
        onPageChanged: (index, reason) {
          log('index is $index');
          if (items.isNotEmpty && index == items.length - 1 && !fetchingExtra) {
            loadMoreFeedItems(
              items[index].author,
              items[index].permlink,
              appData,
            );
          }
        },
      ),
      items: items.map((item) {
        return Builder(
          builder: (BuildContext context) {
            return _fullPost(item);
          },
        );
      }).toList(),
    );
  }

  Widget _listItemAuthor(String author) {
    return Column(
      children: [
        const SizedBox(height: 5),
        Row(
          children: [
            const Spacer(),
            InkWell(
              child: CustomCircleAvatar(
                height: 45,
                width: 45,
                url: 'https://imgp.blurt.world/profileimage/$author/64x64',
                borderColor: Colors.deepOrange,
              ),
              onTap: () {
                var screen = UserChannel(name: author);
                var route = MaterialPageRoute(builder: (c) => screen);
                Navigator.of(context).push(route);
              },
            ),
            const SizedBox(width: 5),
          ],
        ),
      ],
    );
  }

  Widget _listItem(FeedResponseItem item, BlurtAppData appData) {
    return InkWell(
      child: Stack(
        children: [
          _postImageForList(item),
          _postDetailsContentForList(item),
          _listItemAuthor(item.author),
        ],
      ),
      onTap: () {
        var screen = PostDetails(author: item.author, permlink: item.permlink);
        var route = MaterialPageRoute(builder: (c) => screen);
        Navigator.of(context).push(route);
      },
    );
  }

  Widget listFrom(List<FeedResponseItem> items, BlurtAppData appData) {
    var screenWidth = MediaQuery.of(context).size.width;
    var gridCount = screenWidth.toInt() / 340;
    return Container(
      margin: const EdgeInsets.only(top: 5),
      child: screenWidth > 600
          ? GridView.builder(
              gridDelegate: SliverGridDelegateWithMaxCrossAxisExtent(
                maxCrossAxisExtent: screenWidth / gridCount.toInt().toDouble(),
                childAspectRatio: 2 / 1.1,
                crossAxisSpacing: 5,
                mainAxisSpacing: 5,
              ),
              itemCount: items.length,
              itemBuilder: (c, i) {
                return _listItem(items[i], appData);
              },
            )
          : ListView.separated(
              itemBuilder: (c, i) {
                return _listItem(items[i], appData);
              },
              separatorBuilder: (c, i) => const Divider(
                height: 10,
                color: Colors.transparent,
              ),
              itemCount: items.length,
            ),
    );
  }

  Widget _futureFeed(BlurtAppData appData) {
    return FutureBuilder(
      future: loadData,
      builder: (builder, snapshot) {
        if (snapshot.hasError) {
          return _errorState();
        } else if (snapshot.connectionState == ConnectionState.done) {
          if (feedStyle == FeedStyle.page) {
            return carouselFrom(items, appData);
          } else {
            return listFrom(items, appData);
          }
        } else {
          return const Center(child: CircularProgressIndicator());
        }
      },
    );
  }

  Future<void> loadInitialFeed(
      http.Request request, BlurtAppData appData) async {
    var records = await BlurtCommunicator().getFeedItems(request, appData);
    var data = records;
    data.sort((a, b) {
      var aTime = a.createdAt;
      var bTime = b.createdAt;
      if (aTime.isAfter(bTime)) {
        return -1;
      } else if (bTime.isAfter(aTime)) {
        return 1;
      } else {
        return 0;
      }
    });
    setState(() {
      items = data.where((element) {
        return appData.blockAndCoal.contains(element.author) == false ||
            (element.meta?.tags ?? []).contains('nsfw');
      }).toList();
    });
  }

  void loadMoreFeedItems(
    String author,
    String permlink,
    BlurtAppData appData,
  ) async {
    var request = widget.item.getRequestForNextPage(context, author, permlink);
    if (request != null) {
      setState(() {
        fetchingExtra = true;
      });
      var records = await BlurtCommunicator().getFeedMoreItems(request);
      var itemsIds = items.map((e) => e.id);
      var newRecords = records.where((e) => !itemsIds.contains(e.id)).toList();
      newRecords.sort((a, b) {
        var aTime = a.createdAt;
        var bTime = b.createdAt;
        if (aTime.isAfter(bTime)) {
          return -1;
        } else if (bTime.isAfter(aTime)) {
          return 1;
        } else {
          return 0;
        }
      });
      newRecords.where((element) {
        return appData.blockAndCoal.contains(element.author) == false ||
            (element.meta?.tags ?? []).contains('nsfw');
      }).toList();
      setState(() {
        items += newRecords;
        noMoreRecords = newRecords.isEmpty;
        fetchingExtra = false;
      });
    }
  }

  List<FabOverItemData> _fabItems(BlurtUserData? user) {
    var displayStyle = feedStyle == FeedStyle.page
        ? FabOverItemData(
            displayName: 'Grid View',
            icon: Icons.list,
            onTap: () {
              setState(() {
                isMenuOpen = false;
                feedStyle = FeedStyle.list;
              });
            },
          )
        : FabOverItemData(
            displayName: 'One Page View',
            icon: Icons.note_outlined,
            onTap: () {
              setState(() {
                isMenuOpen = false;
                feedStyle = FeedStyle.page;
              });
            },
          );
    var fabItems = [
      displayStyle,
      FabOverItemData(
        displayName: 'Refresh',
        icon: Icons.refresh,
        onTap: () {
          setState(() {
            isMenuOpen = false;
            loadData = null;
          });
        },
      ),
    ];
    if (user != null) {
      fabItems.add(
        FabOverItemData(
          displayName: 'Add a Post',
          icon: Icons.add,
          onTap: () {
            setState(() {
              isMenuOpen = false;
              var screen = AddPostScreen(
                parentAuthor: null,
                parentPermlink: null,
                onDone: () {},
              );
              var route = MaterialPageRoute(builder: (c) => screen);
              Navigator.of(context).push(route);
            });
          },
        ),
      );
    }
    fabItems.add(
      FabOverItemData(
        displayName: 'Close',
        icon: Icons.close,
        onTap: () {
          setState(() {
            isMenuOpen = false;
          });
        },
      ),
    );
    return fabItems;
  }

  Widget _fabContainer(BlurtUserData? user) {
    if (!isMenuOpen) {
      return FabCustom(
        icon: Icons.bolt,
        onTap: () {
          setState(() {
            isMenuOpen = true;
          });
        },
      );
    }
    return FabOverlay(
      items: _fabItems(user),
      onBackgroundTap: () {
        setState(() {
          isMenuOpen = false;
        });
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    super.build(context);
    var appData = Provider.of<BlurtAppData>(context);
    var request = widget.item.getRequest(context);
    if ((loadData == null || widgetName != widget.item.name) &&
        request != null) {
      setState(() {
        widgetName = widget.item.name;
        loadData = loadInitialFeed(request, appData);
      });
    }
    return SafeArea(
      child: Stack(
        children: [
          _futureFeed(appData),
          _fabContainer(appData.userData),
        ],
      ),
    );
  }
}
