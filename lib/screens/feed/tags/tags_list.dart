import 'package:blurt/bloc/server.dart';
import 'package:blurt/communication/feeds/blurt_communicator.dart';
import 'package:blurt/models/blurt_tags_details.dart';
import 'package:blurt/models/drawer_item.dart';
import 'package:blurt/screens/feed/tags/tags_sort_options.dart';
import 'package:blurt/screens/feed/tags_feed.dart';
import 'package:blurt/widgets/error_state_widget.dart';
import 'package:blurt/widgets/fab_custom.dart';
import 'package:blurt/widgets/fab_overlay.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:provider/provider.dart';

class TagsListScreen extends StatefulWidget {
  const TagsListScreen({
    Key? key,
    required this.domain,
  }) : super(key: key);
  final String domain;

  @override
  State<TagsListScreen> createState() => _TagsListScreenState();
}

class _TagsListScreenState extends State<TagsListScreen>
    with AutomaticKeepAliveClientMixin<TagsListScreen> {
  @override
  bool get wantKeepAlive => true;
  late Future<List<BlurtTagDetailsItem>> loadData;
  var fetchingExtra = false;
  final DrawerItem item = DrawerItem(name: "Tags", icon: Icons.tag);

  var sortBy = 'name';
  var sortOrder = 'asc';
  var isMenuOpen = false;

  @override
  void initState() {
    super.initState();
    loadData = BlurtCommunicator().getTags(widget.domain);
  }

  Widget _errorState() {
    return ErrorStateWidget(
      onRetry: () {
        setState(() {
          loadData = BlurtCommunicator().getTags(widget.domain);
        });
      },
    );
  }

  List<FabOverItemData> _fabItems() {
    var fabItems = [
      FabOverItemData(
        displayName: 'Refresh',
        icon: Icons.refresh,
        onTap: () {
          setState(() {
            isMenuOpen = false;
            loadData = BlurtCommunicator().getTags(widget.domain);
          });
        },
      ),
      FabOverItemData(
        displayName: 'Change Sort',
        icon: Icons.sort_by_alpha,
        onTap: () {
          setState(() {
            isMenuOpen = false;
            showBottomSheet();
          });
        },
      ),
    ];
    fabItems.add(
      FabOverItemData(
        displayName: 'Close',
        icon: Icons.close,
        onTap: () {
          setState(() {
            isMenuOpen = false;
          });
        },
      ),
    );
    return fabItems;
  }

  Widget _fabContainer() {
    if (!isMenuOpen) {
      return FabCustom(
        icon: Icons.bolt,
        onTap: () {
          setState(() {
            isMenuOpen = true;
          });
        },
      );
    }
    return FabOverlay(
      items: _fabItems(),
      onBackgroundTap: () {
        setState(() {
          isMenuOpen = false;
        });
      },
    );
  }

  Widget _listView(bool isDarkMode, List<BlurtTagDetailsItem> items) {
    var color = isDarkMode ? Colors.white : Colors.black;
    items.sort((a, b) {
      if (sortBy == 'name') {
        if (sortOrder == 'desc') {
          return b.name.toLowerCase().compareTo(a.name.toLowerCase());
        } else {
          return a.name.toLowerCase().compareTo(b.name.toLowerCase());
        }
      } else if (sortBy == 'payout') {
        if (sortOrder == 'asc') {
          return a.payout.compareTo(b.payout);
        } else {
          return b.payout.compareTo(a.payout);
        }
      } else if (sortBy == 'post') {
        if (sortOrder == 'asc') {
          return a.topPosts.compareTo(b.topPosts);
        } else {
          return b.topPosts.compareTo(a.topPosts);
        }
      } else {
        if (sortOrder == 'asc') {
          return a.comments.compareTo(b.comments);
        } else {
          return b.comments.compareTo(a.comments);
        }
      }
    });
    var formatter = NumberFormat();
    return Container(
      margin: const EdgeInsets.only(top: 10),
      child: ListView.separated(
        itemBuilder: (c, i) {
          var item = items[i];
          return ListTile(
            leading: Icon(Icons.tag, color: color),
            title: Text(item.name.isEmpty ? 'No Tag' : item.name),
            trailing: Column(
              crossAxisAlignment: CrossAxisAlignment.end,
              children: [
                Text("${formatter.format(item.payout)} BLURT"),
                Text('${item.comments} Comments'),
                Text('${item.topPosts} Posts'),
              ],
            ),
            onTap: () {
              var hotFeed = TagsFeedScreen(
                item: DrawerItem(
                    name: "hot/${item.name}",
                    icon: Icons.local_fire_department_outlined),
              );
              var route = MaterialPageRoute(builder: (c) => hotFeed);
              Navigator.of(context).push(route);
            },
          );
        },
        separatorBuilder: (c, i) => const Divider(),
        itemCount: items.length,
      ),
    );
  }

  Widget _futureFeed(bool isDarkMode) {
    return FutureBuilder(
      future: loadData,
      builder: (builder, snapshot) {
        if (snapshot.hasError) {
          return _errorState();
        } else if (snapshot.connectionState == ConnectionState.done) {
          return _listView(
            isDarkMode,
            snapshot.data as List<BlurtTagDetailsItem>,
          );
        } else {
          return const Center(child: CircularProgressIndicator());
        }
      },
    );
  }

  void showBottomSheet() {
    showModalBottomSheet(
      context: context,
      builder: (builder) {
        return TagsSortOptionScreen(
          sortBy: sortBy,
          sortOrder: sortOrder,
          result: (sortBy, sortOrder) {
            setState(() {
              this.sortBy = sortBy;
              this.sortOrder = sortOrder;
            });
          },
        );
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    super.build(context);
    var data = Provider.of<BlurtAppData>(context);
    var isDarkMode = data.isDarkMode;
    return SafeArea(
      child: Stack(
        children: [
          _futureFeed(isDarkMode),
          _fabContainer(),
        ],
      ),
    );
  }
}
