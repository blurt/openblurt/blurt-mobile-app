import 'package:flutter/material.dart';

class TagsSortOptionScreen extends StatefulWidget {
  const TagsSortOptionScreen({
    Key? key,
    required this.sortBy,
    required this.sortOrder,
    required this.result,
  }) : super(key: key);
  final String sortOrder;
  final String sortBy;
  final Function(String, String) result;

  @override
  State<TagsSortOptionScreen> createState() => _TagsSortOptionScreenState();
}

class _TagsSortOptionScreenState extends State<TagsSortOptionScreen> {
  late String sortOrder;
  late String sortBy;

  @override
  void initState() {
    super.initState();
    sortOrder = widget.sortOrder;
    sortBy = widget.sortBy;
  }

  List<Widget> _getSortOption(String title, String option, IconData icon) {
    return [
      Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Row(
            children: [
              Icon(icon),
              const SizedBox(width: 5),
              Text(title),
            ],
          ),
          Checkbox(
            value: sortBy == option,
            activeColor: Colors.deepOrange,
            onChanged: (newValue) {
              if (newValue != null) {
                setState(() {
                  sortBy = newValue ? option : sortBy;
                });
              }
            },
          ),
        ],
      ),
      const SizedBox(width: 10),
    ];
  }

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: 275,
      child: Scaffold(
        appBar: AppBar(
          title: const Text('Select Sort Options'),
          actions: [
            IconButton(
              onPressed: () {
                widget.result(sortBy, sortOrder);
                Navigator.of(context).pop();
              },
              icon: const Icon(Icons.check),
            )
          ],
        ),
        body: Column(
          children: [
            const SizedBox(height: 10),
            Text(
              'Sort by:',
              style: Theme.of(context).textTheme.headline6,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children:
                  _getSortOption('Payouts', 'payout', Icons.attach_money) +
                      _getSortOption('Name', 'name', Icons.sort_by_alpha) +
                      _getSortOption('Posts', 'post', Icons.newspaper) +
                      _getSortOption('Comments', 'comment', Icons.comment),
            ),
            const SizedBox(height: 10),
            Text(
              'Sort Order:',
              style: Theme.of(context).textTheme.bodyMedium,
            ),
            Row(
              children: [
                const Spacer(),
                const Icon(Icons.expand_more),
                const SizedBox(width: 5),
                const Text('Ascending'),
                Checkbox(
                  value: sortOrder == 'asc',
                  activeColor: Colors.deepOrange,
                  onChanged: (newValue) {
                    if (newValue != null) {
                      setState(() {
                        sortOrder = newValue ? 'asc' : 'desc';
                      });
                    }
                  },
                ),
                const SizedBox(width: 20),
                const Icon(Icons.expand_less),
                const SizedBox(width: 5),
                const Text('Descending'),
                Checkbox(
                  value: sortOrder == 'desc',
                  activeColor: Colors.deepOrange,
                  onChanged: (newValue) {
                    if (newValue != null) {
                      setState(() {
                        sortOrder = newValue ? 'desc' : 'asc';
                      });
                    }
                  },
                ),
                const Spacer(),
              ],
            )
          ],
        ),
      ),
    );
  }
}
