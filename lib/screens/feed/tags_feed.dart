import 'package:blurt/bloc/server.dart';
import 'package:blurt/models/drawer_item.dart';
import 'package:blurt/screens/feed/new_feed_screen.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class TagsFeedScreen extends StatefulWidget {
  const TagsFeedScreen({
    Key? key,
    required this.item,
  }) : super(key: key);
  final DrawerItem item;

  @override
  State<TagsFeedScreen> createState() => _TagsFeedScreenState();
}

class _TagsFeedScreenState extends State<TagsFeedScreen> {
  @override
  Widget build(BuildContext context) {
    var appData = Provider.of<BlurtAppData>(context);
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.item.name),
      ),
      body: NewFeedScreen(item: widget.item, appData: appData),
    );
  }
}
