import 'package:blurt/bloc/server.dart';
import 'package:blurt/communication/feeds/blurt_communicator.dart';
import 'package:blurt/models/drawer_item.dart';
import 'package:blurt/models/feed_response_item.dart';
import 'package:blurt/screens/add_a_post/add_post.dart';
import 'package:blurt/screens/post_details/post_details.dart';
import 'package:blurt/screens/user_channel/user_channel.dart';
import 'package:blurt/widgets/error_state_widget.dart';
import 'package:blurt/widgets/fab_custom.dart';
import 'package:blurt/widgets/fab_overlay.dart';
import 'package:blurt/widgets/new_feed_item_widget.dart';
import 'package:flutter/material.dart';

class NewFeedScreen extends StatefulWidget {
  const NewFeedScreen({
    Key? key,
    required this.item,
    required this.appData,
  }) : super(key: key);
  final DrawerItem item;
  final BlurtAppData appData;

  @override
  State<NewFeedScreen> createState() => _NewFeedScreenState();
}

class _NewFeedScreenState extends State<NewFeedScreen>
    with AutomaticKeepAliveClientMixin<NewFeedScreen> {
  @override
  bool get wantKeepAlive => true;

  late Future<void> loadData;
  List<FeedResponseItem> items = [];
  var fetchingExtra = false;
  String widgetName = '';
  bool isMenuOpen = false;
  late ScrollController controller;
  var noMoreRecords = false;
  var loadingInitialFeed = false;

  @override
  void initState() {
    super.initState();
    loadData = loadInitialFeed();
    controller = ScrollController()..addListener(_scrollListener);
  }

  void _scrollListener() {
    if (controller.position.pixels == controller.position.maxScrollExtent) {
      getNextFeedData();
    }
  }

  void getNextFeedData() async {
    var request = widget.item.getRequestForNextPage(
      context,
      items.last.author,
      items.last.permlink,
    );
    if (request != null) {
      setState(() {
        fetchingExtra = true;
      });
      var records = await BlurtCommunicator().getFeedMoreItems(request);
      var itemsIds = items.map((e) => e.id);
      var newRecords = records.where((e) => !itemsIds.contains(e.id)).toList();
      newRecords.sort((a, b) {
        var aTime = a.createdAt;
        var bTime = b.createdAt;
        if (aTime.isAfter(bTime)) {
          return -1;
        } else if (bTime.isAfter(aTime)) {
          return 1;
        } else {
          return 0;
        }
      });
      newRecords.where((element) {
        return widget.appData.blockAndCoal.contains(element.author) == false ||
            (element.meta?.tags ?? []).contains('nsfw');
      }).toList();
      setState(() {
        items += newRecords;
        noMoreRecords = newRecords.isEmpty;
        fetchingExtra = false;
      });
    }
  }

  Future<void> loadInitialFeed() async {
    setState(() {
      loadingInitialFeed = true;
    });
    var request = widget.item.getRequest(context)!;
    var records = await BlurtCommunicator().getFeedItems(
      request,
      widget.appData,
    );
    var data = records;
    data.sort((a, b) {
      var aTime = a.createdAt;
      var bTime = b.createdAt;
      if (aTime.isAfter(bTime)) {
        return -1;
      } else if (bTime.isAfter(aTime)) {
        return 1;
      } else {
        return 0;
      }
    });
    setState(() {
      items = data.where((element) {
        return widget.appData.blockAndCoal.contains(element.author) == false ||
            (element.meta?.tags ?? []).contains('nsfw');
      }).toList();
      loadingInitialFeed = false;
    });
  }

  List<FabOverItemData> _fabItems() {
    var fabItems = [
      FabOverItemData(
        displayName: 'Refresh',
        icon: Icons.refresh,
        onTap: () {
          setState(() {
            isMenuOpen = false;
            loadData = loadInitialFeed();
          });
        },
      ),
    ];
    if (widget.appData.userData != null) {
      fabItems.add(
        FabOverItemData(
          displayName: 'Add a Post',
          icon: Icons.add,
          onTap: () {
            setState(() {
              isMenuOpen = false;
              var screen = AddPostScreen(
                parentAuthor: null,
                parentPermlink: null,
                onDone: () {},
              );
              var route = MaterialPageRoute(builder: (c) => screen);
              Navigator.of(context).push(route);
            });
          },
        ),
      );
    }
    fabItems.add(
      FabOverItemData(
        displayName: 'Close',
        icon: Icons.close,
        onTap: () {
          setState(() {
            isMenuOpen = false;
          });
        },
      ),
    );
    return fabItems;
  }

  Widget _fabContainer() {
    if (!isMenuOpen) {
      return FabCustom(
        icon: Icons.bolt,
        onTap: () {
          setState(() {
            isMenuOpen = true;
          });
        },
      );
    }
    return FabOverlay(
      items: _fabItems(),
      onBackgroundTap: () {
        setState(() {
          isMenuOpen = false;
        });
      },
    );
  }

  Widget _loadingDataForPullToRefresh() {
    return const Center(
      child: ListTile(
        title: Text('Loading Data', textAlign: TextAlign.center),
        subtitle: Text('Please wait', textAlign: TextAlign.center),
      ),
    );
  }

  Widget _listView() {
    return RefreshIndicator(
      onRefresh: () {
        return loadInitialFeed();
      },
      child: loadingInitialFeed
          ? _loadingDataForPullToRefresh()
          : ListView.separated(
              controller: controller,
              itemBuilder: (c, i) {
                if (i == items.length) {
                  return ListTile(
                    title: Center(
                      child: Text(
                        noMoreRecords
                            ? 'End of List'
                            : fetchingExtra
                                ? 'Loading Next page'
                                : 'Next Page',
                      ),
                    ),
                  );
                }
                return FeedItemWidget(
                  item: items[i],
                  showUserAvatar: true,
                  onPostTapped: () {
                    var screen = PostDetails(
                      author: items[i].author,
                      permlink: items[i].permlink,
                    );
                    var route = MaterialPageRoute(builder: (c) => screen);
                    Navigator.of(context).push(route);
                  },
                  onUserProfileTapped: () {
                    var screen = UserChannel(name: items[i].author);
                    var route = MaterialPageRoute(builder: (c) => screen);
                    Navigator.of(context).push(route);
                  },
                );
              },
              separatorBuilder: (c, i) => const Divider(
                height: 0,
                color: Colors.transparent,
              ),
              itemCount: items.length + 1,
            ),
    );
  }

  Widget _futureFeed() {
    return FutureBuilder(
      future: loadData,
      builder: (builder, snapshot) {
        if (snapshot.hasError) {
          return ErrorStateWidget(onRetry: () {
            setState(() {
              loadData = loadInitialFeed();
            });
          });
        } else if (snapshot.connectionState == ConnectionState.done) {
          return _listView();
        } else {
          return const Center(child: CircularProgressIndicator());
        }
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    super.build(context);
    return SafeArea(
      child: Stack(
        children: [
          _futureFeed(),
          _fabContainer(),
        ],
      ),
    );
  }
}
