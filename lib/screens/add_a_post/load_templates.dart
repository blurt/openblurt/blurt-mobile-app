import 'package:adaptive_action_sheet/adaptive_action_sheet.dart';
import 'package:blurt/models/template.dart';
import 'package:flutter/material.dart';
import 'package:localstorage/localstorage.dart';

class LoadTemplates extends StatefulWidget {
  const LoadTemplates({
    Key? key,
    required this.onSelect,
  }) : super(key: key);
  final Function(String, String, String, String) onSelect;

  @override
  State<LoadTemplates> createState() => _LoadTemplatesState();
}

class _LoadTemplatesState extends State<LoadTemplates> {
  LocalStorage storage = LocalStorage('blurt_local_storage');
  var initialized = false;

  void showBottomSheet(TemplateItemList list, int i) {
    showAdaptiveActionSheet(
      context: context,
      title: const Text('Select Action'),
      androidBorderRadius: 30,
      actions: [
        BottomSheetAction(
          title: const Text(
            'Load this Template',
            style: TextStyle(color: Colors.deepOrange),
          ),
          leading: const Icon(Icons.arrow_back, color: Colors.deepOrange),
          onPressed: (context) async {
            widget.onSelect(
              list.items[i].title,
              list.items[i].body,
              list.items[i].tags,
              list.items[i].thumb,
            );
            Navigator.of(context).pop();
            Navigator.of(context).pop();
          },
        ),
        BottomSheetAction(
          title: const Text(
            'Delete',
            style: TextStyle(color: Colors.red),
          ),
          leading: const Icon(Icons.delete, color: Colors.red),
          onPressed: (context) async {
            var items = list.items;
            items.removeAt(i);
            list.items = items;
            await storage.setItem('templates', list.toJSONEncodable());
            setState(() {
              Navigator.of(context).pop();
              Navigator.of(context).pop();
            });
          },
        )
      ],
      cancelAction: CancelAction(
        title: const Text(
          'Cancel',
          style: TextStyle(
            color: Colors.deepOrange,
          ),
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Templates'),
      ),
      body: SafeArea(
        child: FutureBuilder(
          future: storage.ready,
          builder: (c, s) {
            if (s.data == null) {
              return const Center(
                child: CircularProgressIndicator(),
              );
            }
            TemplateItemList list;
            if (!initialized) {
              var items = storage.getItem('templates');
              if (items != null) {
                list = TemplateItemList.from(items);
              } else {
                list = TemplateItemList(items: []);
              }
              initialized = true;
            } else {
              list = TemplateItemList(items: []);
            }
            if (list.items.isEmpty) {
              return const Center(child: Text('No items found.'));
            }
            return Container(
              margin: const EdgeInsets.only(top: 10),
              child: ListView.separated(
                itemBuilder: (c, i) {
                  return ListTile(
                    title: Text(list.items[i].title),
                    subtitle: list.items[i].body.length > 60
                        ? Text(list.items[i].body.substring(0, 59))
                        : Text(list.items[i].body),
                    onTap: () {
                      showBottomSheet(list, i);
                    },
                  );
                },
                separatorBuilder: (c, i) => const Divider(),
                itemCount: list.items.length,
              ),
            );
          },
        ),
      ),
    );
  }
}
