import 'dart:convert';
import 'dart:developer';

import 'package:blurt/bloc/server.dart';
import 'package:blurt/models/blurt_chain_properties.dart';
import 'package:blurt/models/blurt_user_stream.dart';
import 'package:blurt/models/login_bridge_response.dart';
import 'package:blurt/widgets/custom_circle_avatar.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_markdown/flutter_markdown.dart';
import 'package:http/http.dart' as http;
import 'package:provider/provider.dart';

class PreviewAndPublishScreen extends StatefulWidget {
  const PreviewAndPublishScreen({
    Key? key,
    required this.title,
    required this.description,
    required this.tags,
    required this.permlink,
    required this.parentAuthor,
    required this.parentPermlink,
    required this.thumbnail,
    required this.onDone,
  }) : super(key: key);
  final String title;
  final String description;
  final List<String> tags;
  final String permlink;
  final String thumbnail;
  final String? parentAuthor;
  final String? parentPermlink;
  final Function onDone;

  @override
  State<PreviewAndPublishScreen> createState() =>
      _PreviewAndPublishScreenState();
}

class _PreviewAndPublishScreenState extends State<PreviewAndPublishScreen> {
  var isShowingFees = false;
  BlurtChainProperties? chainProperties;
  var isLoadingChainProperty = false;

  void showError(String string) {
    var snackBar = SnackBar(content: Text('Error: $string'));
    ScaffoldMessenger.of(context).showSnackBar(snackBar);
  }

  Widget _postAuthorInfo(BlurtUserData? user) {
    var username = user?.username ?? 'sagarkothari88';
    return Row(
      children: [
        CustomCircleAvatar(
            height: 45,
            width: 45,
            url: 'https://imgp.blurt.world/profileimage/$username/64x64'),
        const SizedBox(width: 8),
        Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Row(
              children: [
                Text(username, style: Theme.of(context).textTheme.headline6),
                const SizedBox(height: 5),
              ],
            ),
          ],
        )
      ],
    );
  }

  Widget _content(BlurtUserData? user) {
    return SingleChildScrollView(
      child: Container(
        margin: const EdgeInsets.all(10),
        child: Column(
          children: widget.parentPermlink != null
              ? [MarkdownBody(data: widget.description)]
              : [
                  Text(
                    widget.title,
                    style: Theme.of(context).textTheme.headline4,
                  ),
                  const SizedBox(height: 10),
                  _postAuthorInfo(user),
                  const SizedBox(height: 10),
                  MarkdownBody(data: widget.description),
                ],
        ),
      ),
    );
  }

  Future<BlurtChainProperties> getProperties() async {
    var request = http.Request('POST', Uri.parse('https://rpc.blurt.world/'));
    request.body = json.encode({
      "jsonrpc": "2.0",
      "method": "call",
      "params": ["condenser_api", "get_chain_properties", []]
    });
    http.StreamedResponse response = await request.send();
    if (response.statusCode == 200) {
      var responseString = await response.stream.bytesToString();
      return BlurtChainProperties.fromJsonString(responseString);
    } else {
      throw response.reasonPhrase.toString();
    }
  }

  void loadChainProperties() async {
    setState(() {
      isLoadingChainProperty = true;
      chainProperties = null;
    });
    try {
      var props = await getProperties();
      setState(() {
        isLoadingChainProperty = false;
        chainProperties = props;
        isShowingFees = true;
      });
    } catch (e) {
      setState(() {
        isLoadingChainProperty = false;
        chainProperties = null;
        isShowingFees = false;
      });
    }
  }

  Widget? _fab() {
    var data = Provider.of<BlurtAppData>(context, listen: false);
    var user = data.userData;
    var domain = data.domain;
    if (user == null) return null;
    return FloatingActionButton(
      onPressed: () async {
        if (isLoadingChainProperty) return;
        if (!isShowingFees) {
          loadChainProperties();
        } else {
          // just to show loading indicator there
          setState(() {
            // set this to reload feed details
            isLoadingChainProperty = true;
            isShowingFees = false;
          });
          try {
            var platform = const MethodChannel('blog.blurt.blurt/auth');
            final String result = await platform.invokeMethod('comment', {
              'username': user.username,
              'postingKey': user.postingKey,
              'domain': domain,
              'author': user.username,
              'permlink': widget.permlink,
              'title': widget.title,
              'body': base64.encode(utf8.encode(widget.description)),
              'tags': widget.tags.join(" "),
              'summary': base64.encode(utf8.encode(
                  widget.description.length < 100
                      ? widget.description
                      : widget.description.substring(0, 100))),
              'pAuthor': widget.parentAuthor ?? '',
              'pPermlink': widget.parentPermlink ?? 'blurt',
              'thumbImage': widget.thumbnail,
            });
            var response = LoginBridgeResponse.fromJsonString(result);
            if (response.valid && response.error.isEmpty) {
              log("Successfully post published");
            }
            setState(() {
              widget.onDone();
              Navigator.of(context).pop();
              Navigator.of(context).pop();
              isLoadingChainProperty = false;
              isShowingFees = false;
            });
          } catch (e) {
            showError('Error occurred - ${e.toString()}');
            setState(() {
              isLoadingChainProperty = false;
              isShowingFees = false;
            });
          }
        }
      },
      child: isLoadingChainProperty
          ? const CircularProgressIndicator()
          : isShowingFees
              ? const Icon(Icons.publish)
              : const Icon(Icons.arrow_forward),
    );
  }

  Widget _publishFeesView() {
    var data = Provider.of<BlurtAppData>(context, listen: false);
    var user = data.userData;
    var isDarkMode = data.isDarkMode;
    if (user == null) return Container();
    if (chainProperties == null) return Container();
    var map = {
      "author": user.username,
      "title": widget.title,
      "body": widget.description,
      "permlink": widget.permlink,
      "json_metadata": {
        "tags": widget.tags,
        "app": "blurt/mobile/1.0.0",
        "format": "markdown",
        "description": widget.description.length < 100
            ? widget.description
            : widget.description.substring(0, 100)
      }
    };
    var string = json.encode(map);
    var length = string.length.toDouble();
    var bwFee = ((length / 1024.0) * chainProperties!.bandwidthFee);
    if (bwFee < 0.001) {
      bwFee = 0.001;
    }
    var fee = chainProperties!.flatFee + bwFee;
    return Column(
      children: [
        const Spacer(),
        Container(
          height: 84,
          decoration:
              BoxDecoration(color: isDarkMode ? Colors.black : Colors.white),
          child: Row(
            children: [
              const Spacer(),
              Text(
                "Fee ${fee.toStringAsFixed(3)} BLURT",
                style: Theme.of(context).textTheme.bodySmall,
                textAlign: TextAlign.center,
              ),
              const SizedBox(width: 80),
            ],
          ),
        ),
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    var data = Provider.of<BlurtAppData>(context);
    var user = data.userData;
    return Scaffold(
      appBar: AppBar(
        title: const Text('Preview'),
      ),
      body: GestureDetector(
        onTap: () {
          setState(() {
            isShowingFees = false;
          });
        },
        child: Stack(
          children: [
            _content(user),
            isShowingFees ? _publishFeesView() : Container()
          ],
        ),
      ),
      floatingActionButton: _fab(),
    );
  }
}
