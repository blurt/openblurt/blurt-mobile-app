import 'package:blurt/bloc/server.dart';
import 'package:blurt/models/template.dart';
import 'package:blurt/screens/add_a_post/load_templates.dart';
import 'package:blurt/screens/add_a_post/preview_publish.dart';
import 'package:blurt/utils/random_string.dart';
import 'package:blurt/widgets/fab_custom.dart';
import 'package:blurt/widgets/fab_overlay.dart';
import 'package:flutter/material.dart';
import 'package:localstorage/localstorage.dart';
import 'package:provider/provider.dart';

class AddPostScreen extends StatefulWidget {
  const AddPostScreen({
    Key? key,
    required this.parentAuthor,
    required this.parentPermlink,
    required this.onDone,
  }) : super(key: key);
  final String? parentAuthor;
  final String? parentPermlink;
  final Function onDone;

  @override
  State<AddPostScreen> createState() => _AddPostScreenState();
}

class _AddPostScreenState extends State<AddPostScreen> {
  void showError(String string) {
    var snackBar = SnackBar(content: Text('Error: $string'));
    ScaffoldMessenger.of(context).showSnackBar(snackBar);
  }

  void showMessage(String string) {
    var snackBar = SnackBar(content: Text('Message: $string'));
    ScaffoldMessenger.of(context).showSnackBar(snackBar);
  }

  var title = '';
  var description = '';
  var thumbnail = '';
  List<String> tags = [];
  var isMenuOpen = false;
  var titleFieldController = TextEditingController();
  var tagsFieldController = TextEditingController();
  var bodyFieldController = TextEditingController();
  var thumbFieldController = TextEditingController();
  var savingTemplate = false;

  Widget titleField() {
    return TextField(
      controller: titleFieldController,
      decoration: InputDecoration(
        hintText: 'Title goes here',
        labelText: 'Title',
        labelStyle: TextStyle(color: server.themeColor),
        focusedBorder: UnderlineInputBorder(
          borderSide: BorderSide(color: server.themeColor, width: 1.0),
        ),
        border: UnderlineInputBorder(
          borderSide: BorderSide(color: server.themeColor, width: 1.0),
        ),
      ),
      onChanged: (text) {
        setState(() {
          title = text;
        });
      },
      maxLines: 1,
      minLines: 1,
      maxLength: 150,
    );
  }

  Widget _thumbUrlField() {
    return TextField(
      controller: thumbFieldController,
      decoration: InputDecoration(
        hintText: 'Post Thumbnail URL goes here',
        labelText: 'Post Thumb Image URL',
        labelStyle: TextStyle(color: server.themeColor),
        focusedBorder: UnderlineInputBorder(
          borderSide: BorderSide(color: server.themeColor, width: 1.0),
        ),
        border: UnderlineInputBorder(
          borderSide: BorderSide(color: server.themeColor, width: 1.0),
        ),
      ),
      onChanged: (text) {
        setState(() {
          thumbnail = text;
        });
      },
      maxLines: 1,
      minLines: 1,
      // maxLength: 150,
    );
  }

  Widget _comment() {
    return TextFormField(
      controller: bodyFieldController,
      decoration: InputDecoration(
        hintText: widget.parentPermlink != null
            ? 'Add comment here'
            : 'Write your story',
        labelText: widget.parentPermlink != null ? 'Comment' : 'Body',
        labelStyle: TextStyle(color: server.themeColor),
        focusedBorder: UnderlineInputBorder(
          borderSide: BorderSide(color: server.themeColor, width: 1.0),
        ),
        border: UnderlineInputBorder(
          borderSide: BorderSide(color: server.themeColor, width: 1.0),
        ),
      ),
      onChanged: (text) {
        setState(() {
          description = text;
        });
      },
      maxLines: 8,
      minLines: 5,
    );
  }

  Widget _tags() {
    return TextField(
      controller: tagsFieldController,
      decoration: InputDecoration(
        hintText: 'Space separated tags',
        labelText: 'Tags',
        labelStyle: TextStyle(color: server.themeColor),
        focusedBorder: UnderlineInputBorder(
          borderSide: BorderSide(color: server.themeColor, width: 1.0),
        ),
        border: UnderlineInputBorder(
          borderSide: BorderSide(color: server.themeColor, width: 1.0),
        ),
      ),
      onChanged: (text) {
        setState(() {
          tags = text.trim().split(" ");
        });
      },
      maxLines: 1,
      minLines: 1,
    );
  }

  Widget _body(BlurtAppData appData) {
    return SafeArea(
      child: Stack(
        children: [
          Container(
            margin: const EdgeInsets.all(20),
            child: Column(
              children: widget.parentPermlink != null
                  ? [_comment()]
                  : [titleField(), _comment(), _tags(), _thumbUrlField()],
            ),
          ),
          _fabContainer(appData),
        ],
      ),
    );
  }

  void navigateToPreviewAndPublish(BlurtAppData appData) {
    if (appData.userData == null) {
      showError('You\'re not logged in');
      return;
    }
    if (widget.parentPermlink != null && description.isEmpty) {
      showError('Please add comment');
      return;
    }
    if (widget.parentPermlink == null && title.isEmpty) {
      showError('Please enter post title');
      return;
    }
    if (widget.parentPermlink == null && description.isEmpty) {
      showError('Please enter post body');
      return;
    }
    if (widget.parentPermlink == null && tags.isEmpty) {
      showError('Please enter at least one tag');
      return;
    }
    var screen = PreviewAndPublishScreen(
      tags: tags,
      title: title,
      description: description,
      thumbnail: thumbnail.isEmpty
          ? 'https://imgp.blurt.world/220x220/https://cdn.publish0x.com/prod/fs/images/f843764f9514e1194501f4c4f3a8356e6670cfd884841f851d69e26fa6fa3d6c.png'
          : thumbnail,
      permlink: RandomString().getRandomString(20),
      parentAuthor: widget.parentAuthor,
      parentPermlink: widget.parentPermlink,
      onDone: widget.onDone,
    );
    var route = MaterialPageRoute(builder: (c) => screen);
    Navigator.of(context).push(route);
  }

  void saveATemplate() async {
    if (widget.parentPermlink != null && description.isEmpty) {
      showError('Please add comment');
      return;
    }
    if (widget.parentPermlink == null && title.isEmpty) {
      showError('Please enter post title');
      return;
    }
    if (widget.parentPermlink == null && description.isEmpty) {
      showError('Please enter post body');
      return;
    }
    if (widget.parentPermlink == null && tags.isEmpty) {
      showError('Please enter at least one tag');
      return;
    }
    setState(() {
      savingTemplate = true;
    });
    final LocalStorage storage = LocalStorage('blurt_local_storage');
    await storage.ready;
    TemplateItemList list;
    var items = storage.getItem('templates');
    if (items != null) {
      list = TemplateItemList.from(items);
    } else {
      list = TemplateItemList(items: []);
    }
    list.items.add(
      TemplateItem(
        title: title,
        tags: tags.join(" "),
        thumb: thumbnail,
        body: description,
      ),
    );
    await storage.setItem('templates', list.toJSONEncodable());
    title = '';
    titleFieldController.clear();
    tagsFieldController.clear();
    bodyFieldController.clear();
    thumbFieldController.clear();
    description = '';
    tags = [];
    thumbnail = '';
    setState(() {
      savingTemplate = false;
      showMessage('Template saved');
    });
  }

  List<FabOverItemData> _fabItems(BlurtAppData appData) {
    var fabItems = [
      FabOverItemData(
        displayName: 'Load Template',
        icon: Icons.open_in_browser,
        onTap: () {
          setState(() {
            isMenuOpen = false;
            Navigator.of(context).push(
              MaterialPageRoute(
                builder: (c) => LoadTemplates(
                  onSelect: (t, b, tgs, thmb) {
                    setState(() {
                      title = t;
                      description = b;
                      tags = tgs.split(" ");
                      thumbnail = thmb;
                      titleFieldController.text = t;
                      bodyFieldController.text = b;
                      tagsFieldController.text = tgs;
                      thumbFieldController.text = thmb;
                    });
                  },
                ),
              ),
            );
          });
        },
      ),
      FabOverItemData(
        displayName: 'Save as Template',
        icon: Icons.save,
        onTap: () {
          setState(() {
            isMenuOpen = false;
            saveATemplate();
          });
        },
      ),
      FabOverItemData(
        displayName: 'Clear all fields',
        icon: Icons.delete_sweep,
        onTap: () {
          setState(() {
            isMenuOpen = false;
            title = '';
            titleFieldController.clear();
            tagsFieldController.clear();
            bodyFieldController.clear();
            thumbFieldController.clear();
            description = '';
            tags = [];
            thumbnail = '';
          });
        },
      ),
      FabOverItemData(
        displayName: 'Preview & Publish',
        icon: Icons.arrow_forward,
        onTap: () {
          setState(() {
            navigateToPreviewAndPublish(appData);
            isMenuOpen = false;
          });
        },
      ),
    ];
    fabItems.add(
      FabOverItemData(
        displayName: 'Close',
        icon: Icons.close,
        onTap: () {
          setState(() {
            isMenuOpen = false;
          });
        },
      ),
    );
    return fabItems;
  }

  Widget _fabContainer(BlurtAppData appData) {
    if (!isMenuOpen) {
      return FabCustom(
        icon: Icons.bolt,
        onTap: () {
          setState(() {
            isMenuOpen = true;
          });
        },
      );
    }
    return FabOverlay(
      items: _fabItems(appData),
      onBackgroundTap: () {
        setState(() {
          isMenuOpen = false;
        });
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    var data = Provider.of<BlurtAppData>(context);
    return Scaffold(
      appBar: AppBar(
        title: Text(
            widget.parentPermlink != null ? 'Add a comment' : 'Add a Post'),
      ),
      body: savingTemplate
          ? const Center(child: CircularProgressIndicator())
          : _body(data),
    );
  }
}
