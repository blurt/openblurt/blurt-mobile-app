import 'package:blurt/communication/feeds/blurt_communicator.dart';
import 'package:blurt/models/blurt_witness_info.dart';
import 'package:blurt/screens/user_channel/user_channel_info.dart';
import 'package:blurt/screens/user_channel/user_channel_info_votes.dart';
import 'package:blurt/widgets/fab_custom.dart';
import 'package:blurt/widgets/fab_overlay.dart';
import 'package:flutter/material.dart';

class UserChannelInfoContainer extends StatefulWidget {
  const UserChannelInfoContainer({
    Key? key,
    required this.name,
    required this.domain,
  }) : super(key: key);
  final String name;
  final String domain;

  @override
  State<UserChannelInfoContainer> createState() =>
      _UserChannelInfoContainerState();
}

class _UserChannelInfoContainerState extends State<UserChannelInfoContainer> {
  bool isMenuOpen = false;

  var isShowingInfo = true;
  var isShowingWitnessVotes = false;
  var isShowingWitnessVotesReceived = false;

  late Future<BlurtWitnessInfo> getWitnessInfo;

  @override
  void initState() {
    super.initState();
    getWitnessInfo = BlurtCommunicator().getWitnessInfo(
      widget.domain,
      widget.name,
    );
  }

  Widget _errorState() {
    return Column(
      children: [
        const Spacer(),
        Row(children: const [
          Spacer(),
          Text(
            'Oops! Something went wrong.\nPlease Try again.',
            textAlign: TextAlign.center,
          ),
          Spacer(),
        ]),
        const SizedBox(height: 20),
        ElevatedButton(
          onPressed: () {
            setState(() {
              getWitnessInfo = BlurtCommunicator().getWitnessInfo(
                widget.domain,
                widget.name,
              );
            });
          },
          child: const Text('Retry'),
        ),
        const Spacer(),
      ],
    );
  }

  List<FabOverItemData> _fabItems(BlurtWitnessInfo info) {
    List<FabOverItemData> fabItems = [];
    if (info.result != null && isShowingInfo) {
      fabItems.add(
        FabOverItemData(
          displayName: 'Received Witness votes',
          icon: Icons.how_to_vote,
          onTap: () {
            setState(() {
              isMenuOpen = false;
              isShowingInfo = false;
              isShowingWitnessVotes = false;
              isShowingWitnessVotesReceived = true;
            });
          },
        ),
      );
    }
    if (isShowingInfo) {
      fabItems.add(
        FabOverItemData(
          displayName: 'Given Witness votes',
          icon: Icons.check,
          onTap: () {
            setState(() {
              isMenuOpen = false;
              isShowingInfo = false;
              isShowingWitnessVotes = true;
              isShowingWitnessVotesReceived = false;
            });
          },
        ),
      );
    }
    if (!isShowingInfo) {
      fabItems.add(
        FabOverItemData(
          displayName: '${widget.name}\'s info',
          icon: Icons.info,
          onTap: () {
            setState(() {
              isMenuOpen = false;
              isShowingInfo = true;
              isShowingWitnessVotes = false;
              isShowingWitnessVotesReceived = false;
            });
          },
        ),
      );
    }
    fabItems.add(
      FabOverItemData(
        displayName: 'Close',
        icon: Icons.close,
        onTap: () {
          setState(() {
            isMenuOpen = false;
          });
        },
      ),
    );
    return fabItems;
  }

  Widget _fabContainer(BlurtWitnessInfo info) {
    if (!isMenuOpen) {
      return FabCustom(
        icon: Icons.bolt,
        onTap: () {
          setState(() {
            isMenuOpen = true;
          });
        },
      );
    }
    return FabOverlay(
      items: _fabItems(info),
      onBackgroundTap: () {
        setState(() {
          isMenuOpen = false;
        });
      },
    );
  }

  Widget _futureFeed() {
    return FutureBuilder(
      future: getWitnessInfo,
      builder: (builder, snapshot) {
        if (snapshot.hasError) {
          return _errorState();
        } else if (snapshot.hasData &&
            snapshot.connectionState == ConnectionState.done) {
          return Stack(
            children: [
              isShowingInfo
                  ? UserChannelInfo(
                      name: widget.name,
                      domain: widget.domain,
                      info: snapshot.data as BlurtWitnessInfo,
                    )
                  : isShowingWitnessVotes
                      ? UserChannelInfoWitnessVotes(
                          domain: widget.domain,
                          name: widget.name,
                          isGivenVotes: true,
                        )
                      : isShowingWitnessVotesReceived
                          ? UserChannelInfoWitnessVotes(
                              domain: widget.domain,
                              name: widget.name,
                              isGivenVotes: false,
                            )
                          : Container(),
              _fabContainer(snapshot.data as BlurtWitnessInfo),
            ],
          );
        } else {
          return const Center(child: CircularProgressIndicator());
        }
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: _futureFeed(),
    );
  }
}
