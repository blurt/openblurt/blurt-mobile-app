import 'dart:convert';
import 'dart:developer';

import 'package:adaptive_action_sheet/adaptive_action_sheet.dart';
import 'package:blurt/bloc/server.dart';
import 'package:blurt/communication/feeds/blurt_communicator.dart';
import 'package:blurt/models/login_bridge_response.dart';
import 'package:blurt/models/user_channel_followers_following.dart';
import 'package:blurt/screens/user_channel/user_channel.dart';
import 'package:blurt/widgets/custom_circle_avatar.dart';
import 'package:blurt/widgets/fab_custom.dart';
import 'package:blurt/widgets/fab_overlay.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:http/http.dart' as http;
import 'package:provider/provider.dart';

class UserChannelUsers extends StatefulWidget {
  const UserChannelUsers({
    Key? key,
    required this.name,
  }) : super(key: key);
  final String name;

  @override
  State<UserChannelUsers> createState() => _UserChannelUsersState();
}

class _UserChannelUsersState extends State<UserChannelUsers>
    with AutomaticKeepAliveClientMixin<UserChannelUsers> {
  @override
  bool get wantKeepAlive => true;

  Future<List<List<UserChannelFollowItem>>>? loadData;

  var isShowingFollowers = true;
  var isShowingFollowings = false;
  var isShowingBlocked = false;
  var isMenuOpen = false;

  var isLoading = false;

  http.Request getFollowersRequest(String domain, String forUser) {
    var request = http.Request('POST', Uri.parse(domain));
    request.body = json.encode({
      "jsonrpc": "2.0",
      "method": "call",
      "params": [
        "condenser_api",
        "get_followers", //: "get_following",
        [forUser, "", "blog", 1000]
      ]
    });
    return request;
  }

  http.Request getFollowingsRequest(String domain, String forUser) {
    var request = http.Request('POST', Uri.parse(domain));
    request.body = json.encode({
      "jsonrpc": "2.0",
      "method": "call",
      "params": [
        "condenser_api",
        "get_following",
        [forUser, "", "blog", 1000]
      ]
    });
    return request;
  }

  http.Request getBlockedRequest(String domain, String forUser) {
    var request = http.Request('POST', Uri.parse(domain));
    request.body = json.encode({
      "jsonrpc": "2.0",
      "method": "call",
      "params": [
        "condenser_api",
        "get_following",
        [forUser, "", "ignore", 1000]
      ]
    });
    return request;
  }

  Future<List<List<UserChannelFollowItem>>> loadUserData(
    String domain,
    BlurtAppData appData,
  ) async {
    var followers = await BlurtCommunicator().getFollow(getFollowersRequest(
      domain,
      widget.name,
    ));
    var followings = await BlurtCommunicator().getFollow(getFollowingsRequest(
      domain,
      widget.name,
    ));
    var blocked = await BlurtCommunicator().getFollow(getBlockedRequest(
      domain,
      widget.name,
    ));
    List<UserChannelFollowItem> userFollowers = [];
    List<UserChannelFollowItem> userFollowings = [];
    List<UserChannelFollowItem> userBlocked = [];
    if (widget.name == appData.userData?.username) {
      userFollowers = followers;
      userFollowings = followings;
      userBlocked = blocked;
    } else if (appData.userData?.username != null) {
      userFollowers = await BlurtCommunicator().getFollow(getFollowersRequest(
        domain,
        appData.userData!.username,
      ));
      userFollowings = await BlurtCommunicator().getFollow(getFollowingsRequest(
        domain,
        appData.userData!.username,
      ));
      userBlocked = await BlurtCommunicator().getFollow(getBlockedRequest(
        domain,
        appData.userData!.username,
      ));
    }
    return [
      followers,
      followings,
      blocked,
      userFollowers,
      userFollowings,
      userBlocked,
    ];
  }

  Widget _errorState() {
    return Column(
      children: [
        const Spacer(),
        Row(children: const [
          Spacer(),
          Text(
            'Oops! Something went wrong.\nPlease Try again.',
            textAlign: TextAlign.center,
          ),
          Spacer(),
        ]),
        const SizedBox(height: 20),
        ElevatedButton(
          onPressed: () {
            setState(() {
              loadData = null;
            });
          },
          child: const Text('Retry'),
        ),
        const Spacer(),
      ],
    );
  }

  void showError(String string) {
    var snackBar = SnackBar(content: Text('Error: $string'));
    ScaffoldMessenger.of(context).showSnackBar(snackBar);
  }

  void showMessage(String string) {
    var snackBar = SnackBar(content: Text('Message: $string'));
    ScaffoldMessenger.of(context).showSnackBar(snackBar);
  }

  void showBottomSheetForUserFlagWithFee(
    BlurtAppData appData,
    String fees,
    String title,
    String actionAccount,
    String actionType,
  ) {
    showAdaptiveActionSheet(
      context: context,
      title: Text('This operation will cost you $fees BLURT'),
      androidBorderRadius: 30,
      actions: [
        BottomSheetAction(
          title: Text(
            title, // 'Block ${widget.name}',
            style: const TextStyle(color: Colors.deepOrange),
          ),
          onPressed: (context) async {
            Navigator.of(context).pop();
            var platform = const MethodChannel('blog.blurt.blurt/auth');
            setState(() {
              isLoading = true;
            });
            final String result = await platform.invokeMethod('updateFollow', {
              'accountName': appData.userData!.username,
              'postingKey': appData.userData!.postingKey,
              'actionAccount': actionAccount,
              'actionType': actionType, //'mute',
            });
            var response = LoginBridgeResponse.fromJsonString(result);
            if (response.error.isEmpty) {
              showMessage('You\'ve $title');
            } else {
              showError(response.error);
            }
            setState(() {
              isLoading = false;
              loadData = null;
            });
          },
        ),
        BottomSheetAction(
          title: const Text(
            'Cancel',
            style: TextStyle(color: Colors.deepOrange),
          ),
          onPressed: (context) {
            Navigator.of(context).pop();
          },
        )
      ],
    );
  }

  void performAction(
    BlurtAppData appData,
    String actionAccount,
    String actionType,
  ) async {
    var platform = const MethodChannel('blog.blurt.blurt/auth');
    setState(() {
      isLoading = true;
    });
    final String result = await platform.invokeMethod('updateFollowInfo', {
      'accountName': appData.userData!.username,
      'actionAccount': actionAccount,
      'actionType': actionType,
    });
    var response = LoginBridgeResponse.fromJsonString(result);
    log("Response error is ${response.error}");
    if (response.error.startsWith('fee: ')) {
      showBottomSheetForUserFlagWithFee(
        appData,
        response.error.replaceAll('fee: ', ''),
        '$actionType $actionAccount',
        actionAccount,
        actionType,
      );
    } else {
      showError(response.error);
    }
    setState(() {
      isLoading = false;
    });
  }

  void _userTapActions(
    BlurtAppData appData,
    String title,
    List<List<UserChannelFollowItem>> list,
  ) {
    var isFollowing =
        list[4].where((e) => e.following == title).toList().isNotEmpty;
    var isBlocked =
        list[5].where((e) => e.following == title).toList().isNotEmpty;
    List<BottomSheetAction> actions = [
      BottomSheetAction(
        leading: const Icon(Icons.feed, color: Colors.deepOrange),
        title: const Text(
          'View blog',
          style: TextStyle(color: Colors.deepOrange),
        ),
        onPressed: (context) {
          Navigator.of(context).pop();
          var screen = UserChannel(name: title);
          var route = MaterialPageRoute(builder: (c) => screen);
          Navigator.of(context).push(route);
        },
      )
    ];
    if (appData.userData != null) {
      actions.add(
        BottomSheetAction(
          leading: Icon(
            isFollowing ? Icons.person_remove : Icons.person_add,
            color: Colors.deepOrange,
          ),
          title: Text(
            isFollowing ? 'Unfollow' : 'Follow',
            style: const TextStyle(color: Colors.deepOrange),
          ),
          onPressed: (context) {
            Navigator.of(context).pop();
            performAction(appData, title, isFollowing ? 'unfollow' : 'follow');
          },
        ),
      );
      actions.add(
        BottomSheetAction(
          leading: Icon(
            isBlocked ? Icons.person : Icons.person_off,
            color: Colors.deepOrange,
          ),
          title: Text(
            isBlocked ? 'Unblock' : 'Block',
            style: const TextStyle(color: Colors.deepOrange),
          ),
          onPressed: (context) {
            Navigator.of(context).pop();
            performAction(appData, title, isBlocked ? 'unmute' : 'mute');
          },
        ),
      );
    }
    actions.add(
      BottomSheetAction(
        leading: const Icon(Icons.cancel, color: Colors.deepOrange),
        title: const Text(
          'Cancel',
          style: TextStyle(color: Colors.deepOrange),
        ),
        onPressed: (context) {
          Navigator.of(context).pop();
        },
      ),
    );
    showAdaptiveActionSheet(
      context: context,
      title: Text('Choose Action for $title'),
      androidBorderRadius: 30,
      actions: actions,
    );
  }

  Widget _listTile(
    String type,
    UserChannelFollowItem item,
    BlurtAppData appData,
    List<List<UserChannelFollowItem>> list,
  ) {
    String value = type == 'follower' ? item.follower : item.following;
    return ListTile(
      leading: CustomCircleAvatar(
          height: 45,
          width: 45,
          url: 'https://imgp.blurt.world/profileimage/$value/64x64'),
      title: Text(value),
      onTap: () {
        _userTapActions(appData, value, list);
      },
    );
  }

  Widget _headerForList(String text) {
    return Container(
      margin: const EdgeInsets.only(top: 10),
      child: Row(
        children: [
          const Spacer(),
          Text(text, style: Theme.of(context).textTheme.titleLarge),
          const Spacer(),
        ],
      ),
    );
  }

  Widget _centerMessageWithIcon(String message, IconData icon) {
    return Center(
      child: Column(
        children: [
          const Spacer(),
          Text(message),
          const SizedBox(height: 10),
          Icon(icon),
          const Spacer(),
        ],
      ),
    );
  }

  Widget _list(
    List<List<UserChannelFollowItem>> list,
    BlurtAppData appData,
  ) {
    if (list.isEmpty || list.length != 6) {
      return _centerMessageWithIcon('No Data found', Icons.heart_broken);
    }
    var followers = list[0];
    if (followers.isEmpty) {
      return _centerMessageWithIcon('No followers found', Icons.heart_broken);
    }
    return Stack(
      children: [
        Container(
          margin: const EdgeInsets.only(top: 40),
          child: ListView.separated(
            itemBuilder: (c, i) {
              return _listTile('follower', followers[i], appData, list);
            },
            separatorBuilder: (c, i) => const Divider(),
            itemCount: followers.length,
          ),
        ),
        _headerForList('Followers (${followers.length})'),
      ],
    );
  }

  Widget _listFollowings(
    List<List<UserChannelFollowItem>> list,
    BlurtAppData appData,
  ) {
    if (list.isEmpty || list.length != 6) {
      return _centerMessageWithIcon('No Data found', Icons.heart_broken);
    }
    var followings = list[1];
    if (followings.isEmpty) {
      return _centerMessageWithIcon(
        '${widget.name} follows no one',
        Icons.heart_broken,
      );
    }
    return Stack(
      children: [
        Container(
          margin: const EdgeInsets.only(top: 40),
          child: ListView.separated(
            itemBuilder: (c, i) {
              var follower = followings[i];
              var isFollowing = followings
                  .where((element) => element.following == follower.follower)
                  .toList()
                  .isNotEmpty;
              return _listTile('following', followings[i], appData, list);
            },
            separatorBuilder: (c, i) => const Divider(),
            itemCount: followings.length,
          ),
        ),
        _headerForList('Follows (${followings.length})'),
      ],
    );
  }

  Widget _listBlocked(
    List<List<UserChannelFollowItem>> list,
    BlurtAppData appData,
  ) {
    if (list.isEmpty || list.length != 6) {
      return _centerMessageWithIcon('No Data found', Icons.heart_broken);
    }
    var blocked = list[2];
    if (blocked.isEmpty) {
      return _centerMessageWithIcon(
        '${widget.name} blocks no one',
        Icons.favorite,
      );
    }
    return Stack(
      children: [
        Container(
          margin: const EdgeInsets.only(top: 40),
          child: ListView.separated(
            itemBuilder: (c, i) {
              return _listTile('blocked', blocked[i], appData, list);
            },
            separatorBuilder: (c, i) => const Divider(),
            itemCount: blocked.length,
          ),
        ),
        _headerForList('Blocked (${blocked.length})'),
      ],
    );
  }

  Widget _fabContainer() {
    if (!isMenuOpen) {
      return FabCustom(
        icon: Icons.bolt,
        onTap: () {
          setState(() {
            isMenuOpen = true;
          });
        },
      );
    }
    return FabOverlay(
      items: [
        FabOverItemData(
          displayName: 'Followers',
          icon: Icons.person_add,
          onTap: () {
            setState(() {
              isMenuOpen = false;
              isShowingFollowers = true;
              isShowingFollowings = false;
              isShowingBlocked = false;
            });
          },
        ),
        FabOverItemData(
          displayName: 'Follows',
          icon: Icons.person_add_alt,
          onTap: () {
            setState(() {
              isMenuOpen = false;
              isShowingFollowers = false;
              isShowingFollowings = true;
              isShowingBlocked = false;
            });
          },
        ),
        FabOverItemData(
          displayName: 'Blocked',
          icon: Icons.block,
          onTap: () {
            setState(() {
              isMenuOpen = false;
              isShowingFollowers = false;
              isShowingFollowings = false;
              isShowingBlocked = true;
            });
          },
        ),
        FabOverItemData(
          displayName: 'Refresh',
          icon: Icons.refresh,
          onTap: () {
            setState(() {
              isMenuOpen = false;
              isShowingFollowers = true;
              isShowingFollowings = false;
              isShowingBlocked = false;
              loadData = null;
            });
          },
        )
      ],
      onBackgroundTap: () {
        setState(() {
          isMenuOpen = false;
        });
      },
    );
  }

  Widget _data(
    List<List<UserChannelFollowItem>> list,
    BlurtAppData appData,
  ) {
    return SafeArea(
      child: Stack(
        children: [
          isShowingFollowers
              ? _list(list, appData)
              : isShowingFollowings
                  ? _listFollowings(list, appData)
                  : _listBlocked(list, appData),
          _fabContainer(),
        ],
      ),
    );
  }

  Widget _futureFeed(BlurtAppData appData) {
    return FutureBuilder(
      future: loadData,
      builder: (builder, snapshot) {
        if (snapshot.hasError) {
          return _errorState();
        } else if (snapshot.hasData &&
            snapshot.connectionState == ConnectionState.done) {
          return _data(
              snapshot.data as List<List<UserChannelFollowItem>>, appData);
        } else {
          return const Center(child: CircularProgressIndicator());
        }
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    super.build(context);
    var appData = Provider.of<BlurtAppData>(context);
    var domain = appData.domain;
    if (loadData == null) {
      setState(() {
        loadData = loadUserData(domain, appData);
      });
    }
    return isLoading
        ? const Center(child: CircularProgressIndicator())
        : _futureFeed(appData);
  }
}
