import 'package:adaptive_action_sheet/adaptive_action_sheet.dart';
import 'package:blurt/bloc/server.dart';
import 'package:blurt/communication/feeds/blurt_communicator.dart';
import 'package:blurt/models/user_notification.dart';
import 'package:blurt/screens/post_details/post_details.dart';
import 'package:blurt/screens/user_channel/user_channel.dart';
import 'package:blurt/widgets/custom_circle_avatar.dart';
import 'package:blurt/widgets/fab_custom.dart';
import 'package:blurt/widgets/fab_overlay.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:timeago/timeago.dart' as timeago;

class UserNotifications extends StatefulWidget {
  const UserNotifications({
    Key? key,
    required this.name,
  }) : super(key: key);
  final String name;

  @override
  State<UserNotifications> createState() => _UserNotificationsState();
}

class UserNotificationFilterOption {
  final String displayName;
  final String? filterName;
  final IconData iconData;
  const UserNotificationFilterOption({
    required this.displayName,
    required this.iconData,
    required this.filterName,
  });
}

class _UserNotificationsState extends State<UserNotifications>
    with AutomaticKeepAliveClientMixin<UserNotifications> {
  @override
  bool get wantKeepAlive => true;
  Future<List<UserNotificationItem>>? loadData;

  var isFilterMenuOn = false;
  late DateTime dateTime;

  UserNotificationFilterOption listType = const UserNotificationFilterOption(
    displayName: 'All',
    iconData: Icons.list,
    filterName: null,
  );

  List<UserNotificationFilterOption> items = const [
    UserNotificationFilterOption(
      displayName: 'All',
      iconData: Icons.list,
      filterName: null,
    ),
    UserNotificationFilterOption(
      displayName: 'Replies',
      iconData: Icons.reply,
      filterName: 'reply',
    ),
    UserNotificationFilterOption(
      displayName: 'Mentions',
      iconData: Icons.mark_unread_chat_alt_outlined,
      filterName: 'mention',
    ),
    UserNotificationFilterOption(
      displayName: 'Follows',
      iconData: Icons.person_add,
      filterName: 'follow',
    ),
    UserNotificationFilterOption(
      displayName: 'ReBlurts',
      iconData: Icons.repeat,
      filterName: 'reblog',
    ),
    UserNotificationFilterOption(
      displayName: 'Transfers',
      iconData: Icons.currency_bitcoin,
      filterName: 'transfer',
    ),
    UserNotificationFilterOption(
      displayName: 'Witness Vote',
      iconData: Icons.how_to_vote_outlined,
      filterName: 'witness_vote',
    ),
  ];

  Widget _errorState() {
    return Column(
      children: [
        const Spacer(),
        Row(children: const [
          Spacer(),
          Text(
            'Oops! Something went wrong.\nPlease Try again.',
            textAlign: TextAlign.center,
          ),
          Spacer(),
        ]),
        const SizedBox(height: 20),
        ElevatedButton(
          onPressed: () {
            setState(() {
              loadData = null;
            });
          },
          child: const Text('Retry'),
        ),
        const Spacer(),
      ],
    );
  }

  List<FabOverItemData> _fabItems() {
    var fabItems = items.map((item) {
      return FabOverItemData(
        displayName: item.displayName,
        icon: item.iconData,
        onTap: () {
          setState(() {
            isFilterMenuOn = false;
            listType = item;
          });
        },
      );
    }).toList();
    fabItems.add(
      FabOverItemData(
        displayName: 'Reload',
        icon: Icons.refresh,
        onTap: () {
          setState(() {
            isFilterMenuOn = false;
            loadData = null;
          });
        },
      ),
    );
    fabItems.add(
      FabOverItemData(
        displayName: 'Close',
        icon: Icons.close,
        onTap: () {
          setState(() {
            isFilterMenuOn = false;
          });
        },
      ),
    );
    return fabItems;
  }

  Widget _fabContainer() {
    if (!isFilterMenuOn) {
      return FabCustom(
        icon: Icons.filter_alt,
        onTap: () {
          setState(() {
            isFilterMenuOn = true;
          });
        },
      );
    }
    return FabOverlay(
      items: _fabItems(),
      onBackgroundTap: () {
        setState(() {
          isFilterMenuOn = false;
        });
      },
    );
  }

  Widget _filterTitle(String? filterName) {
    var text =
        filterName == null ? 'No filter applied' : 'Filtered By $filterName';
    return Column(
      children: [
        const SizedBox(height: 5),
        Row(
          children: [
            const Spacer(),
            Text(
              text,
              style: const TextStyle(color: Colors.deepOrange, fontSize: 15),
            ),
            const Spacer(),
          ],
        ),
      ],
    );
  }

  Widget _emptyContainer() {
    return SafeArea(
      child: Stack(
        children: [
          Center(
            child: Column(
              children: const [
                Spacer(),
                Text('No notifications to show'),
                SizedBox(height: 10),
                Icon(Icons.notifications_off_outlined),
                Spacer(),
              ],
            ),
          ),
          _fabContainer(),
          _filterTitle(listType.filterName),
        ],
      ),
    );
  }

  String _textForItem(UserNotificationItem obj) {
    var text = '';
    if (obj.type == 'vote') {
      text = 'Upvoted ${obj.permlink}';
    } else if (obj.type == 'transfer') {
      text = 'Transferred ${obj.amount}. Memo: ${obj.memo}';
    } else if (obj.type == 'reply') {
      text = 'Replied on ${obj.permlink}';
    } else if (obj.type == 'follow') {
      text = 'follows ${widget.name}';
    } else if (obj.type == 'reblog') {
      text = 'ReBlurted ${obj.permlink}';
    } else if (obj.type == 'mention') {
      text = 'mentioned ${widget.name} in ${obj.permlink}';
    } else if (obj.type == 'witness_vote') {
      text =
          '${widget.name} has ${(obj.approve ?? false) ? 'approved' : 'unapproved'} your witness';
    }
    return text;
  }

  IconData _iconForItem(UserNotificationItem obj) {
    return obj.type == 'vote'
        ? Icons.favorite
        : obj.type == 'transfer'
            ? Icons.currency_bitcoin
            : obj.type == 'reply'
                ? Icons.reply
                : obj.type == 'follow'
                    ? Icons.person_add
                    : obj.type == 'reblog'
                        ? Icons.repeat
                        : obj.type == 'mention'
                            ? Icons.mark_unread_chat_alt_outlined
                            : obj.type == 'witness_vote'
                                ? obj.approve == true
                                    ? Icons.how_to_vote_outlined
                                    : Icons.no_accounts
                                : Icons.bolt;
  }

  void onTapItem(UserNotificationItem item) {
    List<BottomSheetAction> actionItems = [];
    if (item.name.isNotEmpty) {
      actionItems.add(
        BottomSheetAction(
          title: Text(
            "View ${item.name}'s Blog",
            style: const TextStyle(color: Colors.deepOrange),
          ),
          onPressed: (context) {
            Navigator.of(context).pop();
            var screen = UserChannel(name: item.name);
            var route = MaterialPageRoute(builder: (c) => screen);
            Navigator.of(context).push(route);
          },
        ),
      );
    }
    if ((item.type == 'reply' ||
            item.type == 'reblog' ||
            item.type == 'mention' ||
            item.type == 'vote') &&
        item.permlink.isNotEmpty) {
      var author = item.author.isNotEmpty ? item.author : widget.name;
      actionItems.add(
        BottomSheetAction(
          title: const Text(
            "View Post",
            style: TextStyle(color: Colors.deepOrange),
          ),
          onPressed: (context) {
            Navigator.of(context).pop();
            var screen = PostDetails(author: author, permlink: item.permlink);
            var route = MaterialPageRoute(builder: (c) => screen);
            Navigator.of(context).push(route);
          },
        ),
      );
    }
    showAdaptiveActionSheet(
      context: context,
      title: const Text('Select Action'),
      androidBorderRadius: 30,
      actions: actionItems,
      cancelAction: CancelAction(
          title: const Text(
        'Cancel',
        style: TextStyle(
          color: Colors.deepOrange,
        ),
      )),
    );
  }

  Widget _listItem(UserNotificationItem item) {
    var obj = item;
    var text = _textForItem(item);
    var time = DateTime.fromMillisecondsSinceEpoch(item.timestamp * 1000);
    var ago = timeago.format(time);
    var isBefore = time.compareTo(dateTime); // -1 future, 1 past
    return ListTile(
      leading: CustomCircleAvatar(
        height: 45,
        width: 45,
        url: 'https://imgp.blurt.world/profileimage/${obj.name}/64x64',
      ),
      title: Text(obj.name),
      subtitle: Text("$text\n$ago"),
      trailing: isBefore == 1
          ? Stack(
              children: [
                Icon(
                  _iconForItem(item),
                  color: Colors.deepOrange,
                ),
                Container(
                  margin: const EdgeInsets.only(left: 30, top: 3),
                  child: const Icon(
                    Icons.circle,
                    color: Colors.deepOrange,
                    size: 15,
                  ),
                ),
              ],
            )
          : Icon(
              _iconForItem(item),
              color: Colors.deepOrange,
            ),
      onTap: () {
        onTapItem(item);
      },
    );
  }

  Widget _listView(List<UserNotificationItem> filtered) {
    return SafeArea(
      child: Stack(
        children: [
          Container(
            margin: const EdgeInsets.only(top: 25),
            child: ListView.separated(
              itemBuilder: (c, i) {
                return _listItem(filtered[i]);
              },
              separatorBuilder: (c, i) => const Divider(),
              itemCount: filtered.length,
            ),
          ),
          _fabContainer(),
          _filterTitle(listType.filterName),
        ],
      ),
    );
  }

  Widget _listForNotifications(List<UserNotificationItem> notifications) {
    var filtered = notifications;
    if (listType.filterName != null) {
      filtered = notifications.where((element) {
        return element.type == listType.filterName;
      }).toList();
    }
    if (filtered.isEmpty) {
      return _emptyContainer();
    }
    return _listView(filtered);
  }

  Widget _futureFeed(BlurtAppData appData) {
    return FutureBuilder(
      future: loadData,
      builder: (builder, snapshot) {
        if (snapshot.hasError) {
          return _errorState();
        } else if (snapshot.hasData &&
            snapshot.connectionState == ConnectionState.done) {
          var data = snapshot.data as List<UserNotificationItem>;
          data
              .where(
                  (element) => !appData.blockAndCoal.contains(element.author))
              .toList();
          return _listForNotifications(
              snapshot.data as List<UserNotificationItem>);
        } else {
          return const Center(child: CircularProgressIndicator());
        }
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    super.build(context);
    var appData = Provider.of<BlurtAppData>(context);
    if (loadData == null) {
      setState(() {
        dateTime = appData.notificationDateTime;
        loadData = BlurtCommunicator().getNotifications(widget.name, appData);
      });
    }
    return _futureFeed(appData);
  }
}
