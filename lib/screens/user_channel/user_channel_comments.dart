import 'dart:convert';

import 'package:blurt/bloc/server.dart';
import 'package:blurt/communication/feeds/blurt_communicator.dart';
import 'package:blurt/models/feed_response_item.dart';
import 'package:blurt/screens/post_details/post_details.dart';
import 'package:blurt/widgets/custom_circle_avatar.dart';
import 'package:blurt/widgets/error_state_widget.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;

class UserChannelComments extends StatefulWidget {
  const UserChannelComments({
    Key? key,
    required this.name,
    required this.appData,
    required this.route,
  }) : super(key: key);
  final String route;
  final String name;
  final BlurtAppData appData;

  @override
  State<UserChannelComments> createState() => _UserChannelCommentsState();
}

class _UserChannelCommentsState extends State<UserChannelComments> {
  late Future<void> loadData;
  late ScrollController controller;
  List<FeedResponseItem> items = [];
  var isLoadingNextPage = false;

  @override
  void initState() {
    super.initState();
    loadData = _loadInitialData();
    controller = ScrollController()..addListener(_scrollListener);
  }

  Future<void> _loadInitialData() async {
    var resultItems = await BlurtCommunicator().getFeedItems(
      getRequest(widget.appData.domain),
      widget.appData,
    );
    setState(() {
      items = resultItems;
    });
  }

  void _scrollListener() {
    if (controller.position.pixels == controller.position.maxScrollExtent &&
        !isLoadingNextPage) {
      _loadNextPage();
    }
  }

  Future<void> _loadNextPage() async {
    setState(() {
      isLoadingNextPage = true;
    });
    var resultItems = await BlurtCommunicator().getFeedMoreItems(
      getRequestNextPage(
        widget.appData.domain,
        items.last.permlink,
        items.last.author,
      ),
    );
    var keys = items.map((e) => e.permlink);
    resultItems = resultItems.where((e) => !keys.contains(e.permlink)).toList();
    setState(() {
      isLoadingNextPage = false;
      items += resultItems;
    });
  }

  http.Request getRequest(String domain) {
    var request = http.Request('POST', Uri.parse(domain));
    request.body = json.encode({
      "id": 0,
      "jsonrpc": "2.0",
      "method": "call",
      "params": [
        "condenser_api",
        "get_state",
        [
          "@${widget.name}/${widget.route}",
        ]
      ]
    });
    return request;
  }

  http.Request getRequestNextPage(
    String domain,
    String startPermlink,
    String author,
  ) {
    var request = http.Request('POST', Uri.parse(domain));
    if (widget.route == "comments") {
      request.body = json.encode({
        "id": 14,
        "jsonrpc": "2.0",
        "method": "call",
        "params": [
          "condenser_api",
          "get_discussions_by_comments",
          [
            {
              "limit": 20,
              "start_author": author,
              "start_permlink": startPermlink,
            }
          ]
        ]
      });
    } else {
      request.body = json.encode({
        "id": 33,
        "jsonrpc": "2.0",
        "method": "call",
        "params": [
          "condenser_api",
          "get_replies_by_last_update",
          [author, startPermlink, 20]
        ]
      });
    }
    return request;
  }

  Widget _errorState() {
    return ErrorStateWidget(
      onRetry: () {
        loadData = BlurtCommunicator().getFeedItems(
          getRequest(widget.appData.domain),
          widget.appData,
        );
      },
    );
  }

  Widget _list(List<FeedResponseItem> items) {
    if (items.isEmpty) {
      return Center(
        child: widget.route == "comment"
            ? Text('No comments from @${widget.name}')
            : Text('No replies for @${widget.name}'),
      );
    }
    items.sort((a, b) => b.createdAt.compareTo(a.createdAt));
    return Container(
      margin: const EdgeInsets.only(top: 30),
      child: ListView.separated(
        itemBuilder: (c, i) {
          if (i == items.length) {
            return ListTile(
              title: Center(
                child: Text(
                  isLoadingNextPage ? 'Loading Next page' : 'Next Page',
                ),
              ),
            );
          }
          var subtitle = '';
          if (items[i].body.length < 100) {
            subtitle = items[i].body;
          } else {
            subtitle = items[i].body.substring(0, 99);
          }
          var avatar = CustomCircleAvatar(
            height: 45,
            width: 45,
            url:
                'https://imgp.blurt.world/profileimage/${items[i].author}/64x64',
            borderColor: Colors.deepOrange,
          );
          return ListTile(
            leading: avatar,
            title: Text('RE: ${items[i].rootTitle}'),
            subtitle: Text(subtitle),
            onTap: () {
              var details = PostDetails(
                author: items[i].author,
                permlink: items[i].permlink,
              );
              var route = MaterialPageRoute(builder: (c) => details);
              Navigator.of(context).push(route);
            },
          );
        },
        separatorBuilder: (c, i) => const Divider(),
        itemCount: items.length + 1,
        controller: controller,
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
      future: loadData,
      builder: (builder, snapshot) {
        if (snapshot.hasError) {
          return _errorState();
        } else if (snapshot.connectionState == ConnectionState.done) {
          return Stack(
            children: [
              Row(
                children: [
                  const Spacer(),
                  Text(
                    widget.route == 'comments' ? 'Comments' : 'Replies',
                    style: Theme.of(context).textTheme.titleLarge,
                  ),
                  const Spacer(),
                ],
              ),
              _list(items),
            ],
          );
        } else {
          return const Center(child: CircularProgressIndicator());
        }
      },
    );
  }
}
