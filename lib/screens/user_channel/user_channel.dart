import 'dart:developer';

import 'package:adaptive_action_sheet/adaptive_action_sheet.dart';
import 'package:blurt/bloc/server.dart';
import 'package:blurt/models/login_bridge_response.dart';
import 'package:blurt/screens/home_screen.dart';
import 'package:blurt/screens/user_channel/user_channel_info_container.dart';
import 'package:blurt/screens/user_channel/user_channel_posts_new.dart';
import 'package:blurt/screens/user_channel/user_channel_users.dart';
import 'package:blurt/screens/user_channel/user_notifications.dart';
import 'package:blurt/screens/user_channel/user_transfer_history.dart';
import 'package:blurt/widgets/custom_circle_avatar.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_email_sender/flutter_email_sender.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:provider/provider.dart';
import 'package:url_launcher/url_launcher.dart';

class UserChannel extends StatefulWidget {
  const UserChannel({Key? key, required this.name}) : super(key: key);
  final String name;

  @override
  State<UserChannel> createState() => _UserChannelState();
}

class _UserChannelState extends State<UserChannel> {
  static const storage = FlutterSecureStorage();
  var selectedTabIndex = 0;
  var isLoading = false;
  var shouldGoBack = false;

  Widget _logOutButton(BlurtAppData appData) {
    return IconButton(
      onPressed: () async {
        showAdaptiveActionSheet(
          context: context,
          title: const Text('Are you sure that you want to log out?'),
          androidBorderRadius: 30,
          actions: [
            BottomSheetAction(
              title: const Text(
                'Yes. Log me out',
                style: TextStyle(color: Colors.red),
              ),
              leading: const Icon(Icons.logout, color: Colors.deepOrange),
              onPressed: (context) async {
                await storage.delete(key: 'username');
                await storage.delete(key: 'postingKey');
                await storage.delete(key: 'notification');
                setState(() {
                  server.updateUserData(null, appData);
                  Navigator.of(context).pop();
                  Navigator.of(context).pop();
                  var route =
                      MaterialPageRoute(builder: (c) => const HomePage());
                  Navigator.of(context).pushReplacement(route);
                });
              },
            )
          ],
          cancelAction: CancelAction(
            title: const Text(
              'Cancel',
              style: TextStyle(
                color: Colors.deepOrange,
              ),
            ),
          ),
        );
      },
      icon: const Icon(Icons.logout),
    );
  }

  String _titleText(BlurtAppData appData) {
    String title = "";
    switch (selectedTabIndex) {
      case 0:
        title = 'Blog';
        break;
      case 1:
        title = 'Followers, Follows, Blocked';
        break;
      case 2:
        title = 'Info';
        break;
      case 3:
        title = appData.userData?.username == widget.name
            ? 'Notifications'
            : 'History';
        break;
      case 4:
        title = 'History';
        break;
      default:
        title = 'Yo';
    }
    return title;
  }

  Widget _appBarTitle(BlurtAppData appData) {
    return Row(
      children: [
        CustomCircleAvatar(
            height: 34,
            width: 34,
            url: 'https://imgp.blurt.world/profileimage/${widget.name}/64x64'),
        const SizedBox(width: 5),
        Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              widget.name,
              style: Theme.of(context).textTheme.titleSmall,
            ),
            Text(
              _titleText(appData),
              style: Theme.of(context).textTheme.bodyLarge,
            ),
          ],
        )
      ],
    );
  }

  void showBottomSheetForUserFlagWithFee(BlurtAppData appData, String fees) {
    showAdaptiveActionSheet(
      context: context,
      title: Text('This operation will cost you $fees BLURT'),
      androidBorderRadius: 30,
      actions: [
        BottomSheetAction(
          title: Text(
            'Block ${widget.name}',
            style: const TextStyle(color: Colors.deepOrange),
          ),
          leading: const Icon(Icons.block, color: Colors.deepOrange),
          onPressed: (context) async {
            Navigator.of(context).pop();
            var platform = const MethodChannel('blog.blurt.blurt/auth');
            setState(() {
              isLoading = true;
            });
            final String result = await platform.invokeMethod('updateFollow', {
              'accountName': appData.userData!.username,
              'postingKey': appData.userData!.postingKey,
              'actionAccount': widget.name,
              'actionType': 'mute',
            });
            var response = LoginBridgeResponse.fromJsonString(result);
            if (response.error.isEmpty) {
              showMessage('You\'ve blocked/muted user - ${widget.name}');
            } else {
              showError(response.error);
            }
            setState(() {
              isLoading = false;
              if (response.error.isEmpty) {
                shouldGoBack = true;
              }
            });
          },
        ),
      ],
      cancelAction: CancelAction(
        title: const Text(
          'Cancel',
          style: TextStyle(
            color: Colors.deepOrange,
          ),
        ),
      ),
    );
  }

  void showBottomSheetForFlagging(BlurtAppData appData) {
    if (appData.userData == null) {
      showAdaptiveActionSheet(
        context: context,
        title: const Text('You must be logged in to report/block user.'),
        androidBorderRadius: 30,
        actions: [
          BottomSheetAction(
            title: const Text(
              'Okay',
              style: TextStyle(color: Colors.deepOrange),
            ),
            onPressed: (context) {
              Navigator.of(context).pop();
            },
          )
        ],
      );
    } else {
      showAdaptiveActionSheet(
        context: context,
        title: Text('Choose Action for user - ${widget.name}'),
        androidBorderRadius: 30,
        actions: [
          BottomSheetAction(
            title: const Text(
              'Block user',
              style: TextStyle(color: Colors.deepOrange),
            ),
            onPressed: (context) async {
              Navigator.of(context).pop();
              var platform = const MethodChannel('blog.blurt.blurt/auth');
              setState(() {
                isLoading = true;
              });
              final String result =
                  await platform.invokeMethod('updateFollowInfo', {
                'accountName': appData.userData!.username,
                'actionAccount': widget.name,
                'actionType': 'mute',
              });
              var response = LoginBridgeResponse.fromJsonString(result);
              log("Response error is ${response.error}");
              if (response.error.startsWith('fee: ')) {
                showBottomSheetForUserFlagWithFee(
                  appData,
                  response.error.replaceAll('fee: ', ''),
                );
              } else {
                showError(response.error);
              }
              setState(() {
                isLoading = false;
              });
            },
          ),
          BottomSheetAction(
            title: const Text(
              'Report user',
              style: TextStyle(color: Colors.deepOrange),
            ),
            onPressed: (context) async {
              Navigator.of(context).pop();
              final Email email = Email(
                body: 'I want to report User - @${widget.name}.',
                subject: 'Report user - @${widget.name}',
                recipients: ['user-report@blurt.foundation'],
                isHTML: false,
              );
              try {
                await FlutterEmailSender.send(email);
              } catch (e) {
                var data = Uri.encodeFull(
                    "subject=Report user - @${widget.name}&body=I want to report User - @${widget.name}");
                var url =
                    Uri.parse('mailto:user-report@blurt.foundation?$data');
                launchUrl(url);
                log('Something went wrong ${e.toString()}');
              }
            },
          ),
        ],
        cancelAction: CancelAction(
          title: const Text(
            'Cancel',
            style: TextStyle(color: Colors.deepOrange),
          ),
        ),
      );
    }
  }

  Widget _flagButton(BlurtAppData appData) {
    return IconButton(
      onPressed: () {
        showBottomSheetForFlagging(appData);
      },
      icon: const Icon(Icons.flag),
    );
  }

  void showError(String string) {
    var snackBar = SnackBar(content: Text('Error: $string'));
    ScaffoldMessenger.of(context).showSnackBar(snackBar);
  }

  void showMessage(String string) {
    var snackBar = SnackBar(content: Text('Message: $string'));
    ScaffoldMessenger.of(context).showSnackBar(snackBar);
  }

  @override
  Widget build(BuildContext context) {
    if (shouldGoBack) {
      Navigator.of(context).pop();
    }
    var appData = Provider.of<BlurtAppData>(context);
    var user = appData.userData;
    return DefaultTabController(
      length: user != null && user.username == widget.name ? 5 : 4,
      child: Builder(
        builder: (context) {
          final tabController = DefaultTabController.of(context);
          tabController.addListener(() {
            log("New tab index: ${tabController.index}");
            setState(() {
              selectedTabIndex = tabController.index;
            });
          });
          return Scaffold(
            appBar: AppBar(
              centerTitle: false,
              title: _appBarTitle(appData),
              actions: isLoading
                  ? null
                  : user != null && user.username == widget.name
                      ? [_logOutButton(appData)]
                      : [_flagButton(appData)],
              bottom: isLoading
                  ? null
                  : TabBar(
                      tabs: user != null && user.username == widget.name
                          ? const [
                              Tab(icon: Icon(Icons.text_snippet_outlined)),
                              Tab(icon: Icon(Icons.person_add_alt)),
                              Tab(icon: Icon(Icons.info_outline)),
                              Tab(icon: Icon(Icons.notifications)),
                              Tab(icon: Icon(Icons.wallet)),
                            ]
                          : const [
                              Tab(icon: Icon(Icons.text_snippet_outlined)),
                              Tab(icon: Icon(Icons.person_add_alt)),
                              Tab(icon: Icon(Icons.info_outline)),
                              Tab(icon: Icon(Icons.wallet)),
                            ],
                    ),
            ),
            body: isLoading
                ? const Center(child: CircularProgressIndicator())
                : TabBarView(
                    children: user != null && user.username == widget.name
                        ? [
                            UserChannelPostsNew(
                              name: widget.name,
                              appData: appData,
                            ),
                            UserChannelUsers(name: widget.name),
                            UserChannelInfoContainer(
                              name: widget.name,
                              domain: appData.domain,
                            ),
                            UserNotifications(name: widget.name),
                            UserTransferHistoryScreen(
                              name: widget.name,
                              appData: appData,
                            ),
                          ]
                        : [
                            UserChannelPostsNew(
                              name: widget.name,
                              appData: appData,
                            ),
                            UserChannelUsers(name: widget.name),
                            UserChannelInfoContainer(
                              name: widget.name,
                              domain: appData.domain,
                            ),
                            UserTransferHistoryScreen(
                              name: widget.name,
                              appData: appData,
                            ),
                          ],
                  ),
          );
        },
      ),
    );
  }
}
