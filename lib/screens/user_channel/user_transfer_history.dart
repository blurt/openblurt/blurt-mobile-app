import 'package:blurt/bloc/server.dart';
import 'package:blurt/communication/feeds/blurt_communicator.dart';
import 'package:blurt/models/blurt_transfer_history.dart';
import 'package:blurt/models/blurt_transfers.dart';
import 'package:blurt/models/login_bridge_response.dart';
import 'package:blurt/screens/post_details/post_details.dart';
import 'package:blurt/widgets/error_state_widget.dart';
import 'package:blurt/widgets/fab_custom.dart';
import 'package:blurt/widgets/fab_overlay.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

class UserTransferHistoryScreen extends StatefulWidget {
  const UserTransferHistoryScreen({
    Key? key,
    required this.name,
    required this.appData,
  }) : super(key: key);
  final String name;
  final BlurtAppData appData;

  @override
  State<UserTransferHistoryScreen> createState() =>
      _UserTransferHistoryScreenState();
}

enum TransferType {
  claims,
  transfers,
  transferPower,
  authorRewards,
  curatorRewards
}

class _UserTransferHistoryScreenState extends State<UserTransferHistoryScreen>
    with AutomaticKeepAliveClientMixin<UserTransferHistoryScreen> {
  @override
  bool get wantKeepAlive => true;

  var isMenuOpen = false;
  var type = TransferType.claims;

  late Future<BlurtTransferResponse> loadData;
  var isClaiming = false;
  static const platform = MethodChannel('blog.blurt.blurt/auth');

  @override
  void initState() {
    super.initState();
    loadData = BlurtCommunicator().getTransferHistory(
      widget.name,
      widget.appData.domain,
    );
  }

  Widget _errorState() {
    return ErrorStateWidget(onRetry: () {
      setState(() {
        loadData = BlurtCommunicator().getTransferHistory(
          widget.name,
          widget.appData.domain,
        );
      });
    });
  }

  Widget _emptyContainer() {
    return SafeArea(
      child: Center(
        child: Column(
          children: const [
            Spacer(),
            Text('No data to show'),
            SizedBox(height: 10),
            Icon(Icons.wallet),
            Spacer(),
          ],
        ),
      ),
    );
  }

  void claimRewards(BlurtTransferResponse response) async {
    var username = widget.appData.userData?.username;
    var postingKey = widget.appData.userData?.postingKey;
    var domain = widget.appData.domain;
    var rewardBlurt = response.rewardBlurt;
    var rewardVestingBalance = response.rewardVestingBalance;
    if (username == null ||
        postingKey == null ||
        rewardBlurt == null ||
        rewardVestingBalance == null) return;
    setState(() {
      isClaiming = true;
    });
    final String result = await platform.invokeMethod('claimRewards', {
      'accountName': username,
      'postingKey': postingKey,
      'rewardBlurt': rewardBlurt,
      'rewardVestingBalance': rewardVestingBalance,
      'domain': domain,
    });
    var platformResponse = LoginBridgeResponse.fromJsonString(result);
    if (platformResponse.valid && platformResponse.error.isEmpty) {
      setState(() {
        isClaiming = false;
        loadData = BlurtCommunicator().getTransferHistory(
          widget.name,
          widget.appData.domain,
        );
      });
    }
  }

  Widget _claimRewardsItem(BlurtTransferHistory claimItem) {
    var text = "";
    if (claimItem.operationData.rewardBlurt != "0.000 BLURT") {
      text = claimItem.operationData.rewardBlurt;
    }
    if (text.isNotEmpty) {
      text += " and ";
    }
    text += claimItem.operationData.rewardVests;
    return ListTile(
      title: const Text("Claim rewards"),
      subtitle: Text(text),
      trailing: Text(claimItem.dateTimeString),
    );
  }

  Widget _transferItem(BlurtTransferHistory claimItem, bool isPower) {
    var from = claimItem.operationData.from;
    var to = claimItem.operationData.to;
    var text = "";
    if (from == widget.name) {
      if (from == to && isPower) {
        text = "Powered up ${claimItem.operationData.amount}";
      } else {
        text =
            "Sent ${claimItem.operationData.amount}${isPower ? " Power" : ""} to $to";
      }
    } else {
      text =
          "Received ${claimItem.operationData.amount}${isPower ? " Power" : ""} from $from";
    }
    if (claimItem.operationData.memo.isNotEmpty) {
      text += "\nMemo: ${claimItem.operationData.memo}";
    }
    return ListTile(
      leading: Icon(
        isPower ? Icons.power_settings_new : Icons.compare_arrows,
        color: server.themeColor,
      ),
      title: Text(from == to && isPower ? "Power Up" : "Blurt Transfer"),
      subtitle: Text(text),
      trailing: Text(claimItem.dateTimeString),
    );
  }

  Widget _claimButton(BlurtTransferResponse response) {
    var hasRewards = response.rewardBlurt != null &&
        response.rewardVestingBlurt != null &&
        (response.rewardBlurt != "0.000 BLURT" ||
            response.rewardVestingBlurt != "0.000 BLURT");
    if (!hasRewards) {
      return const ListTile(
        title: Text('No rewards to claim'),
      );
    }
    var titleText = "";
    if (widget.appData.userData?.username == widget.name) {
      titleText = "Your rewards";
    } else {
      titleText = "@${widget.name}'s rewards";
    }
    return ListTile(
      title: Text(titleText),
      subtitle: Text(
          "${response.rewardBlurt ?? ""}\n${response.rewardVestingBlurt ?? ""} Power"),
      trailing: widget.appData.userData?.username == widget.name
          ? ElevatedButton(
              onPressed: () {
                claimRewards(response);
              },
              child: const Text('Claim'),
            )
          : null,
    );
  }

  Widget _rewardOnPost(BlurtTransferHistory item, bool isAuthor) {
    return ListTile(
      leading: Icon(
        isAuthor ? Icons.edit_note : Icons.thumb_up_sharp,
        color: server.themeColor,
      ),
      title: Text(isAuthor ? "Author Rewards" : "Curation Rewards"),
      subtitle: isAuthor
          ? Text("@${item.operationData.author}/${item.operationData.permlink}")
          : Text(
              "@${item.operationData.commentAuthor}/${item.operationData.commentPermlink}"),
      trailing: Text(item.dateTimeString),
      onTap: () {
        var screen = PostDetails(
          author: isAuthor
              ? item.operationData.author
              : item.operationData.commentAuthor,
          permlink: isAuthor
              ? item.operationData.permlink
              : item.operationData.commentPermlink,
        );
        var route = MaterialPageRoute(builder: (c) => screen);
        Navigator.of(context).push(route);
      },
    );
  }

  Widget _listForTransfers(BlurtTransferResponse response) {
    List<BlurtTransferHistory> history;
    String noDataText;
    switch (type) {
      case TransferType.transfers:
        history = response.transfer.transferHistory;
        noDataText = 'No recent blurt transfer history found';
        break;
      case TransferType.claims:
        history = response.claims.transferHistory;
        noDataText = 'No recent claims history found';
        break;
      case TransferType.transferPower:
        history = response.transferPower.transferHistory;
        noDataText = 'No recent blurt power transfer history found';
        break;
      case TransferType.authorRewards:
        history = response.authorRewards.transferHistory;
        noDataText = 'No recent author rewards history found';
        break;
      case TransferType.curatorRewards:
        history = response.curationRewards.transferHistory;
        noDataText = 'No recent curator rewards history found';
        break;
    }
    return SafeArea(
      child: Stack(
        children: [
          Container(
            margin: const EdgeInsets.only(top: 25),
            child: ListView.separated(
              itemBuilder: (c, i) {
                if (i == 0) {
                  return _claimButton(response);
                } else if (history.isEmpty) {
                  return ListTile(title: Text(noDataText));
                }
                var item = history[i - 1];
                switch (type) {
                  case TransferType.transfers:
                    return _transferItem(item, false);
                  case TransferType.claims:
                    return _claimRewardsItem(item);
                  case TransferType.transferPower:
                    return _transferItem(item, true);
                  case TransferType.authorRewards:
                    return _rewardOnPost(item, true);
                  case TransferType.curatorRewards:
                    return _rewardOnPost(item, false);
                }
              },
              separatorBuilder: (c, i) => const Divider(),
              itemCount: history.isEmpty ? 2 : history.length + 1,
            ),
          ),
          _fabContainer(),
        ],
      ),
    );
  }

  List<FabOverItemData> _fabItems() {
    List<FabOverItemData> fabItems = [
      FabOverItemData(
        displayName: 'Claimed Rewards',
        icon: Icons.add_card_sharp,
        onTap: () {
          setState(() {
            isMenuOpen = false;
            type = TransferType.claims;
          });
        },
      ),
      FabOverItemData(
        displayName: 'Transferred Blurt',
        icon: Icons.compare_arrows,
        onTap: () {
          setState(() {
            isMenuOpen = false;
            type = TransferType.transfers;
          });
        },
      ),
      FabOverItemData(
        displayName: 'Transferred Blurt Power',
        icon: Icons.power_settings_new,
        onTap: () {
          setState(() {
            isMenuOpen = false;
            type = TransferType.transferPower;
          });
        },
      ),
      FabOverItemData(
        displayName: 'Author Rewards',
        icon: Icons.edit_note,
        onTap: () {
          setState(() {
            isMenuOpen = false;
            type = TransferType.authorRewards;
          });
        },
      ),
      FabOverItemData(
        displayName: 'Curator Rewards',
        icon: Icons.thumb_up_sharp,
        onTap: () {
          setState(() {
            isMenuOpen = false;
            type = TransferType.curatorRewards;
          });
        },
      ),
      FabOverItemData(
        displayName: 'Close',
        icon: Icons.close,
        onTap: () {
          setState(() {
            isMenuOpen = false;
          });
        },
      ),
    ];
    return fabItems;
  }

  Widget _fabContainer() {
    if (!isMenuOpen) {
      return FabCustom(
        icon: Icons.filter_list_alt,
        onTap: () {
          setState(() {
            isMenuOpen = true;
          });
        },
      );
    }
    return FabOverlay(
      items: _fabItems(),
      onBackgroundTap: () {
        setState(() {
          isMenuOpen = false;
        });
      },
    );
  }

  Widget _filterTitle() {
    var text = "";
    switch (type) {
      case TransferType.transfers:
        text = "Showing Blurt Transfer History";
        break;
      case TransferType.claims:
        text = "Showing Blurt Claims History";
        break;
      case TransferType.transferPower:
        text = "Showing Blurt Power Transfer History";
        break;
      case TransferType.authorRewards:
        text = "Showing Author Rewards History";
        break;
      case TransferType.curatorRewards:
        text = "Showing Curator Rewards History";
        break;
    }
    return Column(
      children: [
        const SizedBox(height: 5),
        Row(
          children: [
            const Spacer(),
            Text(
              text,
              style: const TextStyle(color: Colors.deepOrange, fontSize: 15),
            ),
            const Spacer(),
          ],
        ),
      ],
    );
  }

  Widget _futureFeed() {
    return FutureBuilder(
      future: loadData,
      builder: (builder, snapshot) {
        if (snapshot.hasError) {
          return _errorState();
        } else if (snapshot.hasData &&
            snapshot.connectionState == ConnectionState.done) {
          return Stack(
            children: [
              _listForTransfers(snapshot.data as BlurtTransferResponse),
              _filterTitle(),
            ],
          );
        } else {
          return const Center(child: CircularProgressIndicator());
        }
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    super.build(context);
    return isClaiming
        ? const Center(child: CircularProgressIndicator())
        : _futureFeed();
  }
}
