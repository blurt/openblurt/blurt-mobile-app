import 'package:blurt/communication/feeds/blurt_communicator.dart';
import 'package:blurt/models/blurt_user_witness_votes.dart';
import 'package:blurt/screens/user_channel/user_channel.dart';
import 'package:blurt/widgets/custom_circle_avatar.dart';
import 'package:flutter/material.dart';

class UserChannelInfoWitnessVotes extends StatefulWidget {
  const UserChannelInfoWitnessVotes({
    Key? key,
    required this.domain,
    required this.name,
    required this.isGivenVotes,
  }) : super(key: key);
  final String name;
  final String domain;
  final bool isGivenVotes;

  @override
  State<UserChannelInfoWitnessVotes> createState() =>
      _UserChannelInfoWitnessVotesState();
}

class _UserChannelInfoWitnessVotesState
    extends State<UserChannelInfoWitnessVotes> {
  late Future<List<BlurtUserWitnessVotesObjectVoteItem>> getWitnessVotes;

  @override
  void initState() {
    super.initState();
    getWitnessVotes = widget.isGivenVotes
        ? BlurtCommunicator().getGivenWitnessVotes(
            widget.domain,
            widget.name,
          )
        : BlurtCommunicator().getWitnessVotes(
            widget.domain,
            widget.name,
          );
  }

  Widget _errorState() {
    return Column(
      children: [
        const Spacer(),
        Row(children: const [
          Spacer(),
          Text(
            'Oops! Something went wrong.\nPlease Try again.',
            textAlign: TextAlign.center,
          ),
          Spacer(),
        ]),
        const SizedBox(height: 20),
        ElevatedButton(
          onPressed: () {
            setState(() {
              getWitnessVotes = widget.isGivenVotes
                  ? BlurtCommunicator().getGivenWitnessVotes(
                      widget.domain,
                      widget.name,
                    )
                  : BlurtCommunicator().getWitnessVotes(
                      widget.domain,
                      widget.name,
                    );
            });
          },
          child: const Text('Retry'),
        ),
        const Spacer(),
      ],
    );
  }

  Widget _listTile(String name) {
    return ListTile(
      leading: CustomCircleAvatar(
          height: 45,
          width: 45,
          url: 'https://imgp.blurt.world/profileimage/$name/64x64'),
      title: Text(name),
      onTap: () {
        var screen = UserChannel(name: name);
        var route = MaterialPageRoute(builder: (c) => screen);
        Navigator.of(context).push(route);
      },
    );
  }

  Widget _listView(List<BlurtUserWitnessVotesObjectVoteItem> votesInfo) {
    var items = votesInfo
        .where((element) => widget.isGivenVotes
            ? element.account == widget.name
            : element.witness == widget.name)
        .toList();
    if (items.isEmpty) {
      return Center(
        child: widget.isGivenVotes
            ? Text("${widget.name} does not vote any witness")
            : Text("No Voters for Witness - ${widget.name}"),
      );
    }
    return ListView.separated(
      itemBuilder: (c, i) {
        if (i == 0) {
          return ListTile(
            title: widget.isGivenVotes
                ? Text("${items.length} Blurt witnesses")
                : Text("${items.length} Blurt users"),
            subtitle: widget.isGivenVotes
                ? Text(
                    "@${widget.name} votes for ${items.length} Blurt witnesses")
                : Text("Votes for witness - @${widget.name}"),
          );
        }
        return _listTile(
            widget.isGivenVotes ? items[i - 1].witness : items[i - 1].account);
      },
      separatorBuilder: (c, i) => const Divider(),
      itemCount: items.length + 1,
    );
  }

  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
      future: getWitnessVotes,
      builder: (builder, snapshot) {
        if (snapshot.hasError) {
          return _errorState();
        } else if (snapshot.hasData &&
            snapshot.connectionState == ConnectionState.done) {
          return _listView(
              snapshot.data as List<BlurtUserWitnessVotesObjectVoteItem>);
        } else {
          return const Center(child: CircularProgressIndicator());
        }
      },
    );
  }
}
