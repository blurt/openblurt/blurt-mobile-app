import 'dart:convert';

import 'package:blurt/bloc/server.dart';
import 'package:blurt/communication/feeds/blurt_communicator.dart';
import 'package:blurt/models/blurt_user_stream.dart';
import 'package:blurt/models/feed_response_item.dart';
import 'package:blurt/screens/post_details/post_details.dart';
import 'package:blurt/screens/user_channel/user_channel_comments.dart';
import 'package:blurt/widgets/error_state_widget.dart';
import 'package:blurt/widgets/fab_custom.dart';
import 'package:blurt/widgets/fab_overlay.dart';
import 'package:blurt/widgets/new_feed_item_widget.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:provider/provider.dart';

class UserChannelPostsNew extends StatefulWidget {
  const UserChannelPostsNew({
    Key? key,
    required this.name,
    required this.appData,
  }) : super(key: key);
  final String name;
  final BlurtAppData appData;

  @override
  State<UserChannelPostsNew> createState() => _UserChannelPostsNewState();
}

class _UserChannelPostsNewState extends State<UserChannelPostsNew>
    with AutomaticKeepAliveClientMixin<UserChannelPostsNew> {
  @override
  bool get wantKeepAlive => true;

  late Future<void> loadData;
  bool hideReBlurts = false;
  var isMenuOpen = false;

  var isShowingPosts = true;
  var isShowingComments = false;
  var isShowingReplies = false;

  var noMoreRecords = false;
  var loadingInitialFeed = false;
  var fetchingExtra = false;
  List<FeedResponseItem> items = [];
  late ScrollController controller;

  @override
  void initState() {
    super.initState();
    loadData = loadInitialFeed();
    controller = ScrollController()..addListener(_scrollListener);
  }

  void _scrollListener() {
    if (controller.position.pixels == controller.position.maxScrollExtent) {
      getNextFeedData();
    }
  }

  void getNextFeedData() async {
    var request = http.Request('POST', Uri.parse(widget.appData.domain));
    request.body = json.encode({
      "id": 7,
      "jsonrpc": "2.0",
      "method": "call",
      "params": [
        "condenser_api",
        "get_discussions_by_blog",
        [
          {
            "tag": widget.name,
            "limit": 20,
            "start_author": items.last.author,
            "start_permlink": items.last.permlink
          }
        ]
      ]
    });
    setState(() {
      fetchingExtra = true;
    });
    var records = await BlurtCommunicator().getFeedMoreItems(request);
    var itemsIds = items.map((e) => e.id);
    var newRecords = records.where((e) => !itemsIds.contains(e.id)).toList();
    newRecords.sort((a, b) {
      var aTime = a.createdAt;
      var bTime = b.createdAt;
      if (aTime.isAfter(bTime)) {
        return -1;
      } else if (bTime.isAfter(aTime)) {
        return 1;
      } else {
        return 0;
      }
    });
    newRecords.where((element) {
      return widget.appData.blockAndCoal.contains(element.author) == false ||
          (element.meta?.tags ?? []).contains('nsfw');
    }).toList();
    setState(() {
      items += newRecords;
      noMoreRecords = newRecords.isEmpty;
      fetchingExtra = false;
    });
  }

  Future<void> loadInitialFeed() async {
    setState(() {
      loadingInitialFeed = true;
    });
    var records = await BlurtCommunicator().getFeedItems(
      getRequest(widget.appData.domain),
      widget.appData,
    );
    var data = records;
    data.sort((a, b) {
      var aTime = a.createdAt;
      var bTime = b.createdAt;
      if (aTime.isAfter(bTime)) {
        return -1;
      } else if (bTime.isAfter(aTime)) {
        return 1;
      } else {
        return 0;
      }
    });
    setState(() {
      items = data.where((element) {
        return widget.appData.blockAndCoal.contains(element.author) == false ||
            (element.meta?.tags ?? []).contains('nsfw');
      }).toList();
      loadingInitialFeed = false;
    });
  }

  Widget _errorState() {
    return ErrorStateWidget(
      onRetry: () {
        loadData = BlurtCommunicator().getFeedItems(
          getRequest(widget.appData.domain),
          widget.appData,
        );
      },
    );
  }

  http.Request getRequest(String domain) {
    var request = http.Request('POST', Uri.parse(domain));
    request.body = json.encode({
      "id": 0,
      "jsonrpc": "2.0",
      "method": "call",
      "params": [
        "condenser_api",
        "get_state",
        [
          "@${widget.name}",
        ]
      ]
    });
    return request;
  }

  Widget _loadingDataForPullToRefresh() {
    return const Center(
      child: ListTile(
        title: Text('Loading Data', textAlign: TextAlign.center),
        subtitle: Text('Please wait', textAlign: TextAlign.center),
      ),
    );
  }

  Widget _listView() {
    return RefreshIndicator(
      onRefresh: () {
        return loadInitialFeed();
      },
      child: loadingInitialFeed
          ? _loadingDataForPullToRefresh()
          : ListView.separated(
              controller: controller,
              itemBuilder: (c, i) {
                if (i == items.length) {
                  return ListTile(
                    title: Center(
                      child: Text(
                        noMoreRecords
                            ? 'End of List'
                            : fetchingExtra
                                ? 'Loading Next page'
                                : 'Next Page',
                      ),
                    ),
                  );
                }
                return FeedItemWidget(
                  showUserAvatar: false,
                  item: items[i],
                  onPostTapped: () {
                    var screen = PostDetails(
                      author: items[i].author,
                      permlink: items[i].permlink,
                    );
                    var route = MaterialPageRoute(builder: (c) => screen);
                    Navigator.of(context).push(route);
                  },
                  onUserProfileTapped: () {},
                );
              },
              separatorBuilder: (c, i) => const Divider(
                height: 0,
                color: Colors.transparent,
              ),
              itemCount: items.length + 1,
            ),
    );
  }

  Widget _futureFeed(BlurtAppData appData) {
    return FutureBuilder(
      future: loadData,
      builder: (builder, snapshot) {
        if (snapshot.hasError) {
          return _errorState();
        } else if (snapshot.connectionState == ConnectionState.done) {
          return _listView();
        } else {
          return const Center(child: CircularProgressIndicator());
        }
      },
    );
  }

  List<FabOverItemData> _fabItems(BlurtUserData? user) {
    List<FabOverItemData> fabItems = (isShowingPosts)
        ? [
            FabOverItemData(
              displayName: 'Refresh',
              icon: Icons.refresh,
              onTap: () {
                setState(() {
                  isMenuOpen = false;
                  loadData = BlurtCommunicator().getFeedItems(
                    getRequest(widget.appData.domain),
                    widget.appData,
                  );
                });
              },
            ),
            FabOverItemData(
              displayName: hideReBlurts ? 'Show ReBlurts' : 'Hide ReBlurts',
              icon: Icons.repeat,
              onTap: () {
                setState(() {
                  isMenuOpen = false;
                  hideReBlurts = !hideReBlurts;
                });
              },
            ),
          ]
        : [
            FabOverItemData(
              displayName: 'Posts',
              icon: Icons.note_rounded,
              onTap: () {
                setState(() {
                  isMenuOpen = false;
                  isShowingPosts = true;
                  isShowingComments = false;
                  isShowingReplies = false;
                });
              },
            )
          ];
    if (isShowingPosts) {
      fabItems.add(
        FabOverItemData(
          displayName: 'Comments',
          icon: Icons.comment,
          onTap: () {
            setState(() {
              isMenuOpen = false;
              isShowingPosts = false;
              isShowingComments = true;
              isShowingReplies = false;
            });
          },
        ),
      );
      fabItems.add(
        FabOverItemData(
          displayName: 'Replies',
          icon: Icons.reply,
          onTap: () {
            setState(() {
              isMenuOpen = false;
              isShowingPosts = false;
              isShowingComments = false;
              isShowingReplies = true;
            });
          },
        ),
      );
    }
    fabItems.add(
      FabOverItemData(
        displayName: 'Close',
        icon: Icons.close,
        onTap: () {
          setState(() {
            isMenuOpen = false;
          });
        },
      ),
    );
    return fabItems;
  }

  Widget _fabContainer(BlurtUserData? user) {
    if (!isMenuOpen) {
      return FabCustom(
        icon: Icons.bolt,
        onTap: () {
          setState(() {
            isMenuOpen = true;
          });
        },
      );
    }
    return FabOverlay(
      items: _fabItems(user),
      onBackgroundTap: () {
        setState(() {
          isMenuOpen = false;
        });
      },
    );
  }

  Widget _actualContainer(BlurtAppData appData) {
    if (isShowingPosts) {
      return _futureFeed(appData);
    } else if (isShowingComments) {
      return UserChannelComments(
        name: widget.name,
        appData: appData,
        route: 'comments',
      );
    } else {
      return UserChannelComments(
        name: widget.name,
        appData: appData,
        route: 'recent-replies',
      );
    }
  }

  @override
  Widget build(BuildContext context) {
    super.build(context);
    var appData = Provider.of<BlurtAppData>(context);
    var user = appData.userData;
    return SafeArea(
      child: Stack(
        children: [
          _actualContainer(appData),
          _fabContainer(user),
        ],
      ),
    );
  }
}
