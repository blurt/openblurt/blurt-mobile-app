import 'dart:convert';

import 'package:blurt/bloc/server.dart';
import 'package:blurt/communication/feeds/blurt_communicator.dart';
import 'package:blurt/models/blurt_witness_info.dart';
import 'package:blurt/models/user_channel_info.dart';
import 'package:blurt/widgets/custom_circle_avatar.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:intl/intl.dart';
import 'package:provider/provider.dart';

class UserChannelInfo extends StatefulWidget {
  const UserChannelInfo(
      {Key? key, required this.name, required this.domain, required this.info})
      : super(key: key);
  final String name;
  final String domain;
  final BlurtWitnessInfo info;

  @override
  State<UserChannelInfo> createState() => _UserChannelInfoState();
}

class _UserChannelInfoState extends State<UserChannelInfo> {
  late Future<UserChannelInfoResponse> loadData;

  @override
  void initState() {
    super.initState();
    loadData = BlurtCommunicator().getUserChannelInfo(
      getRequest(widget.domain),
      widget.name,
    );
  }

  http.Request getRequest(String domain) {
    var request = http.Request('POST', Uri.parse(domain));
    request.body = json.encode({
      "id": 0,
      "jsonrpc": "2.0",
      "method": "call",
      "params": [
        "condenser_api",
        "get_state",
        [
          "@${widget.name}",
        ]
      ]
    });
    return request;
  }

  Widget _errorState() {
    return Column(
      children: [
        const Spacer(),
        Row(children: const [
          Spacer(),
          Text(
            'Oops! Something went wrong.\nPlease Try again.',
            textAlign: TextAlign.center,
          ),
          Spacer(),
        ]),
        const SizedBox(height: 20),
        ElevatedButton(
          onPressed: () {
            setState(() {
              loadData = BlurtCommunicator().getUserChannelInfo(
                getRequest(widget.domain),
                widget.name,
              );
            });
          },
          child: const Text('Retry'),
        ),
        const Spacer(),
      ],
    );
  }

  Widget _body(UserChannelInfoResponse data, bool isDarkTheme) {
    List<Widget> items = [];
    var color = isDarkTheme ? Colors.white : Colors.black;
    if (data.result.coverImage.isNotEmpty) {
      items.add(Container(
        height: 100,
        margin: const EdgeInsets.only(bottom: 10),
        child: Image.network(data.result.coverImage),
      ));
    }
    items.add(ListTile(
      leading: CustomCircleAvatar(
          height: 64,
          width: 64,
          url: 'https://imgp.blurt.world/profileimage/${widget.name}/64x64'),
      title: Text(
        data.result.name.isEmpty ? widget.name : data.result.name,
        style: Theme.of(context).textTheme.headline5,
      ),
    ));
    if (data.result.about.trim().isNotEmpty) {
      items.add(ListTile(
        leading: Icon(Icons.info, color: color),
        title: const Text('About'),
        subtitle: Text(data.result.about),
        onTap: () {},
      ));
    }
    if (data.result.location.trim().isNotEmpty) {
      items.add(ListTile(
        leading: Icon(Icons.location_on_outlined, color: color),
        title: const Text('Location'),
        subtitle: Text(data.result.location),
        onTap: () {},
      ));
    }
    items.add(ListTile(
      leading: Icon(Icons.newspaper, color: color),
      title: Text('${data.postCount} post(s)'),
      onTap: () {},
    ));
    items.add(ListTile(
      leading: Icon(Icons.attach_money, color: color),
      title: Text(data.balance),
      onTap: () {},
    ));
    items.add(ListTile(
      leading: Icon(Icons.electric_bolt, color: color),
      title: Text(data.blurtPower),
      onTap: () {},
    ));
    items.add(ListTile(
      leading: Icon(Icons.percent, color: color),
      title: Text('Voting Power: ${data.getVotingPower()}'),
      trailing: CircularProgressIndicator(
        value: data.votingPower / 100.0,
        backgroundColor: Colors.grey,
      ),
    ));
    if (widget.info.result != null) {
      items.add(ListTile(
        title: Text(
          '@${widget.name}\'s - Blurt Witness Info',
          textAlign: TextAlign.center,
          style: const TextStyle(fontWeight: FontWeight.w900),
        ),
      ));
      items.add(ListTile(
        title: const Text('Witness Since'),
        trailing: Text(widget.info.result!.created),
      ));
      var votes = double.tryParse(widget.info.result!.votes);
      if (votes != null) {
        votes = votes / 1000000;
        items.add(ListTile(
          title: const Text('Approved Power for Witness'),
          trailing: Text('${NumberFormat.compact().format(votes)} Blurt Power'),
        ));
      }
      items.add(ListTile(
        title: const Text('Account Creation Fee'),
        trailing: Text(widget.info.result!.props.accountCreationFee),
      ));
      items.add(ListTile(
        title: const Text('Operation Flat Fee'),
        trailing: Text(widget.info.result!.props.operationFlatFee),
      ));
      items.add(ListTile(
        title: const Text('Operation Flat Fee'),
        trailing: Text(widget.info.result!.props.operationFlatFee),
      ));
      items.add(ListTile(
        title: const Text('Bandwidth Fee'),
        trailing: Text(widget.info.result!.props.bandwidthKbytesFee),
      ));
      items.add(ListTile(
        title: const Text('Proposal Fee'),
        trailing: Text(widget.info.result!.props.proposalFee),
      ));
      items.add(ListTile(
        title: const Text('Witness Version'),
        trailing: Text(widget.info.result!.runningVersion),
      ));
      items.add(ListTile(
        title: const Text('Missed Blocks'),
        trailing: Text(widget.info.result!.totalMissed.toString()),
      ));
    }
    return Container(
      margin: const EdgeInsets.only(top: 10),
      child: ListView(
        children: items,
      ),
    );
  }

  Widget _futureFeed(bool isDarkTheme) {
    return FutureBuilder(
      future: loadData,
      builder: (builder, snapshot) {
        if (snapshot.hasError) {
          return _errorState();
        } else if (snapshot.hasData &&
            snapshot.connectionState == ConnectionState.done) {
          return _body(snapshot.data as UserChannelInfoResponse, isDarkTheme);
        } else {
          return const Center(child: CircularProgressIndicator());
        }
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    var appData = Provider.of<BlurtAppData>(context);
    var isDarkTheme = appData.isDarkMode;
    return _futureFeed(isDarkTheme);
  }
}
