import 'dart:convert';
import 'dart:developer';

import 'package:adaptive_action_sheet/adaptive_action_sheet.dart';
import 'package:blurt/bloc/server.dart';
import 'package:blurt/screens/eula/eula_screen.dart';
import 'package:blurt/screens/settings/change_server_screen.dart';
import 'package:blurt/screens/user_channel/user_channel.dart';
import 'package:blurt/widgets/custom_circle_avatar.dart';
import 'package:crypto/crypto.dart';
import 'package:flutter/material.dart';
import 'package:flutter_email_sender/flutter_email_sender.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:provider/provider.dart';
import 'package:url_launcher/url_launcher.dart';

class SettingsScreen extends StatefulWidget {
  const SettingsScreen({Key? key}) : super(key: key);

  @override
  State<SettingsScreen> createState() => _SettingsScreenState();
}

class _SettingsScreenState extends State<SettingsScreen> {
  Widget _changeTheme(bool isDarkMode, BlurtAppData appData) {
    var color = isDarkMode ? Colors.white : Colors.black;
    return ListTile(
      leading: isDarkMode
          ? Icon(Icons.wb_sunny, color: color)
          : Icon(Icons.mode_night, color: color),
      title: Text(isDarkMode ? "Switch to Light Mode" : "Switch to Dark Mode"),
      onTap: () async {
        const storage = FlutterSecureStorage();
        await storage.write(
          key: 'is_dark_mode',
          // isDarkMode is enabled, set no to flip
          // isDarkMode is disabled set yes to flip
          value: isDarkMode ? 'no' : 'yes',
        );
        server.changeTheme(!isDarkMode, appData);
      },
    );
  }

  BottomSheetAction getAction(String serverUrl, BlurtAppData appData) {
    return BottomSheetAction(
      title: Text(
        serverUrl,
        style: const TextStyle(color: Colors.deepOrange),
      ),
      onPressed: (context) async {
        Navigator.of(context).pop();
        const storage = FlutterSecureStorage();
        await storage.write(key: 'domain', value: serverUrl);
        server.changeDomain(serverUrl, appData);
      },
    );
  }

  void showBottomSheetForServer(BlurtAppData appData) {
    var option = BottomSheetAction(
      title: const Text(
        'Custom',
        style: TextStyle(color: Colors.deepOrange),
      ),
      onPressed: (context) {
        Navigator.of(context).pop();
        var route = MaterialPageRoute(
          builder: (c) => const ChangeServerScreen(),
        );
        Navigator.of(context).push(route);
      },
    );
    var list = [
      'https://rpc.blurt.world/',
      'https://blurt-rpc.saboin.com/',
      'https://rpc.blurtlatam.com/',
      'https://rpc.blurt.live/',
      'https://rpc.blurt.one/ ',
    ].map((e) => getAction(e, appData)).toList();
    list.add(option);
    showAdaptiveActionSheet(
      context: context,
      title: const Text('Select RPC Server'),
      androidBorderRadius: 30,
      actions: list,
      cancelAction: CancelAction(
        title: const Text(
          'Cancel',
          style: TextStyle(color: Colors.deepOrange),
        ),
      ),
    );
  }

  Widget _changeServer(bool isDarkMode, String domain, BlurtAppData appData) {
    var color = isDarkMode ? Colors.white : Colors.black;
    return ListTile(
      leading: Icon(Icons.public, color: color),
      title: const Text('Change Server'),
      subtitle: Text(domain),
      onTap: () {
        showBottomSheetForServer(appData);
      },
    );
  }

  ListTile _iOSType(BlurtAppData appData) {
    return ListTile(
      leading: const Icon(Icons.numbers, color: Colors.deepOrange),
      title: const Text('Show Code'),
      onTap: () {
        if (appData.userData == null) {
          const snackBar = SnackBar(content: Text('You need to login first.'));
          ScaffoldMessenger.of(context).showSnackBar(snackBar);
        } else {
          var string = '${appData.userData!.username}_blurt';
          var text = md5.convert(utf8.encode(string)).toString();
          var snackBar = SnackBar(
            content: Text('Your participation code is $text'),
          );
          ScaffoldMessenger.of(context).showSnackBar(snackBar);
        }
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    var appData = Provider.of<BlurtAppData>(context);
    var isDarkMode = appData.isDarkMode;
    var color = isDarkMode ? Colors.white : Colors.black;
    var screen = const UserChannel(name: 'sagarkothari88');
    var route = MaterialPageRoute(builder: (c) => screen);
    var url = Uri.parse(
        'https://blurtwallet.com/~witnesses?highlight=sagarkothari88');
    var items = [
      _changeTheme(isDarkMode, appData),
      _changeServer(isDarkMode, appData.domain, appData),
      ListTile(
        leading: Icon(Icons.support_agent, color: color),
        title: const Text('Contact / Support / Helpline'),
        onTap: () async {
          final Email email = Email(
            body: 'Describe your issue here. I want help with...',
            subject: 'Need Support',
            recipients: ['help-line-mobile-app@blurt.foundation'],
            isHTML: false,
          );
          try {
            await FlutterEmailSender.send(email);
          } catch (e) {
            var data = Uri.encodeFull(
                "subject=Need Support&body=Describe your issue here. I want help with...");
            var url =
                Uri.parse('mailto:help-line-mobile-app@blurt.foundation?$data');
            launchUrl(url);
            log('Something went wrong ${e.toString()}');
          }
        },
      ),
      ListTile(
        leading: Icon(Icons.info, color: color),
        title: const Text('Developed & maintained by @sagarkothari88'),
        onTap: () {
          launchUrl(url);
        },
        trailing: IconButton(
          onPressed: () {
            Navigator.of(context).push(route);
          },
          icon: const CustomCircleAvatar(
              height: 45,
              width: 45,
              url:
                  'https://imgp.blurt.world/profileimage/sagarkothari88/64x64'),
        ),
      ),
      ListTile(
        leading: Icon(Icons.info, color: color),
        title: const Text('Vote @sagarkothari88 as your Blurt Witness'),
        onTap: () {
          launchUrl(url);
        },
        trailing: IconButton(
          icon: const CustomCircleAvatar(
              height: 45,
              width: 45,
              url:
                  'https://imgp.blurt.world/profileimage/sagarkothari88/64x64'),
          onPressed: () {
            launchUrl(url);
          },
        ),
      ),
      ListTile(
        leading: Icon(Icons.note_alt_rounded, color: color),
        title: const Text('End User License Agreement'),
        onTap: () {
          var screen = const BlurtAppEulaScreen(fromSettings: true);
          var route = MaterialPageRoute(builder: (c) => screen);
          Navigator.of(context).push(route);
        },
      ),
    ];
    // if (Platform.isIOS) {
    //   items.add(_iOSType(appData));
    // }
    return Container(
      margin: const EdgeInsets.only(top: 10),
      child: ListView.separated(
        itemBuilder: (c, i) {
          return items[i];
        },
        separatorBuilder: (c, i) => const Divider(),
        itemCount: items.length,
      ),
    );
  }
}
