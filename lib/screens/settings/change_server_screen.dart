import 'package:blurt/bloc/server.dart';
import 'package:flutter/material.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:provider/provider.dart';

class ChangeServerScreen extends StatefulWidget {
  const ChangeServerScreen({Key? key}) : super(key: key);

  @override
  State<ChangeServerScreen> createState() => _ChangeServerScreenState();
}

class _ChangeServerScreenState extends State<ChangeServerScreen> {
  var url = "";
  var isLoading = false;
  var controller = TextEditingController();

  void onChangeTapped(BlurtAppData appData) async {
    const storage = FlutterSecureStorage();
    await storage.write(key: 'domain', value: url);
    server.changeDomain(url, appData);
    setState(() {
      Navigator.of(context).pop();
    });
  }

  Widget _serverField(BlurtAppData appData) {
    var appData = Provider.of<BlurtAppData>(context);
    var domain = appData.domain;
    if (url.isEmpty) {
      setState(() {
        url = domain;
        controller.text = domain;
      });
    }
    return Container(
      margin: const EdgeInsets.all(10),
      child: Column(
        children: [
          TextField(
            controller: controller,
            decoration: const InputDecoration(
              icon: Icon(Icons.public),
              label: Text('Blurt RPC Node URL'),
              hintText: 'Enter url here',
            ),
            autocorrect: false,
            onChanged: (value) {
              setState(() {
                url = value;
              });
            },
          ),
          const SizedBox(height: 20),
          isLoading
              ? const CircularProgressIndicator()
              : ElevatedButton(
                  onPressed: () {
                    onChangeTapped(appData);
                  },
                  child: const Text('Save'),
                ),
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    var appData = Provider.of<BlurtAppData>(context);
    return Scaffold(
      appBar: AppBar(
        title: const Text('Change Server'),
      ),
      body: _serverField(appData),
    );
  }
}
