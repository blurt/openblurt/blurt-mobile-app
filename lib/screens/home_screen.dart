import 'dart:developer';

import 'package:blurt/bloc/server.dart';
import 'package:blurt/models/blurt_user_stream.dart';
import 'package:blurt/models/drawer_item.dart';
import 'package:blurt/screens/feed/new_feed_screen.dart';
import 'package:blurt/screens/feed/tags/tags_list.dart';
import 'package:blurt/screens/login_screen.dart';
import 'package:blurt/screens/settings/settings_screen.dart';
import 'package:blurt/screens/user_channel/user_channel.dart';
import 'package:blurt/widgets/custom_circle_avatar.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class HomePage extends StatefulWidget {
  const HomePage({
    Key? key,
  }) : super(key: key);

  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  var selectedTabIndex = 0;

  String _titleText(BlurtUserData? user) {
    var noUserItems = ['Hot', 'Trending', 'New', 'Tags', 'Settings'];
    var userItems = ['My Feed', 'Hot', 'Trending', 'New', 'Tags', 'Settings'];
    var items = user != null ? userItems : noUserItems;
    return items[selectedTabIndex];
  }

  List<Tab> _tabs(BlurtUserData? user) {
    var noUserTabs = [
      const Tab(icon: Icon(Icons.local_fire_department_outlined)),
      const Tab(icon: Icon(Icons.trending_up)),
      const Tab(icon: Icon(Icons.pin)),
      const Tab(icon: Icon(Icons.tag)),
      const Tab(icon: Icon(Icons.settings)),
    ];
    var userTabs = [
      const Tab(icon: Icon(Icons.person_pin_sharp)),
      const Tab(icon: Icon(Icons.local_fire_department_outlined)),
      const Tab(icon: Icon(Icons.trending_up)),
      const Tab(icon: Icon(Icons.pin)),
      const Tab(icon: Icon(Icons.tag)),
      const Tab(icon: Icon(Icons.settings)),
    ];
    var tabs = user != null ? userTabs : noUserTabs;
    return tabs;
  }

  List<Widget> _tabViews(BlurtAppData appData) {
    var hotFeed = NewFeedScreen(
      item: DrawerItem(name: "Hot", icon: Icons.local_fire_department_outlined),
      appData: appData,
    );
    var trendingFeed = NewFeedScreen(
      item: DrawerItem(name: "Trending", icon: Icons.trending_up),
      appData: appData,
    );
    var newFeed = NewFeedScreen(
      item: DrawerItem(name: "New", icon: Icons.new_label),
      appData: appData,
    );
    var tags = TagsListScreen(domain: appData.domain);
    var settings = const SettingsScreen();
    var myFeed = NewFeedScreen(
      item: DrawerItem(name: "My Feed", icon: Icons.person_pin_sharp),
      appData: appData,
    );
    var noUserItems = [hotFeed, trendingFeed, newFeed, tags, settings];
    var userItems = [myFeed, hotFeed, trendingFeed, newFeed, tags, settings];
    return appData.userData != null ? userItems : noUserItems;
  }

  List<Widget> _actionBarItems(BlurtUserData? user) {
    if (user != null) {
      return [
        IconButton(
          onPressed: () {
            var screen = UserChannel(name: user.username);
            var route = MaterialPageRoute(builder: (c) => screen);
            Navigator.of(context).push(route);
          },
          icon: CustomCircleAvatar(
              height: 45,
              width: 45,
              url:
                  'https://imgp.blurt.world/profileimage/${user.username}/64x64'),
        ),
        const SizedBox(width: 10),
      ];
    } else {
      return [
        IconButton(
          onPressed: () {
            var screen = const LoginScreen();
            var route = MaterialPageRoute(builder: (c) => screen);
            Navigator.of(context).push(route);
          },
          icon: Column(
            children: [
              const Icon(Icons.person),
              Text(
                'Login',
                style: Theme.of(context).textTheme.bodySmall,
              ),
            ],
          ),
        ),
        const SizedBox(width: 10),
      ];
    }
  }

  @override
  Widget build(BuildContext context) {
    var data = Provider.of<BlurtAppData>(context);
    var user = data.userData;
    var count = _tabs(user).length;

    return DefaultTabController(
      length: count,
      child: Builder(
        builder: (context) {
          final tabController = DefaultTabController.of(context);
          tabController.addListener(() {
            log("New tab index: ${tabController.index}");
            setState(() {
              selectedTabIndex = tabController.index;
            });
          });
          return Scaffold(
            appBar: AppBar(
              centerTitle: false,
              title: Row(children: [
                Image.asset('assets/icon44.png'),
                const SizedBox(width: 5),
                Text(_titleText(user)),
              ]),
              actions: _actionBarItems(user),
              bottom: TabBar(
                tabs: _tabs(user),
              ),
            ),
            body: TabBarView(
              children: _tabViews(data),
            ),
          );
        },
      ),
    );
  }
}
