import 'dart:convert';
import 'dart:developer';

import 'package:blurt/bloc/server.dart';
import 'package:blurt/models/blurt_chain_properties.dart';
import 'package:blurt/models/feed_response_item.dart';
import 'package:blurt/models/login_bridge_response.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:http/http.dart' as http;
import 'package:provider/provider.dart';

class PostUpvoteBottomScreen extends StatefulWidget {
  const PostUpvoteBottomScreen({
    Key? key,
    required this.appData,
    required this.item,
    required this.onDone,
  }) : super(key: key);
  final BlurtAppData appData;
  final FeedResponseItem item;
  final Function onDone;

  @override
  State<PostUpvoteBottomScreen> createState() => _PostUpvoteBottomScreenState();
}

class _PostUpvoteBottomScreenState extends State<PostUpvoteBottomScreen> {
  var sliderValue = 1.0;
  late Future<BlurtChainProperties> future;
  var isUpvoting = false;

  @override
  void initState() {
    super.initState();
    future = getProperties();
  }

  Future<BlurtChainProperties> getProperties() async {
    var request = http.Request('POST', Uri.parse(widget.appData.domain));
    request.body = json.encode({
      "jsonrpc": "2.0",
      "method": "call",
      "params": ["condenser_api", "get_chain_properties", []]
    });
    http.StreamedResponse response = await request.send();
    if (response.statusCode == 200) {
      var responseString = await response.stream.bytesToString();
      return BlurtChainProperties.fromJsonString(responseString);
    } else {
      throw response.reasonPhrase.toString();
    }
  }

  Widget _upVoteSlider(BlurtChainProperties chainProperties) {
    var data = Provider.of<BlurtAppData>(context);
    var user = data.userData;
    if (user == null) return Container();
    var voteValue = sliderValue * 100;
    var intVoteValue = voteValue.round();
    var map = {
      "voter": user.username,
      "author": widget.item.author,
      "permlink": widget.item.permlink,
      "weight": intVoteValue * 100.0
    };
    var string = json.encode(map);
    var length = string.length.toDouble();
    var bwFee = ((length / 1024.0) * chainProperties.bandwidthFee);
    if (bwFee < 0.001) {
      bwFee = 0.001;
    }
    var fee = chainProperties.flatFee + bwFee;
    log('Fee update - $fee');
    return Column(
      children: [
        const Spacer(),
        Slider(
          value: sliderValue,
          min: 0.0,
          divisions: 20,
          label: '${(sliderValue * 100).round()} %',
          activeColor: Theme.of(context).colorScheme.secondary,
          onChanged: (val) {
            setState(() {
              sliderValue = val;
            });
          },
        ),
        const SizedBox(height: 10),
        Text(
          '${(sliderValue * 100).toStringAsFixed(2)} %\n\nFees: ${fee.toStringAsFixed(3)} BLURT',
          textAlign: TextAlign.center,
        ),
        const Spacer(),
      ],
    );
  }

  void showError(String string) {
    var snackBar = SnackBar(content: Text('Error: $string'));
    ScaffoldMessenger.of(context).showSnackBar(snackBar);
  }

  void saveButtonTapped() async {
    setState(() {
      isUpvoting = true;
    });
    try {
      var platform = const MethodChannel('blog.blurt.blurt/auth');
      var voteValue = sliderValue * 100;
      final String result = await platform.invokeMethod('upvote', {
        'username': widget.appData.userData!.username,
        'postingKey': widget.appData.userData!.postingKey,
        'domain': widget.appData.domain,
        'author': widget.item.author,
        'permlink': widget.item.permlink,
        'weight': voteValue.round() * 100
      });
      var response = LoginBridgeResponse.fromJsonString(result);
      if (response.valid && response.error.isEmpty) {
        log("Successful upvote and bridge communication");
        setState(() {
          widget.onDone();
          Navigator.of(context).pop();
        });
      }
    } catch (e) {
      showError('Something went wrong.\n${e.toString()}');
    } finally {
      setState(() {
        isUpvoting = false;
      });
    }
  }

  Widget _body(BlurtChainProperties props) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Upvote'),
        actions: isUpvoting
            ? []
            : [
                IconButton(
                  onPressed: () {
                    saveButtonTapped();
                  },
                  icon: const Icon(Icons.favorite),
                ),
              ],
        leading: IconButton(
          icon: const Icon(Icons.close),
          onPressed: () {
            Navigator.of(context).pop();
          },
        ),
      ),
      body: isUpvoting
          ? const Center(child: CircularProgressIndicator())
          : _upVoteSlider(props),
    );
  }

  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
      future: future,
      builder: (builder, snapshot) {
        if (snapshot.hasError) {
          return const Scaffold(
            body: Center(
              child: Text('Something went wrong.\nPlease try again later.'),
            ),
          );
        } else if (snapshot.connectionState == ConnectionState.done) {
          return _body(snapshot.data as BlurtChainProperties);
        } else {
          return const Scaffold(
            body: Center(child: CircularProgressIndicator()),
          );
        }
      },
    );
  }
}
