import 'dart:async';
import 'dart:convert';
import 'dart:developer';

import 'package:better_player/better_player.dart';
import 'package:blurt/bloc/server.dart';
import 'package:blurt/models/blurt_user_stream.dart';
import 'package:blurt/models/feed_response.dart';
import 'package:blurt/models/feed_response_item.dart';
import 'package:blurt/screens/add_a_post/add_post.dart';
import 'package:blurt/screens/post_details/post_details_comments.dart';
import 'package:blurt/screens/post_details/post_upvote_bottom_screen.dart';
import 'package:blurt/screens/post_details/post_voters.dart';
import 'package:blurt/screens/user_channel/user_channel.dart';
import 'package:blurt/widgets/custom_circle_avatar.dart';
import 'package:blurt/widgets/fab_custom.dart';
import 'package:blurt/widgets/fab_overlay.dart';
import 'package:flutter/material.dart';
import 'package:flutter_email_sender/flutter_email_sender.dart';
import 'package:http/http.dart' as http;
import 'package:markdown_widget/markdown_widget.dart';
import 'package:provider/provider.dart';
import 'package:reading_time/reading_time.dart';
import 'package:share_plus/share_plus.dart';
import 'package:timeago/timeago.dart' as timeago;
import 'package:url_launcher/url_launcher.dart';
import 'package:webview_flutter/webview_flutter.dart';
import 'package:youtube_player_iframe/youtube_player_iframe.dart';

class PostDetails extends StatefulWidget {
  const PostDetails({
    Key? key,
    required this.author,
    required this.permlink,
  }) : super(key: key);
  final String author;
  final String permlink;

  @override
  State<PostDetails> createState() => _PostDetailsState();
}

class _PostDetailsState extends State<PostDetails> {
  Future<PostDetailsContent>? _futureToLoadFeedDetails;
  var selectedTabIndex = 0;
  final controller = Completer<WebViewController>();
  var isMenuOpen = false;
  @override
  void initState() {
    super.initState();
    _futureToLoadFeedDetails = getFeedDetails();
  }

  Widget _content(
      FeedResponseItem item, bool isDarkMode, String contentString) {
    return Container(
      margin: const EdgeInsets.all(10),
      child: MarkdownWidget(
        data: contentString,
        styleConfig: StyleConfig(
          markdownTheme:
              isDarkMode ? MarkdownTheme.darkTheme : MarkdownTheme.lightTheme,
          imgBuilder: (String url, attributes) {
            return Image.network(url);
          },
          pConfig: PConfig(
            custom: (node) {
              if (node.tag == "youtube") {
                return YoutubePlayer(
                  controller: YoutubePlayerController.fromVideoId(
                      videoId: node.attributes["id"]!),
                  aspectRatio: 16 / 9,
                );
              } else if (node.tag == "dtube") {
                log('URL is - ${node.attributes["url"]!}');
                return BetterPlayer.network(
                  node.attributes["url"]!,
                  betterPlayerConfiguration: const BetterPlayerConfiguration(
                    aspectRatio: 16 / 9,
                  ),
                );
              } else if (node.tag == "hr") {
                return Container(height: 1);
              } else if (node.tag == "hr") {
                return Container(height: 1);
              }
              return Container(height: 1);
            },
            linkStyle: TextStyle(color: server.themeColor),
            onLinkTap: (url) {
              // open url here
              if (url != null) {
                var uri = Uri.parse(url);
                launchUrl(uri);
              }
            },
          ),
        ),
      ),
    );
  }

  Future<PostDetailsContent> getFeedDetails() async {
    var request = http.Request('POST', Uri.parse('https://rpc.blurt.world/'));
    request.body = json.encode({
      "jsonrpc": "2.0",
      "method": "call",
      "params": [
        "condenser_api",
        "get_state",
        ["blurt/@${widget.author}/${widget.permlink}"]
      ]
    });
    http.StreamedResponse response = await request.send();
    if (response.statusCode == 200) {
      var responseString = await response.stream.bytesToString();
      var contentKey = "${widget.author}/${widget.permlink}";
      var content = SingleItemResponse.fromJsonStringForKey(
          string: responseString, forKey: contentKey);
      var comments = CommentsResponse.excludingKeyFromJson(
          string: responseString, forKey: contentKey);
      return PostDetailsContent(comments: comments, content: content);
    } else {
      throw response.reasonPhrase.toString();
    }
  }

  void showError(String string) {
    var snackBar = SnackBar(content: Text('Error: $string'));
    ScaffoldMessenger.of(context).showSnackBar(snackBar);
  }

  Widget _errorState() {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.author),
      ),
      body: Column(
        children: [
          const Spacer(),
          Row(children: const [
            Spacer(),
            Text(
              'Oops! Something went wrong.\nPlease Try again.',
              textAlign: TextAlign.center,
            ),
            Spacer(),
          ]),
          const SizedBox(height: 20),
          ElevatedButton(
            onPressed: () {
              setState(() {
                _futureToLoadFeedDetails = getFeedDetails();
              });
            },
            child: const Text('Retry'),
          ),
          const Spacer(),
        ],
      ),
    );
  }

  Widget _listViewForVoters(FeedResponseItem item) {
    return PostVoters(
      voters: item.activeVotes,
      payout: item.payout,
      netResourceShares: item.netResourceShares,
      totalShares:
          item.activeVotes.map((e) => e.resourceShares).reduce((v, e) => v + e),
    );
  }

  String _processString(String data) {
    var newString = data.replaceAllMapped(
        RegExp(r'(https:\/\/www.youtube.com\/watch\?v=)(...........)'),
        (match) {
      if (match.group(2) != null) {
        return '<youtube id="${match.group(2)}"></youtube>';
      }
      return '';
    });
    newString = newString.replaceAllMapped(
        RegExp(r'(https:\/\/youtu.be\/)(...........)'), (match) {
      if (match.group(2) != null) {
        return '<youtube id="${match.group(2)}"></youtube>';
      }
      return '';
    });
    newString = newString.replaceAllMapped(
        RegExp(r'(https:\/\/d\.tube\/\#\!\/v\/.+\/)(.{46})'), (match) {
      if (match.group(2) != null) {
        return '<dtube url="https://player.d.tube/ipfs/${match.group(2)}"></dtube>';
      }
      return '';
    });
    newString = newString.replaceAllMapped(
        RegExp(r'(https:\/\/d\.tube\/\#\!\/v\/.+\/)(...........)'), (match) {
      if (match.group(2) != null) {
        return '<youtube id="${match.group(2)}"></youtube>';
      }
      return '';
    });
    newString =
        newString.replaceAllMapped(RegExp(r'\!\[.+\]\((.+)\)'), (match) {
      var image = match.group(1);
      return '<img src="$image" />';
    });

    return newString;
  }

  Widget _actionItem(FeedResponseItem item, BlurtUserData? user) {
    return IconButton(
      onPressed: () {
        var screen = UserChannel(name: item.author);
        var route = MaterialPageRoute(builder: (c) => screen);
        Navigator.of(context).push(route);
      },
      icon: CustomCircleAvatar(
          height: 45,
          width: 45,
          url: 'https://imgp.blurt.world/profileimage/${item.author}/64x64'),
    );
  }

  List<FabOverItemData> _fabItems(BlurtAppData appData, FeedResponseItem item) {
    var fabItems = [
      FabOverItemData(
        displayName: 'Share',
        icon: Icons.share,
        onTap: () {
          setState(() {
            isMenuOpen = false;
            Navigator.of(context).pop();
            Share.share(
              'https://blurt.blog/@${widget.author}/${widget.permlink}',
            );
          });
        },
      ),
      FabOverItemData(
        displayName: 'Reply',
        icon: Icons.reply,
        onTap: () {
          setState(() {
            isMenuOpen = false;
          });
          // showError('Feature is under development');
          var screen = AddPostScreen(
            parentPermlink: item.permlink,
            parentAuthor: item.author,
            onDone: () {
              setState(() {
                _futureToLoadFeedDetails = getFeedDetails();
              });
            },
          );
          var route = MaterialPageRoute(builder: (c) => screen);
          Navigator.of(context).push(route);
        },
      ),
    ];
    if (appData.userData?.username != null &&
        appData.userData?.postingKey != null) {
      if (item.activeVotes
          .where((e) => e.voter == appData.userData!.username)
          .isEmpty) {
        fabItems.add(
          FabOverItemData(
            displayName: 'Upvote',
            icon: Icons.favorite,
            onTap: () {
              setState(() {
                isMenuOpen = false;
              });
              showModalBottomSheet(
                context: context,
                useSafeArea: true,
                builder: (b) {
                  return PostUpvoteBottomScreen(
                    appData: appData,
                    item: item,
                    onDone: () {
                      setState(() {
                        _futureToLoadFeedDetails = getFeedDetails();
                      });
                    },
                  );
                },
              );
            },
          ),
        );
      }
    }
    if (item.title.isEmpty &&
        item.rootPermlink.isNotEmpty &&
        item.rootAuthor.isNotEmpty) {
      if (item.rootPermlink == item.parentPermlink &&
          item.rootAuthor == item.parentAuthor) {
        fabItems.insert(
          0,
          FabOverItemData(
            displayName: 'View the Full Context',
            icon: Icons.zoom_out_outlined,
            onTap: () async {
              setState(() {
                isMenuOpen = false;
                Navigator.of(context).push(
                  MaterialPageRoute(
                    builder: (c) => PostDetails(
                        author: item.rootAuthor, permlink: item.rootPermlink),
                  ),
                );
              });
            },
          ),
        );
      } else if (item.parentAuthor.isNotEmpty &&
          item.parentPermlink.isNotEmpty) {
        fabItems.insert(
          0,
          FabOverItemData(
            displayName: 'View the Full Context',
            icon: Icons.zoom_out_outlined,
            onTap: () async {
              setState(() {
                isMenuOpen = false;
                Navigator.of(context).push(
                  MaterialPageRoute(
                    builder: (c) => PostDetails(
                      author: item.rootAuthor,
                      permlink: item.rootPermlink,
                    ),
                  ),
                );
              });
            },
          ),
        );
        fabItems.insert(
          0,
          FabOverItemData(
            displayName: 'View the Direct Parent',
            icon: Icons.upgrade,
            onTap: () async {
              setState(() {
                isMenuOpen = false;
                Navigator.of(context).push(
                  MaterialPageRoute(
                    builder: (c) => PostDetails(
                      author: item.parentAuthor,
                      permlink: item.parentPermlink,
                    ),
                  ),
                );
              });
            },
          ),
        );
      }
    }
    fabItems.add(
      FabOverItemData(
        displayName: 'Close',
        icon: Icons.close,
        onTap: () {
          setState(() {
            isMenuOpen = false;
          });
        },
      ),
    );
    fabItems.insert(
      0,
      FabOverItemData(
        displayName: 'Report',
        icon: Icons.flag,
        onTap: () async {
          setState(() {
            isMenuOpen = false;
          });
          final Email email = Email(
            body:
                'I want to report Post - @${widget.author}/${widget.permlink}',
            subject: 'Report Post - @${widget.author}/${widget.permlink}',
            recipients: ['content-report@blurt.foundation'],
            isHTML: false,
          );
          try {
            await FlutterEmailSender.send(email);
          } catch (e) {
            var data = Uri.encodeFull(
                "subject=Report Post - @${widget.author}/${widget.permlink}&body=I want to report Post - @${widget.author}/${widget.permlink}");
            var url = Uri.parse('mailto:user-report@blurt.foundation?$data');
            launchUrl(url);
            log('Something went wrong ${e.toString()}');
          }
        },
      ),
    );
    return fabItems;
  }

  Widget _fabContainer(BlurtAppData appData, FeedResponseItem item) {
    if (!isMenuOpen) {
      return FabCustom(
        icon: Icons.bolt,
        onTap: () {
          setState(() {
            isMenuOpen = true;
          });
        },
      );
    }
    return FabOverlay(
      items: _fabItems(appData, item),
      onBackgroundTap: () {
        setState(() {
          isMenuOpen = false;
        });
      },
    );
  }

  Widget _futureBuilder(
    bool isDarkMode,
    BlurtUserData? user,
    BlurtAppData appData,
  ) {
    return FutureBuilder(
      future: _futureToLoadFeedDetails,
      builder: (builder, snapshot) {
        if (snapshot.hasError) {
          return _errorState();
        } else if (snapshot.hasData &&
            snapshot.connectionState == ConnectionState.done) {
          var postDetails = snapshot.data as PostDetailsContent;
          var item = postDetails.content.content;
          var comments = postDetails.comments.content;
          comments = comments
              .where((element) =>
                  !appData.blockAndCoal.contains(element.author) &&
                  !(element.meta?.tags ?? []).contains('nsfw'))
              .toList();
          log('comments count ${comments.length}');
          String readTime = readingTime(item.body).msg;
          var title = item.title.isEmpty ? item.rootTitle : item.title;
          String timeInString = "Posted ${timeago.format(item.createdAt)}";
          var app = item.meta?.app ?? 'blurt.blog';
          var subtitle =
              "👤 @${item.author}\n🔖 #${item.category}\n📝️ $timeInString\n🕵️ Read time: $readTime\n⌨️ via $app";
          var tags = "\n🏷️ #${item.meta?.tags.join(" #") ?? ""}<hr>\n";
          var newString = _processString(
              "# $title\n${item.title.isEmpty ? '👤 @${item.author}\n🕵️ Read time: $readTime\n📝️ $timeInString\n⌨️ via $app\n\nComment:\n' : '\n$subtitle$tags'}${item.body}");
          return DefaultTabController(
            length: 3,
            child: Builder(
              builder: (context) {
                final tabController = DefaultTabController.of(context);
                tabController.addListener(() {
                  log("New tab index: ${tabController.index}");
                  setState(() {
                    selectedTabIndex = tabController.index;
                  });
                });
                return Scaffold(
                  appBar: AppBar(
                    title: Text(
                      item.title.isNotEmpty
                          ? item.title
                          : "COMMENT ON: ${item.rootTitle}",
                      maxLines: 2,
                      style: const TextStyle(
                        fontSize: 16,
                      ),
                    ),
                    actions: [_actionItem(item, user)],
                    bottom: const TabBar(
                      tabs: [
                        Tab(icon: Icon(Icons.text_snippet_outlined)),
                        Tab(icon: Icon(Icons.favorite)),
                        Tab(icon: Icon(Icons.comment)),
                      ],
                    ),
                  ),
                  body: TabBarView(
                    children: [
                      SafeArea(
                        child: Stack(
                          children: [
                            _content(item, isDarkMode, newString),
                            _fabContainer(appData, item),
                          ],
                        ),
                      ),
                      _listViewForVoters(item),
                      PostDetailsComments(
                        originalDepth: item.depth,
                        content: PostDetailsComments.refactorComments(
                          List<FeedResponseItem>.from(comments),
                          item.permlink,
                        ),
                      ),
                    ],
                  ),
                );
              },
            ),
          );
        } else {
          return Scaffold(
            appBar: AppBar(
              title: Text(widget.author),
            ),
            body: const Center(child: CircularProgressIndicator()),
          );
        }
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    var data = Provider.of<BlurtAppData>(context);
    var isDarkMode = data.isDarkMode;
    var user = data.userData;
    return _futureBuilder(isDarkMode, user, data);
  }
}
