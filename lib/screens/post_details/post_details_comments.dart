import 'dart:developer';

import 'package:adaptive_action_sheet/adaptive_action_sheet.dart';
import 'package:blurt/models/feed_response_item.dart';
import 'package:blurt/screens/post_details/post_details.dart';
import 'package:blurt/screens/user_channel/user_channel.dart';
import 'package:blurt/utils/random_string.dart';
import 'package:blurt/widgets/custom_circle_avatar.dart';
import 'package:flutter/material.dart';
import 'package:flutter_markdown/flutter_markdown.dart';
import 'package:timeago/timeago.dart' as timeago;
import 'package:url_launcher/url_launcher.dart';

class PostDetailsComments extends StatelessWidget {
  const PostDetailsComments({
    Key? key,
    required this.content,
    required this.originalDepth,
  }) : super(key: key);
  final List<FeedResponseItem> content;
  final int originalDepth;

  static List<FeedResponseItem> refactorComments(
      List<FeedResponseItem> content, String parentPermlink) {
    log('Permlink is $parentPermlink');
    List<FeedResponseItem> refactoredComments = [];
    var newContent = List<FeedResponseItem>.from(content);
    for (var e in newContent) {
      e.visited = false;
    }
    newContent.sort((a, b) {
      var bTime = b.createdAt;
      var aTime = a.createdAt;
      if (aTime.isAfter(bTime)) {
        return -1;
      } else if (bTime.isAfter(aTime)) {
        return 1;
      } else {
        return 0;
      }
    });
    refactoredComments.addAll(
        newContent.where((e) => e.parentPermlink == parentPermlink).toList());
    while (refactoredComments.where((e) => e.visited == false).isNotEmpty) {
      var firstComment =
          refactoredComments.where((e) => e.visited == false).first;
      var indexOfFirstElement = refactoredComments.indexOf(firstComment);
      if (firstComment.children != 0) {
        List<FeedResponseItem> children = newContent
            .where((e) => e.parentPermlink == firstComment.permlink)
            .toList();
        children.sort((a, b) {
          var aTime = a.createdAt;
          var bTime = b.createdAt;
          if (aTime.isAfter(bTime)) {
            return -1;
          } else if (bTime.isAfter(aTime)) {
            return 1;
          } else {
            return 0;
          }
        });
        refactoredComments.insertAll(indexOfFirstElement + 1, children);
      }
      firstComment.visited = true;
    }
    log('Returning ${refactoredComments.length} elements');
    return refactoredComments;
  }

  void showMessage(String string, BuildContext context) {
    var snackBar = SnackBar(content: Text(string));
    ScaffoldMessenger.of(context).showSnackBar(snackBar);
  }

  void showSheet(BuildContext context, FeedResponseItem item) {
    showAdaptiveActionSheet(
      context: context,
      title: Text('Choose Action for \n@${item.author}/${item.permlink}'),
      androidBorderRadius: 30,
      actions: [
        BottomSheetAction(
          title: Text(
            'View ${item.author}\'s Blog',
            style: const TextStyle(color: Colors.deepOrange),
          ),
          leading: const Icon(Icons.person, color: Colors.deepOrange),
          onPressed: (context) {
            var screen = UserChannel(name: item.author);
            var route = MaterialPageRoute(builder: (c) => screen);
            Navigator.of(context).pop();
            Navigator.of(context).push(route);
          },
        ),
        BottomSheetAction(
          title: const Text(
            'Focus on this comment',
            style: TextStyle(color: Colors.deepOrange),
          ),
          leading: const Icon(Icons.remove_red_eye_rounded,
              color: Colors.deepOrange),
          onPressed: (context) {
            Navigator.of(context).pop();
            Navigator.of(context).push(
              MaterialPageRoute(
                builder: (c) =>
                    PostDetails(author: item.author, permlink: item.permlink),
              ),
            );
          },
        ),
      ],
      cancelAction: CancelAction(
        title: const Text(
          'Cancel',
          style: TextStyle(color: Colors.deepOrange),
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    if (content.isEmpty) {
      return Center(
        child: Column(
          children: const [
            Spacer(),
            Text('No comments'),
            SizedBox(height: 10),
            Icon(Icons.comment),
            Spacer(),
          ],
        ),
      );
    }
    return ListView.separated(
      itemBuilder: (c, i) {
        log('Depth is ${content[i].depth}');
        var depth = ((content[i].depth - originalDepth) * 10.0) - 10;
        double width = MediaQuery.of(context).size.width - 90 - depth;
        String timeInString = " · ${timeago.format(content[i].createdAt)}";
        String upVotes = content[i].activeVotes.isNotEmpty
            ? " · ${content[i].activeVotes.length} ❤️"
            : "";
        return ListTile(
          title: Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Container(margin: EdgeInsets.only(left: depth)),
              CustomCircleAvatar(
                  height: 40,
                  width: 40,
                  url:
                      'https://imgp.blurt.world/profileimage/${content[i].author}/64x64'),
              Container(margin: const EdgeInsets.only(right: 5)),
              SizedBox(
                width: width - 11,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                        "${content[i].author} $timeInString · ${content[i].pendingPayoutValue}$upVotes",
                        style: Theme.of(context).textTheme.subtitle2),
                    MarkdownBody(
                      data: RandomString().removeAllHtmlTags(content[i].body),
                      onTapLink: (text, url, title) {
                        launchUrl(Uri.parse(url ?? 'https://google.com'));
                      },
                    ),
                    Container(margin: const EdgeInsets.only(bottom: 10)),
                  ],
                ),
              ),
              const Icon(
                Icons.more_vert,
                color: Colors.deepOrange,
              ),
            ],
          ),
          onTap: () {
            showSheet(context, content[i]);
          },
        );
      },
      separatorBuilder: (c, i) => const Divider(),
      itemCount: content.length,
    );
  }
}
