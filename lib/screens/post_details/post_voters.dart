import 'package:blurt/bloc/server.dart';
import 'package:blurt/models/feed_response_item.dart';
import 'package:blurt/screens/post_details/post_voters_sort_options.dart';
import 'package:blurt/screens/user_channel/user_channel.dart';
import 'package:blurt/widgets/blurt_to_usd.dart';
import 'package:blurt/widgets/custom_circle_avatar.dart';
import 'package:blurt/widgets/fab_custom.dart';
import 'package:blurt/widgets/fab_overlay.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:timeago/timeago.dart' as timeago;

class PostVoters extends StatefulWidget {
  const PostVoters({
    Key? key,
    required this.voters,
    required this.payout,
    required this.netResourceShares,
    required this.totalShares,
  }) : super(key: key);
  final double payout;
  final double netResourceShares;
  final List<FeedResponseItemActiveVotesItem> voters;
  final double totalShares;

  @override
  State<PostVoters> createState() => _PostVotersState();
}

class _PostVotersState extends State<PostVoters> {
  var sortOrder = 'desc';
  var sortBy = 'vote';
  var isFilterMenuOn = false;

  List<FeedResponseItemActiveVotesItem> getSortedVotes() {
    var voters = widget.voters;
    if (sortBy == 'vote') {
      voters.sort((a, b) {
        var aVests = double.tryParse(a.rshares.replaceAll(" vests", "")) ?? 0.0;
        var bVests = double.tryParse(b.rshares.replaceAll(" vests", "")) ?? 0.0;
        if (sortOrder == 'desc') {
          if (aVests > bVests) {
            return -1;
          } else if (aVests < bVests) {
            return 1;
          } else {
            return 0;
          }
        } else {
          if (aVests < bVests) {
            return -1;
          } else if (aVests > bVests) {
            return 1;
          } else {
            return 0;
          }
        }
      });
    } else {
      voters.sort((a, b) {
        var aVests = a.voter.toLowerCase();
        var bVests = b.voter.toLowerCase();
        if (sortOrder == 'desc') {
          if (aVests.compareTo(bVests) > 0) {
            return -1;
          } else if (aVests.compareTo(bVests) < 0) {
            return 1;
          } else {
            return 0;
          }
        } else {
          if (aVests.compareTo(bVests) < 0) {
            return -1;
          } else if (aVests.compareTo(bVests) > 0) {
            return 1;
          } else {
            return 0;
          }
        }
      });
    }
    return voters;
  }

  Widget _listViewForVoters(String domain) {
    var voters = getSortedVotes();
    if (voters.isEmpty) {
      return Center(
        child: Column(
          children: const [
            Spacer(),
            Text('No up-votes'),
            SizedBox(height: 10),
            Icon(Icons.heart_broken),
            Spacer(),
          ],
        ),
      );
    }
    return ListView.separated(
      itemBuilder: (c, i) {
        if (i == 0) {
          var payoutHalf = widget.payout / 2.0;
          return ListTile(
            title: BlurtToUSDWidget(domain: domain, blurt: widget.payout),
            subtitle: Text(
                "Author: ${payoutHalf.toStringAsFixed(3)} BLURT, Curators: ${payoutHalf.toStringAsFixed(3)} BLURT"),
          );
        }
        var index = i - 1;
        DateTime? voteTime = voters[index].voteTime?.toLocal();
        String timeInString = voteTime != null
            ? " · ${timeago.format(voteTime, locale: DateTime.now().timeZoneName)}"
            : "";
        var shares = widget.netResourceShares;
        var value = shares == 0.0
            ? widget.payout *
                (voters[index].resourceShares / widget.totalShares)
            : widget.payout * (voters[index].resourceShares / shares);
        var amount = "${value.toStringAsFixed(3)} BLURT";
        return ListTile(
          leading: CustomCircleAvatar(
              height: 45,
              width: 45,
              url:
                  'https://imgp.blurt.world/profileimage/${voters[index].voter}/64x64'),
          title: Text(voters[index].voter),
          subtitle:
              Text("$amount · ${voters[index].percent / 100}% $timeInString"),
          onTap: () {
            var screen = UserChannel(name: voters[index].voter);
            var route = MaterialPageRoute(builder: (c) => screen);
            Navigator.of(context).push(route);
          },
        );
      },
      separatorBuilder: (c, i) => const Divider(),
      itemCount: voters.length + 1,
    );
  }

  void showBottomSheet() {
    showModalBottomSheet(
      context: context,
      builder: (builder) {
        return PostVotersSortOptionScreen(
          sortBy: sortBy,
          sortOrder: sortOrder,
          result: (sortBy, sortOrder) {
            setState(() {
              this.sortBy = sortBy;
              this.sortOrder = sortOrder;
            });
          },
        );
      },
    );
  }

  List<FabOverItemData> _fabItems() {
    return [
      FabOverItemData(
        displayName: 'Sort',
        icon: Icons.sort_by_alpha,
        onTap: () {
          setState(() {
            isFilterMenuOn = false;
            showBottomSheet();
          });
        },
      ),
      FabOverItemData(
        displayName: 'Close',
        icon: Icons.close,
        onTap: () {
          setState(() {
            isFilterMenuOn = false;
          });
        },
      ),
    ];
  }

  Widget _fab() {
    if (!isFilterMenuOn) {
      return FabCustom(
        icon: Icons.bolt,
        onTap: () {
          setState(() {
            isFilterMenuOn = true;
          });
        },
      );
    }
    return FabOverlay(
      items: _fabItems(),
      onBackgroundTap: () {
        setState(() {
          isFilterMenuOn = false;
        });
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    var appData = Provider.of<BlurtAppData>(context);
    return SafeArea(
      child: Stack(
        children: [
          _listViewForVoters(appData.domain),
          _fab(),
        ],
      ),
    );
  }
}
