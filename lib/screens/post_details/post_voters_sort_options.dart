import 'package:flutter/material.dart';

class PostVotersSortOptionScreen extends StatefulWidget {
  const PostVotersSortOptionScreen({
    Key? key,
    required this.sortBy,
    required this.sortOrder,
    required this.result,
  }) : super(key: key);
  final String sortOrder;
  final String sortBy;
  final Function(String, String) result;

  @override
  State<PostVotersSortOptionScreen> createState() =>
      _PostVotersSortOptionScreenState();
}

class _PostVotersSortOptionScreenState
    extends State<PostVotersSortOptionScreen> {
  late String sortOrder;
  late String sortBy;

  @override
  void initState() {
    super.initState();
    sortOrder = widget.sortOrder;
    sortBy = widget.sortBy;
  }

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: 275,
      child: Scaffold(
        appBar: AppBar(
          title: const Text('Select Sort Options'),
          actions: [
            IconButton(
              onPressed: () {
                widget.result(sortBy, sortOrder);
                Navigator.of(context).pop();
              },
              icon: const Icon(Icons.check),
            )
          ],
        ),
        body: Column(
          children: [
            const SizedBox(height: 10),
            Text(
              'Sort by:',
              style: Theme.of(context).textTheme.headline6,
            ),
            Row(
              children: [
                const Spacer(),
                const Icon(Icons.thumb_up_alt_outlined),
                const SizedBox(width: 5),
                const Text('Vote'),
                Checkbox(
                  value: sortBy == 'vote',
                  activeColor: Colors.deepOrange,
                  onChanged: (newValue) {
                    if (newValue != null) {
                      setState(() {
                        sortBy = newValue ? 'vote' : 'name';
                      });
                    }
                  },
                ),
                const SizedBox(width: 10),
                const Icon(Icons.sort_by_alpha),
                const SizedBox(width: 5),
                const Text('Name'),
                Checkbox(
                  value: sortBy == 'name',
                  activeColor: Colors.deepOrange,
                  onChanged: (newValue) {
                    if (newValue != null) {
                      setState(() {
                        sortBy = newValue ? 'name' : 'vote';
                      });
                    }
                  },
                ),
                const Spacer(),
              ],
            ),
            const SizedBox(height: 10),
            Text(
              'Sort Order:',
              style: Theme.of(context).textTheme.headline6,
            ),
            Row(
              children: [
                const Spacer(),
                const Icon(Icons.expand_more),
                const SizedBox(width: 5),
                const Text('Ascending'),
                Checkbox(
                  value: sortOrder == 'asc',
                  activeColor: Colors.deepOrange,
                  onChanged: (newValue) {
                    if (newValue != null) {
                      setState(() {
                        sortOrder = newValue ? 'asc' : 'desc';
                      });
                    }
                  },
                ),
                const SizedBox(width: 20),
                const Icon(Icons.expand_less),
                const SizedBox(width: 5),
                const Text('Descending'),
                Checkbox(
                  value: sortOrder == 'desc',
                  activeColor: Colors.deepOrange,
                  onChanged: (newValue) {
                    if (newValue != null) {
                      setState(() {
                        sortOrder = newValue ? 'desc' : 'asc';
                      });
                    }
                  },
                ),
                const Spacer(),
              ],
            )
          ],
        ),
      ),
    );
  }
}
