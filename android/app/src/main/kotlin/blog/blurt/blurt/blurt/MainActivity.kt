package blog.blurt.blurt.blurt

import android.annotation.SuppressLint
import android.content.Context
import android.net.Uri
import android.view.View
import android.webkit.*
import android.widget.FrameLayout
import androidx.annotation.NonNull
import androidx.webkit.WebViewAssetLoader
import com.google.gson.Gson
import io.flutter.embedding.android.FlutterActivity
import io.flutter.embedding.engine.FlutterEngine
import io.flutter.plugin.common.MethodChannel


class MainActivity : FlutterActivity() {
    var webView: WebView? = null
    var result: MethodChannel.Result? = null

    override fun configureFlutterEngine(@NonNull flutterEngine: FlutterEngine) {
        super.configureFlutterEngine(flutterEngine)
        if (webView == null) {
            setupView()
        }
        MethodChannel(
            flutterEngine.dartExecutor.binaryMessenger,
            "blog.blurt.blurt/auth"
        ).setMethodCallHandler { call, result ->
            this.result = result
            val username = call.argument<String>("username")
            val postingKey = call.argument<String>("postingKey")
            val domain = call.argument<String>("domain")
            val author = call.argument<String>("author")
            val permlink = call.argument<String>("permlink")
            val weight = call.argument<Int>("weight")
            val title = call.argument<String>("title")
            val body = call.argument<String>("body")
            val tags = call.argument<String>("tags")
            val summary = call.argument<String>("summary")
            val accountName = call.argument<String>("accountName")
            val actionAccount = call.argument<String>("actionAccount")
            val actionType = call.argument<String>("actionType")
            val pAuthor = call.argument<String>("pAuthor")
            val pPermlink = call.argument<String>("pPermlink")
            val thumbImage = call.argument<String>("thumbImage")
            val rewardVestingBalance = call.argument<String>("rewardVestingBalance")
            val rewardBlurt = call.argument<String>("rewardBlurt")

            if (call.method == "validate" && username != null && postingKey != null && domain != null) {
                webView?.evaluateJavascript(
                    "validateKey('$username','$postingKey', '$domain');",
                    null
                )
            } else if (call.method == "upvote" && username != null && postingKey != null && domain != null && author != null && permlink != null && weight != null) {
                webView?.evaluateJavascript(
                    "upvote('$username','$postingKey', '$domain', '$author', '$permlink', $weight);",
                    null
                )
            } else if (call.method == "comment" && username != null && postingKey != null && domain != null && author != null && permlink != null && title != null && body != null && tags != null && summary != null && pAuthor != null && pPermlink != null && thumbImage != null) {
                val versionCode: Int = BuildConfig.VERSION_CODE
                val versionName: String = BuildConfig.VERSION_NAME
                val appVersion = "blurt_Android/${versionName}_${versionCode}";
                webView?.evaluateJavascript(
                    "comment('$username','$postingKey', '$domain', '$permlink', '$title', '$body', '$summary' , '$tags' , '$appVersion', '${pAuthor}', '${pPermlink}', '${thumbImage}');",
                    null
                )
            } else if (call.method == "updateFollowInfo" && accountName != null && actionAccount != null && actionType != null) {
                webView?.evaluateJavascript(
                    "updateFollowInfo('$accountName','$actionAccount', '$actionType');",
                    null
                )
            } else if (call.method == "updateFollow" && accountName != null && actionAccount != null && actionType != null && postingKey != null) {
                webView?.evaluateJavascript(
                    "updateFollow('$accountName', '$postingKey','$actionAccount', '$actionType');",
                    null
                )
            } else if (call.method == "claimRewards" && accountName != null && postingKey != null && rewardBlurt != null && rewardVestingBalance != null && domain != null) {
                webView?.evaluateJavascript(
                    "claimRewards('$accountName', '$postingKey','$rewardBlurt', '$rewardVestingBalance', '$domain');",
                    null
                )
            }
        }
    }

    @SuppressLint("SetJavaScriptEnabled")
    private fun setupView() {
        val params = FrameLayout.LayoutParams(0, 0)
        webView = WebView(activity)
        val decorView = activity.window.decorView as FrameLayout
        decorView.addView(webView, params)
        webView?.visibility = View.GONE
        webView?.settings?.javaScriptEnabled = true
        webView?.settings?.domStorageEnabled = true
        webView?.settings?.loadWithOverviewMode = true
        webView?.webChromeClient = WebChromeClient()
        val assetLoader = WebViewAssetLoader.Builder()
            .addPathHandler("/assets/", WebViewAssetLoader.AssetsPathHandler(this))
            .build()
        val client: WebViewClient = object : WebViewClient() {
            override fun shouldInterceptRequest(
                view: WebView,
                request: WebResourceRequest
            ): WebResourceResponse? {
                return assetLoader.shouldInterceptRequest(request.url)
            }

            override fun shouldInterceptRequest(
                view: WebView,
                url: String
            ): WebResourceResponse? {
                return assetLoader.shouldInterceptRequest(Uri.parse(url))
            }
        }
        webView?.webViewClient = client
        webView?.addJavascriptInterface(WebAppInterface(this), "Android")
         webView?.loadUrl("https://appassets.androidplatform.net/assets/index.html")
//        webView?.loadUrl("file:///android_asset/assets/index.html")
    }
}

class WebAppInterface(private val mContext: Context) {
    @JavascriptInterface
    fun postMessage(message: String) {
        val main = mContext as? MainActivity ?: return
        val gson = Gson()
        val dataObject = gson.fromJson(message, JSEvent::class.java)
        when (dataObject.type) {
            JSBridgeAction.VALIDATE_KEY.value -> {
                // now respond back to flutter
                main.result?.success(message)
            }
            JSBridgeAction.UPVOTE.value -> {
                // now respond back to flutter
                main.result?.success(message)
            }
            JSBridgeAction.COMMENT.value -> {
                // now respond back to flutter
                main.result?.success(message)
            }
            JSBridgeAction.UPDATE_FOLLOW_INFO.value -> {
                // now respond back to flutter
                main.result?.success(message)
            }
            JSBridgeAction.UPDATE_FOLLOW.value -> {
                // now respond back to flutter
                main.result?.success(message)
            }
            JSBridgeAction.CLAIM_REWARDS.value -> {
                // now respond back to flutter
                main.result?.success(message)
            }
        }
    }
}

data class JSEvent(
    val type: String,
)

enum class JSBridgeAction(val value: String) {
    VALIDATE_KEY("validateKey"),
    UPVOTE("upvote"),
    COMMENT("comment"),
    UPDATE_FOLLOW_INFO("updateFollowInfo"),
    UPDATE_FOLLOW("updateFollow"),
    CLAIM_REWARDS("claimRewards"),
}

